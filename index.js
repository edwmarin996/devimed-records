import 'react-native-get-random-values';
import 'react-native-gesture-handler';

import { AppRegistry, YellowBox } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

// To ignore specific warnings
YellowBox.ignoreWarnings([
  'Require cycle:',
  'Calling `getNode()`',
  'Cannot update during',
  // 'Setting a timer',
]);

/*---------------------------------------------------------------------------
 *                      TO HANDLE EVENTS AFTER APP CLOSE
 * ---------------------------------------------------------------------------*/

import BackgroundFetch from 'react-native-background-fetch';
import synchronize from './src/api/syncModule';

let MyHeadlessTask = async event => {
  // Get task id from event
  let taskId = event.taskId;
  await synchronize();

  // Required:  Signal to native code that your task is complete.
  // If you don't do this, your app could be terminated and/or assigned
  // battery-blame for consuming too much time in background.
  BackgroundFetch.finish(taskId);
};

// Register your BackgroundFetch HeadlessTask
BackgroundFetch.registerHeadlessTask(MyHeadlessTask);
/* --------------------------------------------------------------------------- */

AppRegistry.registerComponent(appName, () => App);
