import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Divider, Input } from '@ui-kitten/components';
import { connect } from 'react-redux';
import {
  validateFilters,
  validateRecord,
  showMessage,
} from '../../utils/utils';
import { clearAllFilters } from '../../redux/actions/filters';
import { v4 as uuid } from 'uuid';
import synchronize from '../../api/syncModule';
import syncStates from '../../api/syncStates';
import {
  addHorizontalPaintRecord,
  updateHorizontalPaintRecord,
  addHorizontalPaintImageToDeletedList,
} from '../../redux/actions/horizontalPaint';
import {
  SelectSector,
  SelectRoad,
  SelectAbscissas,
  SelectImages,
  SelectLineSide,
  SelectRoadWay,
  AddComment,
} from '../../components/Filters';

import { CreateOrEditRecord, BasicGallery } from '../../components/General';
import { notEditRecord } from '../../redux/actions/tempData';

export class HorizontalPaintCreateOrEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: null,
      showModal: false,
    };
  }

  getBaseRecord = () => {
    const errors = validateFilters({
      checkSector: true,
      checkRoad: true,
      checkRoadWay: true,
      checkLineSide: true,
      checkAbscissas: true,
    });

    if (errors) {
      this.setState({ errors, showModal: true });
      return null;
    }

    return {
      // Basic fields
      sector_id: this.props.selectedSectorId,
      via_id: this.props.selectedRoadId,
      calzada_id: this.props.selectedRoadWayId,
      costado_id: this.props.selectedLineSideId,
      abscisa_inicial: this.props.initialAbscissa,
      abscisa_final: this.props.finalAbscissa,
      fecha: new Date().toISOString(),

      // Optional fields
      ...(this.props.comment && { comentarios: this.props.comment }),
      ...(this.props.images && { images: this.props.images }),

      meta: { syncState: syncStates.isNotSynchronized },
    };
  };

  saveRecord = () => {
    const record = this.getBaseRecord();
    if (!record) return;

    // Add unique info to record
    record.id = uuid();
    const recordIsValid = validateRecord(record);

    if (!recordIsValid) {
      record.meta.syncState = syncStates.isNotReadyToSynchronize;
    }

    this.props.addHorizontalPaintRecord(record);
    this.props.clearAllFilters();
    showMessage('El registro se guardó correctamente');

    /*
     * Try to synchronize after creating the record
     * (immediate synchronization is not guaranteed)
     */
    synchronize();
  };

  updateRecord = () => {
    const record = this.getBaseRecord();
    if (!record) return;

    // Add original data and update sync state
    record.id = this.props.editedRecord.id;
    record.fecha = this.props.editedRecord.fecha;

    const recordIsValid = validateRecord(record);

    if (!recordIsValid) {
      record.meta.syncState = syncStates.isNotReadyToSynchronize;
    } else {
      record.meta.syncState = syncStates.isNotSynchronized;
    }

    this.props.updateHorizontalPaintRecord(record);

    // Return to viewRecordsPage
    this.props.navigation.jumpTo('viewRecords');
    showMessage('El registro se actualizó correctamente');

    //Clear page
    setTimeout(() => {
      this.props.notEditRecord();
      this.props.clearAllFilters();
    }, 350);

    synchronize();
  };

  onRemoveImage = image => {
    if (image.meta.syncState === syncStates.isFullySynchronized)
      this.props.addGrassMowImageToDeletedList(image.id);
  };

  cancelEditRecord = () => {
    this.props.navigation.jumpTo('viewRecords');

    // To avoid show header image after change tab
    setTimeout(() => {
      this.props.notEditRecord();
      this.props.clearAllFilters();
    }, 350);
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    let buttons = {};
    let image;
    let title;

    if (this.props.isEditingRecord) {
      buttons.ok = {
        title: 'Actualizar',
        handler: this.updateRecord,
      };

      buttons.cancel = {
        title: 'Cancelar',
        handler: this.cancelEditRecord,
      };
      title = 'Editar Registro';
      image = require('../../assets/icons/pen.png');
    } else {
      buttons.ok = {
        title: 'Guardar',
        handler: this.saveRecord,
      };
      buttons.cancel = {
        title: 'Limpiar',
        handler: this.props.clearAllFilters,
      };
      title = 'Registrar pintura horizontal';
      image = require('../../assets/icons/trackWithPaintCar.png');
    }

    return (
      <CreateOrEditRecord
        navBarTitle={title}
        navBarMenu={this.props.navigation.openDrawer}
        navBarCategory='h5'
        image={image}
        errors={this.state.errors}
        closeModal={this.closeModal}
        showModal={this.state.showModal}
        buttons={buttons}>
        {this.props.editedRecord && (
          <Input textStyle={styles.textStyle} disabled>
            {this.props.editedRecord.id}
          </Input>
        )}

        <SelectSector />
        <SelectRoad />
        <SelectRoadWay />
        <SelectLineSide />
        <SelectAbscissas />

        <Divider style={styles.divider} />
        <AddComment />
        <SelectImages onRemoveImage={this.onRemoveImage} />
        <BasicGallery />
        <Divider style={styles.divider} />
      </CreateOrEditRecord>
    );
  }
}

const styles = StyleSheet.create({
  divider: {
    width: '100%',
    height: 1.5,
  },

  textStyle: {
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  selectedSectorId: state.filters.selectedSectorId,
  selectedRoadId: state.filters.selectedRoadId,
  selectedRoadWayId: state.filters.selectedRoadWayId,
  selectedLineSideId: state.filters.selectedLineSideId,
  initialAbscissa: state.filters.initialAbscissa,
  finalAbscissa: state.filters.finalAbscissa,
  comment: state.filters.comment,
  images: state.filters.images,

  isEditingRecord: state.tempData.isEditingRecord,
  editedRecord: state.tempData.editedRecord,
});

const mapDispatchToProps = {
  addHorizontalPaintRecord,
  clearAllFilters,
  notEditRecord,
  updateHorizontalPaintRecord,
  addHorizontalPaintImageToDeletedList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HorizontalPaintCreateOrEdit);
