import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import HorizontalPaintCreateOrEdit from './HorizontalPaintCreateOrEdit';
import HorizontalPaintViewRecords from './HorizontalPaintViewRecords';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { theme } from '../../styles/theme';

const tabItems = [
  {
    label: 'Agregar - Editar',
    name: 'createOrEditRecords',
    component: HorizontalPaintCreateOrEdit,
    icon: 'clipboard',
  },

  {
    label: 'Ver registros',
    name: 'viewRecords',
    component: HorizontalPaintViewRecords,
    icon: 'eye',
  },
];

const Tab = createMaterialBottomTabNavigator();
export default class GrassMowNavigation extends Component {
  render() {
    return (
      <Tab.Navigator barStyle={styles.tabBar} shifting>
        {tabItems.map((tab, idx) => (
          <Tab.Screen
            key={idx}
            options={{
              tabBarIcon: () => getIcon(tab.icon),
              tabBarLabel: tab.label,
            }}
            name={tab.name}
            component={tab.component}
          />
        ))}
      </Tab.Navigator>
    );
  }
}

function getIcon(name, size = 21, color = '#FFF') {
  return <Icon name={name} size={size} disabled color={color} />;
}

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: theme.palette.primary.main,
  },
});
