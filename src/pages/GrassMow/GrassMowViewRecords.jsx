import React, { Component } from 'react';
import { StyleSheet, Alert } from 'react-native';
import { Layout } from '@ui-kitten/components';
import { NavBar, RecordList } from '../../components/General';
import { connect } from 'react-redux';
import syncStates from '../../api/syncStates';
import { setFilters, getCardData, isEditingRecord } from '../../utils/utils';
import { removeGrassMowRecord } from '../../redux/actions/grassMow';
import { editRecord } from '../../redux/actions/tempData';

export class GrassMowViewRecords extends Component {
  // Extra verification before delete record
  handleRemoveRecord = (id, syncState) => {
    const addToPendingSyncList =
      syncState === syncStates.isPartiallySynchronized ||
      syncState === syncStates.isFullySynchronized
        ? true
        : false;

    Alert.alert(
      '¡Alerta!',
      '¿Esta seguro que desea borrar este registro?',
      [
        { text: 'Cancelar' },
        {
          text: 'Borrar',
          onPress: () =>
            this.props.removeGrassMowRecord(id, addToPendingSyncList),
        },
      ],
      { cancelable: true },
    );
  };

  handleEditRecord = recordId => {
    // Don't allow to edit if there is another edition in progress
    if (isEditingRecord()) {
      Alert.alert(
        '¡Alerta!',
        'Actualmente hay otro registro en edición; guarde o cancele los cambios antes de continuar',
        null,
        { cancelable: true },
      );
      return;
    }

    let record = Object.values(this.props.records).find(
      record => record.id === recordId,
    );

    setFilters({
      sectorId: record.sector_id,
      roadId: record.via_id,
      oldRoadSideId: record.costado_id,
      initialAbscissa: record.abscisa_inicial,
      finalAbscissa: record.abscisa_final,
      grassMowGroups: record.cuadrilla,

      comment: record.comentarios,
      images: record.images,
    });

    this.props.navigation.jumpTo('createOrEditRecords');
    this.props.editRecord(record);
  };

  formatRecord(record) {
    return getCardData({
      id: record.id,
      syncState: record.meta.syncState,
      date: record.fecha,

      roadId: record.via_id,
      oldRoadSideId: record.costado_id,
      initialAbscissa: record.abscisa_inicial,
      finalAbscissa: record.abscisa_final,
      grassMowGroups: record.cuadrilla,

      comment: record.comentarios,
      images: record.images,
    });
  }

  render() {
    return (
      <Layout style={styles.layout}>
        <NavBar
          menu={this.props.navigation.openDrawer}
          category='h3'
          title='Registros de Rocería'
        />

        <RecordList
          records={Object.values(this.props.records)}
          formatRecord={this.formatRecord}
          handleRemoveRecord={this.handleRemoveRecord}
          handleEditRecord={this.handleEditRecord}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = state => ({
  records: state.grassMow.records,
});

const mapDispatchToProps = {
  removeGrassMowRecord,
  editRecord,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GrassMowViewRecords);
