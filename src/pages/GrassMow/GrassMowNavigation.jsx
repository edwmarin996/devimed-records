import React from 'react';
import { StyleSheet } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import GrassMowCreateOrEdit from './GrassMowCreateOrEdit';
import GrassMowViewRecords from './GrassMowViewRecords';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { theme } from '../../styles/theme';

const tabItems = [
  {
    label: 'Agregar - Editar',
    name: 'createOrEditRecords',
    component: GrassMowCreateOrEdit,
    icon: 'clipboard',
  },

  {
    label: 'Ver registros',
    name: 'viewRecords',
    component: GrassMowViewRecords,
    icon: 'eye',
  },
];

const Tab = createMaterialBottomTabNavigator();

export default function GrassMowNavigation() {
  return (
    <Tab.Navigator barStyle={styles.tabBar} shifting>
      {tabItems.map((tab, idx) => (
        <Tab.Screen
          key={idx}
          options={{
            tabBarIcon: () => getIcon(tab.icon),
            tabBarLabel: tab.label,
          }}
          name={tab.name}
          component={tab.component}
        />
      ))}
    </Tab.Navigator>
  );
}

function getIcon(name, size = 21, color = '#FFF') {
  return <Icon name={name} size={size} color={color} />;
}

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: theme.palette.primary.main,
  },
});
