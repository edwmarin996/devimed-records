import React, { Component } from 'react';
import { StyleSheet, Image, ScrollView, Linking } from 'react-native';
import { NavBar } from '../../components/General';
import { Text } from '@ui-kitten/components';
import credits from '../../config/iconCredits';
import ListAccordion from '../../components/Other/CustomListAccordion';
import Config from '../../components/Other/Config';
import DeviceInfo from 'react-native-device-info';
import { Divider } from 'react-native-paper';
import { View } from 'react-native';
import { theme } from '../../styles/theme';

export default class AppInfo extends Component {
  render() {
    return (
      <>
        <View style={styles.navBar}>
          <NavBar
            category='h4'
            title='Acerca de la aplicación'
            menu={this.props.navigation.openDrawer}
          />
        </View>

        <ScrollView
          style={styles.scroll}
          keyboardShouldPersistTaps='handled'
          showsVerticalScrollIndicator={false}>
          <View style={styles.imageContainer}>
            <Image
              resizeMode='contain'
              style={styles.image}
              source={require('../../assets/icons/devimed.png')}
            />

            <Text appearance='hint'>Registros - Devimed</Text>
            <Text appearance='hint'>
              {'versión ' + DeviceInfo.getVersion()}
            </Text>
          </View>
          <Divider />
          <ListAccordion
            title='Información de la aplicación'
            icon='cellphone'
            color={theme.palette.primary.main}>
            <View style={styles.imageContainer}>
              <Image
                source={require('../../assets/icons/idm.png')}
                style={styles.imageSmall}
              />
            </View>

            <Text appearance='hint' style={styles.text}>
              Aplicación diseñada por la oficina de innovación desarrollo y
              mediciones.{'\n'}
            </Text>

            <Text appearance='hint' style={styles.text}>
              Para solicitudes, quejas, reclamos o informe de errores por favor
              remítase al{' '}
              <Text
                style={styles.href}
                onPress={() =>
                  Linking.openURL('mailto:medicionesdevimed@gmail.com.co')
                }>
                correo del programador.
              </Text>
            </Text>
          </ListAccordion>

          <Divider />

          <ListAccordion
            title='Créditos de los iconos'
            icon='information'
            color={theme.palette.primary.main}>
            <Text style={styles.text} appearance='hint'>
              Esta aplicación contiene iconos obtenidos desde las siguientes
              fuentes:{'\n'}
            </Text>
            {generateIconCredits()}
          </ListAccordion>
          <Divider />

          <Config />
        </ScrollView>
      </>
    );
  }
}

function generateIconCredits() {
  return credits.map((credit, idx) => (
    <Text appearance='hint' category='label' key={idx}>
      {'\u2022  Iconos diseñados por'}
      <Text
        style={styles.href}
        category='label'
        onPress={() => Linking.openURL(credit.href)}>
        {` ${credit.author} `}
      </Text>
      {`desde`}
      <Text
        style={styles.href}
        category='label'
        onPress={() => Linking.openURL('https://www.flaticon.es/')}>
        {' Flaticon \n'}
      </Text>
    </Text>
  ));
}

const styles = StyleSheet.create({
  navBar: {
    backgroundColor: '#FFF',
    paddingHorizontal: 20,
  },

  scroll: {
    flex: 1,
    backgroundColor: '#FFF',
    paddingHorizontal: 20,
  },

  imageContainer: {
    alignItems: 'center',
    marginVertical: 20,
  },

  image: {
    width: 250,
    height: 250,
  },

  imageSmall: {
    height: 200,
    resizeMode: 'contain',
  },

  text: {
    textAlign: 'justify',
  },

  href: {
    color: '#6666FC',
  },
});
