import React from 'react';
import { Text, Layout } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { userLogout } from '../../redux/actions/auth';
import { connect } from 'react-redux';

export function Unauthorized(props) {
  return (
    <Layout style={styles.layout}>
      <Text style={styles.header}>PERMISOS INSUFICIENTES</Text>

      <Icon style={styles.icon} color="#3C4858" size={200} name="lock" />
      <Text style={styles.footer}>
        No tiene permisos para ingresar a esta aplicación. Contacte al
        administrador del sistema para {'\n'} resolver el problema {'\n\n'}
        <Text onPress={props.userLogout} style={styles.logout}>
          Ingresar con otro usuario
        </Text>
      </Text>
    </Layout>
  );
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  header: {
    textAlign: 'center',
    color: '#3C4858',
    fontSize: 30,
  },

  icon: {
    marginVertical: 15,
  },

  footer: {
    textAlign: 'center',
  },

  logout: {
    color: '#5190FD',
  },
});

const mapDispatchToProps = {
  userLogout,
};

export default connect(
  null,
  mapDispatchToProps,
)(Unauthorized);
