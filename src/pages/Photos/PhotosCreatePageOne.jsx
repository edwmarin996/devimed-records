import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { SelectImages } from '../../components/Filters';
import { Layout } from '@ui-kitten/components';
import { HelperText, Button } from 'react-native-paper';
import { NavBar, EmptyImageList } from '../../components/General';
import CarouselGallery from '../../components/General/CarouselGallery';
import { addPhotoImageToDeletedList } from '../../redux/actions/photos';
import syncStates from '../../api/syncStates';
import { notEditRecord } from '../../redux/actions/tempData';
import { clearAllFilters } from '../../redux/actions/filters';
import { theme } from '../../styles/theme';

export class PhotosCreatePageOne extends Component {
  verifyImageTags = () => {
    //if (__DEV__) return true;
    if (this.props.images.length === 0) return false;

    const allImagesHaveTags = this.props.images.every(
      image => image?.tags?.length > 0,
    );
    return allImagesHaveTags;
  };

  onRemoveImage = image => {
    if (image.meta.syncState === syncStates.isFullySynchronized)
      this.props.addPhotoImageToDeletedList(image.id);
  };

  cancelEditRecord = () => {
    this.props.navigation.jumpTo('viewRecords');

    // To avoid show header image after change tab
    setTimeout(() => {
      this.props.notEditRecord();
      this.props.clearAllFilters();
    }, 350);
  };

  render() {
    let title = 'Registro fotográfico';
    if (this.props.isEditingRecord) title = 'Editar registro';

    return (
      <Layout style={styles.layout}>
        <NavBar
          menu={this.props.navigation.openDrawer}
          title={title}
          category='h3'
        />

        <View style={styles.gallery}>
          {this.props.images.length === 0 ? (
            <EmptyImageList />
          ) : (
            <CarouselGallery onRemoveImage={this.onRemoveImage} />
          )}
        </View>

        <View style={styles.footer}>
          <SelectImages />

          {this.props.images.length > 0 && !this.verifyImageTags() && (
            <HelperText style={styles.helperText} type='error'>
              * Debe etiquetar todas las imágenes
            </HelperText>
          )}

          <View style={styles.buttonContainer}>
            <Button
              mode='outlined'
              style={styles.footerButton}
              onPress={
                this.props.isEditingRecord
                  ? this.cancelEditRecord
                  : this.props.clearAllFilters
              }
              color={theme.palette.error.main}>
              {this.props.isEditingRecord ? 'Cancelar Edición' : 'Limpiar'}
            </Button>

            <Button
              mode='outlined'
              disabled={!this.verifyImageTags()}
              style={styles.footerButton}
              onPress={() => this.props.navigation.push('pageTwo')}
              color={theme.palette.primary.main}>
              CONTINUAR
            </Button>
          </View>
        </View>
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    paddingHorizontal: 25,
  },

  gallery: {
    flex: 0.7,
    justifyContent: 'space-around',
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  footerButton: {
    width: '48%',
    marginVertical: 5,
  },

  helperText: {
    textAlign: 'center',
  },

  footer: {
    justifyContent: 'flex-end',
    flex: 0.3,
    paddingBottom: 15,
  },
});

const mapStateToProps = state => ({
  images: state.filters.images,
  isEditingRecord: state.tempData.isEditingRecord,
});

const mapDispatchToProps = {
  addPhotoImageToDeletedList,
  notEditRecord,
  clearAllFilters,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotosCreatePageOne);
