import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { CreateOrEditRecord } from '../../components/General';
import { Divider, Input } from '@ui-kitten/components';
import { HelperText, Button } from 'react-native-paper';
import syncStates from '../../api/syncStates';
import { v4 as uuid } from 'uuid';
import {
  SelectRoadAllOptions,
  AddComment,
  SelectAbscissas,
  ExternalRecordSwitch,
} from '../../components/Filters';
import {
  clearAllFilters,
  clearRoadId,
  clearAbscissas,
  changeComment,
} from '../../redux/actions/filters';
import {
  validateFilters,
  validateRecord,
  getCoordinates,
  showMessage,
} from '../../utils/utils';
import {
  addPhotoRecord,
  overwritePhotoRecord,
} from '../../redux/actions/photos';
import { notEditRecord } from '../../redux/actions/tempData';
import synchronize from '../../api/syncModule';

export class PhotosCreatePageTwo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isExternalRecord: false,
      showModal: false,
      errors: [],
    };
  }

  getExternalBaseRecord = async addCoordinates => {
    let errors = validateFilters({
      minImages: 1,
    });

    if (errors) {
      this.setState({ errors, showModal: true });
      return;
    }

    let coordinates;
    if (addCoordinates) {
      showMessage('Registrando coordenada, por favor espere');
      coordinates = await getCoordinates();

      if (coordinates === null) {
        errors = ['Debe habilitar el GPS del dispositivo'];
        this.setState({ errors, showModal: true });
        return;
      }
    }

    return {
      fecha: new Date().toISOString(),
      images: this.props.images,
      ...(this.props.comment && { comentarios: this.props.comment }),
      ...(coordinates && { coordenada: coordinates }),

      meta: {
        syncState: syncStates.isNotSynchronized,
        isExternalRecord: true,
      },
    };
  };

  getBaseRecord = () => {
    const errors = validateFilters({
      checkRoad: true,
      minImages: 1,
      checkSingleAbscissa: true,
    });

    if (errors) {
      this.setState({ errors, showModal: true });
      return;
    }

    return {
      fecha: new Date().toISOString(),
      via_id: this.props.selectedRoadId,
      abscisa: this.props.abscissa,
      images: this.props.images,
      ...(this.props.comment && { comentarios: this.props.comment }),

      meta: { syncState: syncStates.isNotSynchronized },
    };
  };

  saveRecord = async () => {
    let record;
    if (this.props.isExternalRecord) {
      record = await this.getExternalBaseRecord(true);
    } else {
      record = this.getBaseRecord();
    }

    if (!record) return;

    // Add unique info to record
    record.id = uuid();
    const recordIsValid = validateRecord(record);

    if (!recordIsValid) {
      record.meta.syncState = syncStates.isNotReadyToSynchronize;
    }

    this.props.addPhotoRecord(record);
    this.props.clearAllFilters();
    this.props.navigation.pop();

    showMessage('El registro se guardó correctamente');

    /*
     * Try to synchronize after creating the record
     * (immediate synchronization is not guaranteed)
     */

    synchronize();
  };

  updateRecord = async () => {
    let record;

    // Append previous coordinates
    if (this.props.isExternalRecord) {
      const recordHasCoordinates = this.props.editedRecord.coordenada;

      if (recordHasCoordinates) {
        record = await this.getExternalBaseRecord(false);
        record.coordenada = this.props.editedRecord.coordenada;
      } else {
        record = await this.getExternalBaseRecord(true);
      }
    } else {
      record = this.getBaseRecord();
    }

    if (!record) return;

    // Add original data and update sync state
    record.id = this.props.editedRecord.id;
    record.fecha = this.props.editedRecord.fecha;

    const recordIsValid = validateRecord(record);

    if (!recordIsValid) {
      record.meta.syncState = syncStates.isNotReadyToSynchronize;
    } else {
      record.meta.syncState = syncStates.isNotSynchronized;
    }

    this.props.overwritePhotoRecord(record);

    // Return to viewRecordsPage
    this.props.navigation.jumpTo('viewRecords');
    showMessage('El registro se actualizó correctamente');

    //Clear page
    setTimeout(() => {
      this.props.notEditRecord();
      this.props.clearAllFilters();
      this.props.navigation.pop();
    }, 350);

    synchronize();
  };

  cancelEditRecord = () => {
    this.props.navigation.pop();
    this.props.navigation.jumpTo('viewRecords');

    // To avoid show header image after change tab
    setTimeout(() => {
      this.props.notEditRecord();
      this.props.clearAllFilters();
    }, 350);
  };

  clearFilters = () => {
    this.props.clearRoadId();
    this.props.clearAbscissas();
    this.props.changeComment('');
  };

  get CreateOrEditConfig() {
    let buttons = {};
    let image;
    let title;

    if (this.props.isEditingRecord) {
      buttons.ok = {
        title: 'Actualizar',
        handler: this.updateRecord,
      };

      buttons.cancel = {
        title: 'Cancelar Edición',
        handler: this.cancelEditRecord,
      };

      title = 'Editar Registro';
      image = require('../../assets/icons/pen.png');

      return { buttons, image, title };
    }

    buttons.ok = {
      title: 'Guardar',
      handler: this.saveRecord,
    };

    buttons.cancel = {
      title: 'Limpiar',
      handler: this.clearFilters,
    };

    title = 'Registro fotográfico';
    image = require('../../assets/icons/eye.png');

    return { buttons, image, title };
  }

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { buttons, image, title } = this.CreateOrEditConfig;

    return (
      <>
        <CreateOrEditRecord
          backButton={() => this.props.navigation.pop()}
          navBarTitle={title}
          navBarMenu={this.props.navigation.openDrawer}
          navBarCategory='h3'
          image={image}
          errors={this.state.errors}
          closeModal={this.closeModal}
          showModal={this.state.showModal}
          buttons={buttons}>
          {this.props.editedRecord && (
            <Input disabled>{this.props.editedRecord.id}</Input>
          )}

          <ExternalRecordSwitch />
          <Divider style={styles.divider} />

          {!this.props.isExternalRecord && (
            <>
              <HelperText>Vía</HelperText>
              <SelectRoadAllOptions />

              <HelperText>Abscisa</HelperText>
              <SelectAbscissas hideFinalAbscissa />
            </>
          )}

          <HelperText>Descripción del registro</HelperText>
          <AddComment />
        </CreateOrEditRecord>
      </>
    );
  }
}

const styles = StyleSheet.create({
  divider: {
    width: '100%',
  },
});

const mapStateToProps = state => ({
  selectedRoadId: state.filters.selectedRoadId,
  abscissa: state.filters.initialAbscissa,
  comment: state.filters.comment,
  images: state.filters.images,
  isExternalRecord: state.filters.isExternalRecord,

  isEditingRecord: state.tempData.isEditingRecord,
  editedRecord: state.tempData.editedRecord,
});

const mapDispatchToProps = {
  addPhotoRecord,
  notEditRecord,
  overwritePhotoRecord,

  clearAllFilters,
  clearRoadId,
  clearAbscissas,
  changeComment,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotosCreatePageTwo);
