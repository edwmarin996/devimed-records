import React, { Component } from 'react';
import { StyleSheet, Alert } from 'react-native';
import { Layout } from '@ui-kitten/components';
import { NavBar, RecordList } from '../../components/General';
import { connect } from 'react-redux';
import syncStates from '../../api/syncStates';
import { setFilters, getCardData, isEditingRecord } from '../../utils/utils';
import { removePhotoRecord } from '../../redux/actions/photos';
import { editRecord } from '../../redux/actions/tempData';

export class PhotosViewRecords extends Component {
  // Extra verification before delete record
  handleRemoveRecord = (id, syncState) => {
    const addToPendingSyncList =
      syncState === syncStates.isPartiallySynchronized ||
      syncState === syncStates.isFullySynchronized
        ? true
        : false;

    Alert.alert(
      '¡Alerta!',
      '¿Esta seguro que desea borrar este registro?',
      [
        { text: 'Cancelar' },
        {
          text: 'Borrar',
          onPress: () => this.props.removePhotoRecord(id, addToPendingSyncList),
        },
      ],
      { cancelable: true },
    );
  };

  handleEditRecord = recordId => {
    // Don't allow to edit if there is another edition in progress
    if (isEditingRecord()) {
      Alert.alert(
        '¡Alerta!',
        'Actualmente hay otro registro en edición; guarde o cancele los cambios antes de continuar',
        null,
        { cancelable: true },
      );
      return;
    }

    let record = Object.values(this.props.records).find(
      record => record.id === recordId,
    );

    setFilters({
      sectorId: record.sector_id,
      roadId: record.via_id,
      initialAbscissa: record.abscisa,
      comment: record.comentarios,
      images: record.images,
      isExternalRecord: record.meta.isExternalRecord,
    });

    this.props.navigation.jumpTo('createOrEditRecords');
    this.props.editRecord(record);
  };

  formatRecord(record) {
    return getCardData(
      {
        id: record.id,
        date: record.fecha,
        roadId: record.via_id,
        abscissa: record.abscisa,
        comment: record.comentarios,
        images: record.images,
        syncState: record.meta.syncState,
      },
      {
        coordenada: record.coordenada,
        isExternalRecord: record.meta.isExternalRecord,
      },
    );
  }

  render() {
    return (
      <Layout style={styles.layout}>
        <NavBar
          menu={this.props.navigation.openDrawer}
          category='h4'
          title='Registros fotográficos'
        />

        <RecordList
          records={Object.values(this.props.records)}
          formatRecord={this.formatRecord}
          handleRemoveRecord={this.handleRemoveRecord}
          handleEditRecord={this.handleEditRecord}
        />
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = state => ({
  records: state.photos.records,
});

const mapDispatchToProps = {
  removePhotoRecord,
  editRecord,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotosViewRecords);
