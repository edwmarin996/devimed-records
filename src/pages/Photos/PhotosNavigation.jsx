import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PhotosCreatePageOne from './PhotosCreatePageOne';
import PhotosCreatePageTwo from './PhotosCreatePageTwo';
import PhotosViewRecords from './PhotosViewRecords';
import { theme } from '../../styles/theme';

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

const tabItems = [
  {
    label: 'Agregar - Editar',
    name: 'createOrEditRecords',
    component: PhotosCreate,
    icon: 'clipboard',
  },

  {
    label: 'Ver registros',
    name: 'viewRecords',
    component: PhotosViewRecords,
    icon: 'eye',
  },
];

export function PhotosCreate() {
  return (
    <Stack.Navigator initialRouteName='pageOne'>
      <Stack.Screen component={PhotosCreatePageOne} name='pageOne' />

      <Stack.Screen
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        component={PhotosCreatePageTwo}
        name='pageTwo'
      />
    </Stack.Navigator>
  );
}

export default class PhotosNavigation extends Component {
  render() {
    return (
      <Tab.Navigator barStyle={styles.tabBar} shifting>
        {tabItems.map((tab, idx) => (
          <Tab.Screen
            key={idx}
            options={{
              tabBarIcon: () => getIcon(tab.icon),
              tabBarLabel: tab.label,
            }}
            name={tab.name}
            component={tab.component}
          />
        ))}
      </Tab.Navigator>
    );
  }
}

function getIcon(name, size = 21, color = '#FFF') {
  return <Icon name={name} size={size} disabled color={color} />;
}

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: theme.palette.primary.main,
  },
});
