import React, { Component } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { connect } from 'react-redux';
import { getDataFromServer } from '../../redux/actions/appData';
import CustomDrawer from '../../components/Other/CustomDrawer';
import Unauthorized from '../Other/Unauthorized';
import roles from '../../config/roles';
import AppInfo from '../Other/AppInfo';
import BackgroundFetch from 'react-native-background-fetch';
import synchronize from '../../api/syncModule.js';
import { Image, StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import GrassMowNavigation from '../GrassMow/GrassMowNavigation';
import HorizontalPaintNavigation from '../HorizontalPaint/HorizontalPaintNavigation';
import PhotosNavigation from '../Photos/PhotosNavigation';
import VerticalSignalsNavigation from '../VerticalSignals/VerticalSignalsNavigation';

// Register drawer items here (Suggested priority order)
const drawerItems = [
  {
    name: 'Señales verticales',
    component: VerticalSignalsNavigation,
    roles: [roles.VERTICAL_SIGNALS],
    icon: require('../../assets/icons/road-sign.png'),
  },
  {
    name: 'Rocería',
    component: GrassMowNavigation,
    roles: [roles.GRASS_MOW_INSPECTOR],
    icon: require('../../assets/icons/grass.png'),
  },
  {
    name: 'Fotografías',
    component: PhotosNavigation,
    roles: [roles.PHOTOS_RECORDS],
    icon: require('../../assets/icons/camera.png'),
  },
  {
    name: 'Señalización Horizontal',
    component: HorizontalPaintNavigation,
    roles: [roles.HORIZONTAL_PAINTING],
    icon: require('../../assets/icons/paintCube.png'),
  },
];

const Drawer = createDrawerNavigator();
export class Home extends Component {
  componentDidMount = () => {
    // Get AppData from server
    this.props.getDataFromServer();

    // Schedule background data sync to server
    BackgroundFetch.configure(
      {
        minimumFetchInterval: 1, // Time in minutes between function activation
        enableHeadless: true, // To allow call function after app was terminated
        stopOnTerminate: false, // To enable call to function after app was terminated
        startOnBoot: true, // Enable call to function after device reboot
        periodic: true, // Define task as periodical
        forceAlarmManager: true, // To improve the accuracy of timer
      },
      async taskId => {
        //Function that will be called on every background-fetch event
        console.log('Received background-fetch event: ', taskId);
        await synchronize();

        // Required:  Signal to native code that your task is complete.
        // If you don't do this, your app could be terminated and/or assigned
        // battery-blame for consuming too much time in background
        BackgroundFetch.finish(taskId);
      },
      error => {
        console.log('Background fetch failed to start: ', error);
      },
    );
  };

  render() {
    const allowedPages = drawerItems.reduce((allowedPages, page, idx) => {
      if (page.roles.every(role => this.props.userRoles.indexOf(role) > -1)) {
        allowedPages.push(
          <Drawer.Screen
            key={idx}
            name={page.name}
            component={page.component}
            options={{
              drawerIcon: () =>
                page.icon && (
                  <Image source={page.icon} style={styles.miniature} />
                ),
            }}
          />,
        );
      }

      return allowedPages;
    }, []);

    if (allowedPages.length === 0) {
      return <Unauthorized />;
    }

    return (
      <>
        <Spinner
          overlayColor='#000000AA'
          visible={this.props.loading}
          textContent={this.props.loadingMessage}
          textStyle={styles.spinnerText}
        />

        <Drawer.Navigator
          edgeWidth={0}
          drawerType='front'
          drawerContent={props => <CustomDrawer {...props} />}>
          {allowedPages}
          <Drawer.Screen
            options={{
              drawerIcon: () => (
                <Image
                  source={require('../../assets/icons/info.png')}
                  style={styles.miniature}
                />
              ),
            }}
            name='Acerca de la aplicación'
            component={AppInfo}
          />
        </Drawer.Navigator>
      </>
    );
  }
}

const styles = StyleSheet.create({
  miniature: {
    width: 25,
    height: 25,
  },

  spinnerText: {
    fontFamily: 'sans-serif-condensed',
    color: '#FFF',
    textAlign: 'center',
  },
});

const mapDispatchToProps = {
  getDataFromServer,
};

const mapStateToProps = state => ({
  userRoles: state.auth.userRoles,
  loading: state.tempData.loading,
  loadingMessage: state.tempData.loadingMessage,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
