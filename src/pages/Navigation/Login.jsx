import React, { Component } from 'react';
import { StyleSheet, View, Image, Linking, Alert } from 'react-native';
import { Text, Input, Layout } from '@ui-kitten/components';
import { getAccessToken, authServerErrors } from '../../api/authModule';
import { ActivityIndicator, Button, TouchableRipple } from 'react-native-paper';
import { keyCloakConfig } from '../../config/config';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const passwordRecoverURL = `${keyCloakConfig.AUTH_SERVER_URL}realms/${
  keyCloakConfig.realm
}/login-actions/reset-credentials?client_id=${keyCloakConfig.resource}`;

const accountURL = `${keyCloakConfig.AUTH_SERVER_URL}realms/${
  keyCloakConfig.realm
}/account`;

const logOutRedirectToAccountURL = `${keyCloakConfig.AUTH_SERVER_URL}realms/${
  keyCloakConfig.realm
}/protocol/openid-connect/logout?redirect_uri=${accountURL}`;

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      loading: false,
      error: null,
      showPassword: false,
    };
  }

  login = async () => {
    this.setState({ loading: true });
    let response = await getAccessToken(
      this.state.username,
      this.state.password,
    );

    if (!response.ok) {
      if (response.error == authServerErrors['Account is not fully set up']) {
        this.setState({ loading: false });

        Alert.alert(
          'Información de su cuenta',
          response.error,
          [
            { text: 'Cancelar' },
            {
              text: 'OK',
              onPress: () => Linking.openURL(logOutRedirectToAccountURL),
            },
          ],
          { cancelable: false },
        );

        return;
      }

      this.setState({ error: response.error });
    }

    this.setState({ loading: false });
  };

  render() {
    const ShowPassword = props => (
      <TouchableWithoutFeedback
        onPress={() =>
          this.setState({ showPassword: !this.state.showPassword })
        }>
        <Icon
          name={this.state.showPassword ? 'eye-off' : 'eye'}
          color={props.style.tintColor}
          size={props.style.height}
        />
      </TouchableWithoutFeedback>
    );

    return (
      <Layout style={styles.layout}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.user}
            source={require('../../assets/icons/workerDevimed.png')}
          />
          <Text style={[styles.text, styles.header]} category='h4'>
            Ingrese a la aplicación
          </Text>
        </View>

        <Input
          value={this.state.username}
          onChangeText={username => this.setState({ username })}
          style={styles.input}
          placeholder='Usuario'
        />

        <Input
          value={this.state.password}
          onChangeText={password => this.setState({ password })}
          style={styles.input}
          secureTextEntry={!this.state.showPassword}
          placeholder='Contraseña'
          accessoryRight={ShowPassword}
          onSubmitEditing={this.login}
        />

        <TouchableRipple
          style={styles.ripple}
          onPress={() => Linking.openURL(passwordRecoverURL)}>
          <Text style={styles.recoverPassword}>
            ¿Ha olvidado su contraseña?
          </Text>
        </TouchableRipple>

        <Button
          mode='contained'
          color='#222222'
          style={styles.button}
          onPress={this.login}>
          Iniciar sesión
        </Button>

        {this.state.error && (
          <Text style={styles.text}>{this.state.error}</Text>
        )}

        {this.state.loading && (
          <View style={styles.footer}>
            <ActivityIndicator size={40} color='#333333' />
            <Text style={styles.text}>Por favor espere...</Text>
          </View>
        )}
      </Layout>
    );
  }
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    paddingHorizontal: 30,
  },

  header: {
    color: '#666666',
  },

  user: {
    width: wp('60%'),
    height: hp('35%'),
    resizeMode: 'cover',
    marginTop: hp('5%'),
  },

  imageContainer: {
    alignItems: 'center',
  },

  text: {
    textAlign: 'center',
    marginVertical: '5%',
    color: '#333333',
  },

  input: {
    marginVertical: '1%',
  },

  ripple: {
    alignItems: 'center',
  },

  recoverPassword: {
    color: '#5555FD',
    marginBottom: 20,
  },

  button: {
    height: 40,
    justifyContent: 'center',
  },

  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
