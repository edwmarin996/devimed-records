import React, { Component } from 'react';
import { connect } from 'react-redux';
import Login from './Login';
import Home from './Home';
import { createStackNavigator } from '@react-navigation/stack';
import { refreshAccessToken } from '../../api/authModule';
import { ImageBackground, StyleSheet, View, Text } from 'react-native';
import config from '../../config/config';
import { resetStore, setReduxTreeVersion } from '../../redux/actions/appData';
import { Linking, Alert } from 'react-native';
import VersionCheck from 'react-native-version-check';
import NetInfo from '@react-native-community/netinfo';
import SplashScreen from 'react-native-splash-screen';
import { ActivityIndicator } from 'react-native-paper';

const Stack = createStackNavigator();

export class Navigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    };
  }

  showUpdateRequiredMessage(storeUrl) {
    Alert.alert(
      '¡Alerta!',
      'Hay una actualización disponible. ¿Desea actualizar ahora?',
      [
        {
          text: 'Cancelar',
          onPress: () => this.setState({ loading: false }),
        },
        {
          text: 'Actualizar',
          onPress: () => (
            Linking.openURL(storeUrl), this.setState({ loading: false })
          ),
        },
      ],
    );
  }

  componentDidMount = () => {
    SplashScreen.hide();
    this.checkAppStatus();
  };

  checkAppStatus = async () => {
    // Check Internet connection
    if (!(await NetInfo.fetch()).isInternetReachable) {
      this.setState({ loading: false });
      return;
    }

    // Check user auth status
    if (this.props.refreshToken) {
      await refreshAccessToken();
    }

    // Check redux tree version, and reset it if necessary
    if (this.props.reduxTreeVersion !== config.REDUX_TREE_VERSIÓN) {
      this.props.resetStore();
      this.props.setReduxTreeVersion(config.REDUX_TREE_VERSIÓN);
    }

    // Check for available updates, and redirect to apps store if necessary
    if (!__DEV__) {
      const { isNeeded, storeUrl } = await VersionCheck.needUpdate();

      // If app doesn't exists in play store or device hasn't internet
      if (!isNeeded) {
        this.setState({ loading: false });
        return;
      }

      this.showUpdateRequiredMessage(storeUrl);
      return;
    }

    this.setState({ loading: false });
  };

  render() {
    if (this.state.loading) return <LoadingScreen />;

    return (
      <Stack.Navigator initialRouteName='Login'>
        {this.props.refreshToken ? (
          <Stack.Screen
            options={{ header: () => null }}
            name='Home'
            component={Home}
          />
        ) : (
          <Stack.Screen
            options={{ header: () => null }}
            name='Login'
            component={Login}
          />
        )}
      </Stack.Navigator>
    );
  }
}

function LoadingScreen() {
  return (
    <ImageBackground
      style={styles.backgroundImage}
      source={require('../../assets/backgrounds/splash.png')}>
      <View style={styles.footer}>
        <ActivityIndicator size={40} color='#333333' />
        <Text style={styles.text}>
          {'Verificando información del usuario \n por favor espere...'}
        </Text>
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 24,
  },

  footer: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    marginVertical: 20,
    textAlign: 'center',
    color: '#333333',
    fontSize: 20,
  },
});

const mapStateToProps = state => ({
  refreshToken: state.auth.refreshToken,
  reduxTreeVersion: state.appData.reduxTreeVersion,
});

const mapDispatchToProps = {
  resetStore,
  setReduxTreeVersion,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Navigation);
