import React, { Component, useState } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { setLocalTiles } from '../../redux/actions/verticalSignals';
import { downloadVerticalSignalsAppData } from '../../redux/actions/verticalSignals';
import VerticalSignalsMap from './VerticalSignalsMap';
import VerticalSignalsForm from './VerticalSignalsForm';
import VerticalSignalsConfig from './VerticalSignalsConfig';
import { theme } from '../../styles/theme';
import * as RNFS from 'react-native-fs';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName='map'>
      <Stack.Screen name='map' component={VerticalSignalsMap} />
      <Stack.Screen name='form' component={VerticalSignalsForm} />
    </Stack.Navigator>
  );
};

const tabItems = [
  {
    label: 'Mapa',
    name: 'createOrEditRecords',
    component: StackNavigator, //StackNavigator,
    icon: 'map',
  },
  {
    label: 'Ajustes',
    name: 'viewRecords',
    component: VerticalSignalsConfig,
    icon: 'settings',
  },
];

export class VerticalSignalsNavigation extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.findTilesPath();
    this.props.downloadVerticalSignalsAppData();
  }

  /**
   * To find local tiles folder
   */
  findTilesPath = async () => {
    const sdCardNamesRegex = /^\w{4}-\w{4}$/;
    const storages = await RNFS.readDir('/storage');

    const sdPath = storages.find(storage =>
      sdCardNamesRegex.test(storage.name),
    );

    if (sdPath?.path) {
      const tilesPath = `file://${sdPath.path}/tiles/{z}/{x}/{y}.png`;
      this.props.setLocalTiles(tilesPath);
    }
  };

  render() {
    return (
      <Tab.Navigator
        keyboardHidesNavigationBar={false}
        barStyle={styles.tabBar}
        shifting
        activeColor={theme.palette.primary.main}
        inactiveColor='#666'
        initialRouteName='map'>
        {tabItems.map((tab, idx) => (
          <Tab.Screen
            key={idx}
            options={{
              tabBarIcon: tab.icon,
              tabBarLabel: tab.label,
            }}
            name={tab.name}
            component={tab.component}
          />
        ))}
      </Tab.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: '#FFF',
    borderColor: '#FBFBFB',
    borderTopWidth: 1.2,
  },
});

const mapDispatchToProps = {
  setLocalTiles,
  downloadVerticalSignalsAppData,
};

export default connect(
  null,
  mapDispatchToProps,
)(VerticalSignalsNavigation);
