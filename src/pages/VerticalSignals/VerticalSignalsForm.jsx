import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, ScrollView, Text } from 'react-native';
import { BasicGallery, ErrorsModal } from '../../components/General';
import { clearAllFilters } from '../../redux/actions/filters';
import { ImageBackground } from 'react-native';
import { HelperText, IconButton, Button } from 'react-native-paper';
import { setSelectedFlavorId } from '../../redux/actions/verticalSignals';
import { v4 as uuid } from 'uuid';
import { notEditRecord } from '../../redux/actions/tempData';

import {
  addOrUpdateVerticalSignal,
  clearVerticalSignalFilters,
} from '../../redux/actions/verticalSignals';
import {
  getCoordinatesFromRegion,
  getGeoJsonCoordinatesFromRegion,
  validateFilters,
} from '../../utils/utils';
import {
  SelectFlavor,
  ShowFlavor,
  SelectPaperType,
} from '../../components/VerticalSignals/';
import {
  SelectRoadSide,
  SelectImages,
  SelectAbscissas,
  SelectRoadAllOptions,
  SelectDate,
} from '../../components/Filters';
import { theme } from '../../styles/theme';

export class VerticalSignalsForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: [],
      showModal: false,
    };
  }

  closePage = () => {
    this.props.navigation.pop();
    this.props.clearAllFilters();
    this.props.clearVerticalSignalFilters();
    this.props.setSelectedFlavorId(null);
    this.props.notEditRecord();
  };

  validateVerticalSignalFilters = () => {
    let errors = validateFilters({
      checkRoad: true,
      checkRoadSide: true,
      checkSingleAbscissa: true,
      maxImages: 1,
    });

    /**
     * validateFilters return null or [], should be converted
     * explicity to [] for append new elements.
     */
    if (!errors) {
      errors = [];
    }

    if (!this.props.selectedFlavorId) {
      errors.push('Debe seleccionar un tipo de señal');
    }

    if (!this.props.selectedPaperTypeId) {
      errors.push('Debe seleccionar un tipo de papel');
    }

    return errors.length ? errors : null;
  };

  getBaseSignal = () => {
    const errors = this.validateVerticalSignalFilters();

    if (errors) {
      this.setState({ errors, showModal: true });
      return null;
    }

    return {
      id: uuid(),
      via_id: this.props.selectedRoadId,
      senal_id: this.props.selectedFlavorId,
      papel_id: this.props.selectedPaperTypeId,
      costado_via_id: this.props.selectedRoadSideId,
      abscisa: this.props.abscissa,
      fecha_instalacion: this.props.selectedDate,

      geom: getGeoJsonCoordinatesFromRegion(this.props.region),

      /**
       * For local use only.
       * Note that the photo field must be different from the original one,
       * in this case, the new field is "photoName" and the original field
       * is "PhotoName".
       */
      ...(this.props.images[0] && { photoNameAlt: this.props.images[0] }),
      coordinates: getCoordinatesFromRegion(this.props.region),
    };
  };

  onSave = () => {
    let signal = this.getBaseSignal();
    if (!signal) {
      return;
    }

    this.props.addOrUpdateVerticalSignal(signal);
    this.closePage();
  };

  onUpdate = () => {
    let signal = this.getBaseSignal();
    if (!signal) {
      return;
    }

    signal = {
      ...this.props.selectedSignal,
      ...signal,
    };

    // Add original geolocation properties and id
    signal.id = this.props.selectedSignal.id;
    signal.geom = this.props.selectedSignal.geom;
    signal.coordinates = this.props.selectedSignal.coordinates;

    this.props.addOrUpdateVerticalSignal(signal);
    this.closePage();
  };

  render() {
    return (
      <ImageBackground
        source={require('../../assets/backgrounds/background_1.png')}
        style={styles.background}>
        <View style={styles.titleContainer}>
          <IconButton
            size={40}
            color='#FFF'
            icon='arrow-left'
            onPress={this.closePage}
          />

          {this.props.isEditingRecord ? (
            <Text style={styles.title}>EDITAR SEÑAL</Text>
          ) : (
            <Text style={styles.title}>AGREGAR SEÑAL</Text>
          )}
        </View>

        <ErrorsModal
          errors={this.state.errors}
          show={this.state.showModal}
          closeModal={() => this.setState({ showModal: false })}
        />

        <View style={styles.showFlavorsContainer}>
          <ShowFlavor onPress={() => this.selectFlavor.open()} />
        </View>

        <ScrollView showsVerticalScrollIndicator={false} style={styles.content}>
          <HelperText>Via</HelperText>
          <SelectRoadAllOptions />

          <HelperText>Costado</HelperText>
          <SelectRoadSide />

          <HelperText>Tipo de papel</HelperText>
          <SelectPaperType />

          <HelperText>Abscissa</HelperText>
          <SelectAbscissas hideFinalAbscissa />

          <HelperText>Fecha de instalación</HelperText>
          <SelectDate />

          <HelperText>Imágenes</HelperText>
          <SelectImages style='modern' />

          <BasicGallery />

          <Button
            onPress={this.props.isEditingRecord ? this.onUpdate : this.onSave}
            mode='contained'
            style={styles.button}
            color={theme.palette.primary.main}>
            Guardar
          </Button>
        </ScrollView>

        <SelectFlavor ref={ref => (this.selectFlavor = ref)} />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },

  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  showFlavorsContainer: {
    top: 8,
    left: 70,
    marginBottom: 10,
  },

  title: {
    color: '#FFF',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 22,
    marginHorizontal: 20,
  },

  content: {
    paddingHorizontal: 20,
  },

  button: {
    width: '100%',
    marginVertical: 10,
  },
});

const mapStateToProps = state => {
  const selectedSignalId = state.verticalSignals.selectedSignalId;
  const selectedSignal = state.verticalSignals.inventory[selectedSignalId];

  return {
    region: state.verticalSignals.region,
    selectedRoadId: state.filters.selectedRoadId,
    selectedRoadSideId: state.filters.selectedRoadSideId,
    abscissa: state.filters.initialAbscissa,

    selectedFlavorId: state.verticalSignals.selectedFlavorId,
    selectedPaperTypeId: state.verticalSignals.selectedPaperTypeId,

    selectedDate: state.filters.selectedDate,
    images: state.filters.images,

    isEditingRecord: state.tempData.isEditingRecord,

    selectedSignal,
    selectedSignalId,
  };
};

const mapDispatchToProps = {
  addOrUpdateVerticalSignal,
  setSelectedFlavorId,
  clearAllFilters,
  clearVerticalSignalFilters,
  notEditRecord,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerticalSignalsForm);
