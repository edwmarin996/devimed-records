import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Image, StyleSheet, View } from 'react-native';
import MapView, { Marker, UrlTile } from 'react-native-maps';
import { getCoordinates, getVerticalSignalIconById } from '../../utils/utils';
import { getVerticalSignalById } from '../../utils/helpers';
import { getDistance } from 'geolib';
import { IconButton } from 'react-native-paper';
import RNBeep from 'react-native-a-beep';
import config from '../../config/config';
import {
  MeasurementModal,
  BottomCard,
  FabButtons,
  BarcodeScanner,
  PhotoModal,
} from '../../components/VerticalSignals';
import {
  toggleFullScreen,
  changeRegion,
  setConfig,
  hideBarcodeScanner,
  setSelectedSignalId,
} from '../../redux/actions/verticalSignals';

const initialRegion = {
  latitude: 6.174627,
  longitude: -75.349311,
  latitudeDelta: 0.005,
  longitudeDelta: 0.005,
};

export class VerticalSignalsMap extends Component {
  constructor(props) {
    super(props);
    this.mapView = React.createRef();
  }

  goToUserLocation = async () => {
    const coordinates = await getCoordinates(false);
    if (!coordinates) return;
    const camera = { center: coordinates, zoom: 20 };
    this.mapView.current.animateCamera(camera, { duration: 2000 });
  };

  onMapViewLongPress = ({ nativeEvent: { coordinate: center } }) => {
    const camera = { center };
    this.mapView.current.animateCamera(camera, { duration: 300 });

    const config = {
      showBottomCard: false,
      showFullScreenMap: false,
      showScope: true,
      movingSignal: false,
    };

    this.props.setConfig(config);
  };

  onMarkerPress = id => {
    const config = {
      showBottomCard: true,
      showFullScreenMap: false,
      showScope: false,
      movingSignal: false,
    };

    this.props.setSelectedSignalId(id);
    this.props.setConfig(config);
  };

  onBarcodeRead = data => {
    const signal = getVerticalSignalById(data);

    if (signal) {
      RNBeep.beep(false);
      this.props.hideBarcodeScanner();

      const camera = { center: signal.coordinates, zoom: 20 };
      this.mapView.current.animateCamera(camera, { duration: 2000 });

      const config = {
        showBottomCard: true,
        showFullScreenMap: false,
        showScope: false,
        movingSignal: false,
      };

      this.props.setSelectedSignalId(signal.id);
      this.props.setConfig(config);
    }
  };

  render() {
    return (
      <View style={StyleSheet.absoluteFill}>
        <MapView
          initialRegion={initialRegion}
          onRegionChangeComplete={this.props.changeRegion}
          onPress={this.props.toggleFullScreen}
          onLongPress={this.onMapViewLongPress}
          ref={this.mapView}
          maxZoomLevel={22}
          minZoomLevel={10}
          //mapType='satellite'
          showsUserLocation
          rotateEnabled
          showsCompass={false}
          showsMyLocationButton={false}
          style={StyleSheet.absoluteFill}>
          {/* Remote tiles (Devimed Tiles server) */}
          {<UrlTile urlTemplate={config.TILES_URL} />}

          {/* Local tiles (SD card) */}
          {<UrlTile urlTemplate={this.props.localTilesPath} />}

          {this.props.signals.map(signal => (
            <Marker
              coordinate={signal.coordinates}
              onPress={() => this.onMarkerPress(signal.id)}
              /**
               * signal.senal_id is required to update the marker
               * image after the signal is updated.
               *
               * https://github.com/react-native-maps/react-native-maps/issues/1611
               */
              key={signal.id + signal.senal_id}
              tracksViewChanges={false}>
              <Image
                style={{
                  width: this.props.iconsSize,
                  height: this.props.iconsSize,
                }}
                source={getVerticalSignalIconById(signal.id)}
              />
            </Marker>
          ))}
        </MapView>

        <MeasurementModal />
        <BarcodeScanner onBarcodeRead={this.onBarcodeRead} />
        <PhotoModal />

        {!this.props.showFullScreenMap && (
          <>
            {this.props.showScope && (
              <View style={styles.scopeContainer}>
                <Image
                  style={{
                    width: this.props.scopeSize,
                    height: this.props.scopeSize,
                  }}
                  source={require('../../assets/icons/scope.png')}
                />
              </View>
            )}

            <IconButton
              style={styles.menu}
              size={40}
              color='#5F5F5F'
              icon='menu'
              onPress={this.props.navigation.openDrawer}
            />
          </>
        )}

        <View style={styles.Bottom}>
          <FabButtons
            navigation={this.props.navigation}
            goToUserLocation={this.goToUserLocation}
          />
          <BottomCard navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  menu: {
    position: 'absolute',
  },

  scopeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  Bottom: {
    position: 'absolute',
    bottom: 5,
    width: '97%',
    marginHorizontal: '1.5%',
  },
});

const mapStateToProps = state => {
  // Signals selector
  let signals = [];

  if (state.verticalSignals.zoom > config.ZOOM_TO_SHOW_SIGNALS) {
    signals = Object.values(state.verticalSignals.inventory).filter(
      signal =>
        getDistance(signal.coordinates, state.verticalSignals.region) <
        config.SEARCH_REGION_RADIUS,
    );
  }

  return {
    signals: signals,
    flavors: state.verticalSignals.flavors,
    showFullScreenMap: state.verticalSignals.showFullScreenMap,
    showScope: state.verticalSignals.showScope,
    region: state.verticalSignals.region,
    localTilesPath: state.verticalSignals.localTilesPath,
    iconsSize: state.verticalSignals.iconsSize,
    scopeSize: state.verticalSignals.scopeSize,
  };
};

const mapDispatchToProps = {
  toggleFullScreen,
  changeRegion,
  setConfig,
  hideBarcodeScanner,
  setSelectedSignalId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerticalSignalsMap);
