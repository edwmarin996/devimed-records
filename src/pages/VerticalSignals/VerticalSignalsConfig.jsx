import React, { Component } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { IconButton } from 'react-native-paper';
import { Text } from 'react-native';
import { ImageBackground } from 'react-native';
import { ConfigCard, ConfigAccordion } from '../../components/VerticalSignals';
import Alert from '../../components/General/Alert';
import CustomLoading from '../../components/General/CustomLoading';
import { theme } from '../../styles/theme';

export class VerticalSignalsConfig extends Component {
  render() {
    return (
      <ImageBackground
        style={styles.background}
        source={require('../../assets/backgrounds/background_2.png')}>
        <View style={styles.titleContainer}>
          <IconButton
            size={40}
            color='#FFF'
            icon='menu'
            onPress={this.props.navigation.openDrawer}
          />

          <Text style={styles.title}>AJUSTES</Text>
        </View>

        <ConfigCard
          inspection={this.props.inspection}
          inventoryLength={this.props.inventoryLength}
          measurementsLength={this.props.measurementsLength}
          unSyncSignals={this.props.verticalSignalsSyncListLength}
          unSyncMeasurements={this.props.measurementsSyncListLength}
        />

        <CustomLoading
          show={this.props.showLoading}
          progress={this.props.loadingProgress}
          color={theme.palette.primary.main}
        />

        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentContainer}>
          {!this.props.showLoading && (
            <>
              {!this.props.inventoryLength && (
                <Alert type='warning'>
                  No hay ninguna señal vertical, puede importarlas en la pestaña
                  "Inventario".
                </Alert>
              )}

              {!this.props.inspection.id && (
                <Alert type='warning'>
                  No hay ninguna inspección, descargue o cree una en el menú
                  "Inspección".
                </Alert>
              )}
            </>
          )}

          <ConfigAccordion
            unSyncMeasurements={this.props.measurementsSyncListLength}
          />
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
  },

  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  title: {
    color: '#FFF',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 22,
    marginLeft: '25%',
  },

  contentContainer: {
    paddingHorizontal: 20,
  },
});

const mapStateToProps = state => ({
  showLoading: state.tempData.showLoading,
  loadingProgress: state.tempData.loadingProgress,

  inspection: state.verticalSignals.inspection,

  inventoryLength: Object.keys(state.verticalSignals.inventory).length,
  measurementsLength: Object.keys(state.verticalSignals.measurements).length,

  verticalSignalsSyncListLength:
    state.verticalSignals.verticalSignalsSyncList.length +
    state.verticalSignals.deletedVerticalSignalsSyncList.length,

  measurementsSyncListLength: state.verticalSignals.measurementsSyncList.length,
});

export default connect(
  mapStateToProps,
  null,
)(VerticalSignalsConfig);
