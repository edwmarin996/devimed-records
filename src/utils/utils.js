import { store } from '../redux/store/store';
import * as helpers from './helpers';
import { PermissionsAndroid, ToastAndroid, Alert } from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import base64 from 'react-native-base64';
import moment from 'moment';
import { ExternalDirectoryPath } from 'react-native-fs';
import {
  clearAllFilters,
  setFilters as setFiltersRedux,
} from '../redux/actions/filters';

// Moment js settings
import 'moment/locale/es';
import config from '../config/config';
moment.updateLocale(moment.locale(), { invalidDate: 'Sin fecha' });

/**
 * @description Validates that the data configured by the user exists and is correct
 * @param {Object} config - Validation config
 * @param {boolean} [config.checkSector] - Verify that a sector was selected
 * @param {boolean} [config.checkRoad] - Verify that a road was selected
 * @param {boolean} [config.checkRoadWay] - Verify that a roadway was selected
 * @param {boolean} [config.checkRoadSide] - Verify that a road side was selected
 * @param {boolean} [config.checkLineSide] - Verify that a road side was selected
 * @param {boolean} [config.checkAbscissas] - Verify that a valid abscissas was selected
 * @param {boolean} [config.checkSingleAbscissa] - Verify that a abscissa (initialAbscissa) was selected
 * @param {boolean} [config.checkGrassMowGroups] - Verify that at least one grass mow group was selected
 * @param {number} [config.minImages] - Verify that a minimum  number of images have been included
 * @param {number} [config.maxImages] - Verify that a maximum  number of images have been included
 * @returns {Object} Array of errors found during validation, or null if no errors were found
 */
export function validateFilters(config) {
  const { filters } = store.getState();

  let errors = [];

  if (config.checkSector) {
    filters.selectedSectorId
      ? undefined
      : errors.push('Debe seleccionar un sector');
  }

  if (config.checkRoad) {
    filters.selectedRoadId
      ? undefined
      : errors.push('Debe seleccionar una vía');
  }

  if (config.checkRoadWay) {
    filters.selectedRoadWayId
      ? undefined
      : errors.push('Debe seleccionar una calzada');
  }

  if (config.checkRoadSide) {
    filters.selectedRoadSideId
      ? undefined
      : errors.push('Debe seleccionar un costado de la vía');
  }

  if (config.checkLineSide) {
    filters.selectedLineSideId
      ? null
      : errors.push('Debe seleccionar un costado de línea');
  }

  if (config.checkAbscissas) {
    const minAbscissa = filters.selectedRoad?.abscisa_inicial;
    const maxAbscissa = filters.selectedRoad?.abscisa_final;
    const initialAbscissa = filters.initialAbscissa;
    const finalAbscissa = filters.finalAbscissa;

    if (initialAbscissa < minAbscissa) {
      errors.push(`La abscisa inicial debe ser mayor o igual a ${minAbscissa}`);
    } else if (initialAbscissa > maxAbscissa) {
      errors.push(`La abscisa inicial debe ser menor o igual a ${maxAbscissa}`);
    }

    if (finalAbscissa < minAbscissa) {
      errors.push(`La abscisa final debe ser mayor o igual a ${minAbscissa}`);
    } else if (finalAbscissa > maxAbscissa) {
      errors.push(`La abscisa final debe ser menor o igual a ${maxAbscissa}`);
    }
  }

  // To check only the initialAbscissa
  if (config.checkSingleAbscissa) {
    const abscissa = filters.initialAbscissa;

    const minAbscissa = filters.selectedRoad?.abscisa_inicial;
    const maxAbscissa = filters.selectedRoad?.abscisa_final;

    if (abscissa < minAbscissa) {
      errors.push(`La abscisa inicial debe ser mayor o igual a ${minAbscissa}`);
    } else if (abscissa > maxAbscissa) {
      errors.push(`La abscisa inicial debe ser menor o igual a ${maxAbscissa}`);
    } else if (abscissa === null) {
      errors.push('Debe seleccionar una abscissa');
    }
  }

  if (config.minImages) {
    if (filters.images.length < config.minImages) {
      errors.push(`Debe subir al menos ${config.minImages} imagen (es)`);
    }
  }

  if (config.maxImages) {
    if (filters.images.length > config.maxImages) {
      errors.push(`Debe subir como máximo ${config.maxImages} imagen (es)`);
    }
  }

  if (config.checkGrassMowGroups) {
    if (filters.selectedGrassMowGroups.length < 1) {
      errors.push('Debe seleccionar al menos una cuadrilla');
    }
  }

  return errors.length ? errors : null;
}

function isValidField(field) {
  if (field !== undefined && field !== null) {
    return true;
  }

  return false;
}

export function removeEmptyProperties(object) {
  return Object.fromEntries(
    Object.entries(object).filter(([_, v]) => {
      if (Array.isArray(v) && v.length === 0) {
        return false;
      }

      if (v == null || v == undefined || v == '') {
        return false;
      }

      if (typeof v === 'object' && Object.keys(v).length === 0) {
        return false;
      }

      return true;
    }),
  );
}

/**
 * @description check if all the fields of a record are valid,
 * that is, different from null or undefined
 * @param {Object} record  - record that will be validated
 * @returns {Boolean} - recordIsValid
 */
export function validateRecord(record) {
  let recordIsValid = true;

  const nonValidFields = Object.values(record).filter(
    value => !isValidField(value),
  );

  if (nonValidFields.length > 0) {
    recordIsValid = false;
  }

  return recordIsValid;
}

/**
 * @description This function establishes the values ​​of the filters,
 * according to the ids passed.
 *
 * @param {Object} filtersConfig - Card data config
 *
 * @param {Number} [filtersConfig.sectorId]
 * @param {Number} [filtersConfig.roadId]
 * @param {Number} [filtersConfig.roadWayId]
 * @param {Number} [filtersConfig.oldRoadSideId]
 * @param {Number} [filtersConfig.roadSideId]
 * @param {Number} [filtersConfig.lineSideId]
 *
 * @param {Number} [filtersConfig.initialAbscissa]
 * @param {Number} [filtersConfig.finalAbscissa]
 * @param {Object} [filtersConfig.images]
 * @param {String} [filtersConfig.comment]
 * @param {Array} [filtersConfig.grassMowGroups]
 *
 * @param {Boolean} [filtersConfig.isExternalRecord]
 * @param {Date} [filtersConfig.date]
 */
export function setFilters(filtersConfig) {
  const filters = {};

  if (isValidField(filtersConfig.sectorId)) {
    filters.selectedSectorId = filtersConfig.sectorId;
  }

  if (isValidField(filtersConfig.roadId)) {
    filters.selectedRoadId = filtersConfig.roadId;
  }

  if (isValidField(filtersConfig.roadWayId)) {
    filters.selectedRoadWayId = filtersConfig.roadWayId;
  }

  // Get roadSide OR oldRoadSide
  if (isValidField(filtersConfig.oldRoadSideId)) {
    filters.selectedRoadSideId = filtersConfig.oldRoadSideId;
  } else if (isValidField(filtersConfig.roadSideId)) {
    filters.selectedRoadSideId = filtersConfig.roadSideId;
  }

  if (isValidField(filtersConfig.lineSideId)) {
    filters.selectedLineSideId = filtersConfig.lineSideId;
  }

  if (isValidField(filtersConfig.initialAbscissa)) {
    filters.initialAbscissa = filtersConfig.initialAbscissa;
  }

  if (isValidField(filtersConfig.finalAbscissa)) {
    filters.finalAbscissa = filtersConfig.finalAbscissa;
  }

  if (isValidField(filtersConfig.comment)) {
    filters.comment = filtersConfig.comment;
  }

  if (isValidField(filtersConfig.images)) {
    filters.images = filtersConfig.images.reverse();
  }

  if (isValidField(filtersConfig.isExternalRecord)) {
    filters.isExternalRecord = filtersConfig.isExternalRecord;
  }

  if (isValidField(filtersConfig.grassMowGroups)) {
    filters.selectedGrassMowGroups = filtersConfig.grassMowGroups;
  }

  if (isValidField(filtersConfig.date)) {
    filters.selectedDate = filtersConfig.date;
  }
  store.dispatch(clearAllFilters());
  store.dispatch(setFiltersRedux(filters));
}

/**
 * @description This function is used to format the data
 * that will be shown on the cards, in the pages to view records
 *
 * @param {Object} recordData - Card data config
 *
 * @param {Number} [recordData.id] - Id of the record
 * @param {Number} [recordData.syncState] - Synchronization state of the record
 * @param {Number} [recordData.date] - Record creation date
 *
 * @param {Number} [recordData.sectorId]
 * @param {Number} [recordData.roadId]
 * @param {Number} [recordData.roadWayId]
 * @param {Number} [recordData.oldRoadSideId]
 * @param {Number} [recordData.roadSideId]
 * @param {Number} [recordData.lineSideId]
 *
 * @param {Number} [recordData.initialAbscissa]
 * @param {Number} [recordData.abscissa]
 * @param {Number} [recordData.finalAbscissa]
 * @param {Object} [recordData.images]
 * @param {String} [recordData.comment]
 *
 * @param {Array} [recordData.grassMowGroups]
 * @param {Object} otherParams - Objet with other parameters to append to the card
 *
 * @returns {Object} Object with the formatted data
 */
export function getCardData(recordData, otherParams = {}) {
  let cardData = {
    id: recordData.id,
    syncState: recordData.syncState,
    date: recordData.date,
    body: {},
    ...otherParams,
  };

  const { appData } = store.getState();

  // Save in the body keys that will be showed in the cards
  let body = {};

  if (isValidField(recordData.sectorId)) {
    body.Sector = appData.sectors[recordData.sectorId].codigo;
  }

  if (isValidField(recordData.roadId)) {
    body.Vía = appData.roads[recordData.roadId].nombre;
  }

  if (isValidField(recordData.roadWayId)) {
    body.Calzada = appData.roadWays[recordData.roadWayId].nombre;
  }

  // Get roadSide OR oldRoadSide
  if (isValidField(recordData.oldRoadSideId)) {
    body.Costado = appData.oldRoadSides[recordData.oldRoadSideId].nombre;
  } else if (isValidField(recordData.roadSideId)) {
    body.Costado = appData.roadSides[recordData.roadSideId].nombre;
  }

  if (isValidField(recordData.lineSideId)) {
    body.Línea = appData.lineSides[recordData.lineSideId].nombre;
  }

  // For single abscissa
  if (isValidField(recordData.abscissa)) {
    body.Abscisa = formatAbscissa(recordData.abscissa);
  }

  if (isValidField(recordData.initialAbscissa)) {
    body['Abscisa inicial'] = formatAbscissa(recordData.initialAbscissa);
  }

  if (isValidField(recordData.finalAbscissa)) {
    body['Abscisa final'] = formatAbscissa(recordData.finalAbscissa);
  }

  if (isValidField(recordData.grassMowGroups)) {
    body.Cuadrillas = helpers.getGrassMowGroupsById(recordData.grassMowGroups);
  }

  if (isValidField(recordData.comment)) {
    body.Comentario = recordData.comment;
  }

  if (isValidField(recordData.images)) {
    cardData.images = recordData.images;
  }

  cardData.body = body;
  return cardData;
}

// Get gps location if available
export const getCoordinates = async (geoJsonFormat = true) => {
  const hasPermissions = await PermissionsAndroid.requestMultiple([
    'android.permission.ACCESS_COARSE_LOCATION',
    'android.permission.ACCESS_FINE_LOCATION',
  ]);

  // Check user permissions
  if (
    hasPermissions['android.permission.ACCESS_COARSE_LOCATION'] !== 'granted' ||
    hasPermissions['android.permission.ACCESS_FINE_LOCATION'] !== 'granted'
  ) {
    return null;
  }

  return new Promise(resolve => {
    Geolocation.getCurrentPosition(
      position => {
        if (geoJsonFormat) {
          resolve(
            `POINT (${position.coords.longitude} ${position.coords.latitude})`,
          );
          return;
        }

        resolve({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          accuracy: position.coords.accuracy,
          altitude: position.coords.altitude,
        });
      },
      () => {
        resolve(null);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 0 },
    );
  });
};

/*---------------------------------------------------------------
 *                         Other functions
 *----------------------------------------------------------------*/

export function isEditingRecord() {
  const { tempData } = store.getState();
  return tempData.isEditingRecord;
}

export function formatAbscissa(abscissa, prefix = '') {
  const kilometer = Math.floor(abscissa / 1000);
  const meters = String(abscissa % 1000).padStart(3, '0');
  return `${prefix}${kilometer} + ${meters}`;
}

export const showMessage = message => {
  ToastAndroid.showWithGravity(message, ToastAndroid.LONG, ToastAndroid.BOTTOM);
};

export const showAlert = message => {
  Alert.alert('¡Alerta!', message);
};

export const getCoordinatesFromRegion = region => {
  return {
    latitude: region.latitude,
    longitude: region.longitude,
  };
};

export const getGeoJsonCoordinatesFromRegion = region => {
  return {
    type: 'Point',
    coordinates: [region.longitude, region.latitude],
  };
};

export const formatDate = (date, includeYear = false) => {
  if (!date) {
    //  return 'No registra';
  }

  if (includeYear) {
    return moment(date)
      .locale('es')
      .format('DD [de] MMMM [de] YYYY');
  }

  return moment(date)
    .locale('es')
    .format('DD [de] MMMM');
};

/*---------------------------------------------------------------
 *                         Vertical Signals
 *----------------------------------------------------------------*/

export const getFlavorIconByCode = code => {
  return { uri: `asset:/vertical_signals/${code}.png` };
};

export const getVerticalSignalIconById = id => {
  const {
    verticalSignals: { flavors, inventory },
  } = store.getState();

  const signal = inventory[id];
  const flavor = flavors[signal.senal_id];

  return getFlavorIconByCode(flavor.codigo);
};

/**
 * @param {String} verticalSignalId id of the vertical signal
 * @param {Object} options options of the functions
 * @param {Boolean} options.remote return high quality image from remote server
 */
export const getVerticalSignalImage = (
  verticalSignalId,
  options = { remote: false },
) => {
  const {
    verticalSignals: { inventory },
  } = store.getState();

  const verticalSignal = inventory[verticalSignalId];

  if (!verticalSignal) {
    return;
  }

  const { photoName, photoUrl, photoNameAlt } = verticalSignal;

  // For local image (captured by user)
  if (photoNameAlt) {
    return { uri: photoNameAlt.uri };
  }

  if (photoName) {
    // For remote image
    if (options.remote) {
      return { uri: `${photoUrl}/${photoName}` };
    }

    // For local image (downloaded by user)
    return {
      uri: `file://${ExternalDirectoryPath}/images/${
        config.LOCAL_IMAGE_RESOLUTION
      }/${photoName}`,
    };
  }

  return require('../assets/icons/road-sign-gray-padding.png');
};

const getShortUUID = (uuid, numberBytes) => {
  const uuidHex = uuid.replace('-', '');
  const index = uuidHex.length - 2 * numberBytes;
  if (index < 0) {
    throw new Error('numberBytes must be less than or equal to 16');
  }

  return uuidHex
    .split('')
    .slice(index)
    .join('');
};

const hexToBase64 = _hexString => {
  const hexString = _hexString.length % 2 === 0 ? _hexString : `0${_hexString}`;
  const hexArray = hexString.match(/\w{2}/g);

  return base64.encode(
    hexArray
      .map(hex => {
        const number = parseInt(hex, 16);
        return String.fromCharCode(number);
      })
      .join(''),
  );
};

export const uuidToShortBase64 = (uuid, numberBytes = 6) => {
  const shortUUID = getShortUUID(uuid, numberBytes);
  const b64ShortUUID = hexToBase64(shortUUID);
  return b64ShortUUID;
};
