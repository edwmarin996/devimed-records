import { store } from '../redux/store/store';

/*---------------------------------------------------------------
 *     Helper functions, find an object by id and return it
 *---------------------------------------------------------------*/

export function getRoadNameById(id) {
  const {
    appData: { roads },
  } = store.getState();

  return roads[id].nombre;
}

export function getRoadWayNameById(id) {
  const {
    appData: { roadWays },
  } = store.getState();

  return roadWays[id].nombre;
}

export function getOldRoadSideNameById(id) {
  const {
    appData: { oldRoadSides },
  } = store.getState();

  return oldRoadSides[id].nombre;
}

export function getRoadSideNameById(id) {
  const {
    appData: { roadSides },
  } = store.getState();

  return roadSides[id].nombre;
}

export function getLineSideNameById(id) {
  const {
    appData: { lineSides },
  } = store.getState();

  lineSides[id].nombre;
}

export function getGrassMowGroupsById(grassMowGroupsIds) {
  const {
    appData: { grassMowGroups },
  } = store.getState();

  const grassMowGroupNames = grassMowGroupsIds.map(target => {
    const grassMowGroup = grassMowGroups.find(({ id }) => id === target);
    return grassMowGroup.nombre;
  });

  return grassMowGroupNames.join(', ');
}

export function getFlavorNameById(id) {
  const {
    verticalSignals: { flavors },
  } = store.getState();

  return flavors[id].codigo;
}

export function getPaperTypeNameById(id) {
  const {
    verticalSignals: { paperTypes },
  } = store.getState();

  return paperTypes[id].nombre;
}

export function getVerticalSignalById(id) {
  const {
    verticalSignals: { inventory },
  } = store.getState();

  return inventory[id];
}
