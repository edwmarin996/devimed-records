import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Alert({ children, type, transparency = '33' }) {
  let color;
  let icon;
  switch (type) {
    case 'success':
      color = '#64B968';
      icon = 'checkbox-marked-circle-outline';
      break;

    case 'info':
      color = '#0172FB';
      icon = 'information-outline';
      break;

    case 'warning':
      color = '#FFAA00';
      icon = 'alert-outline';
      break;

    case 'error':
      color = '#FF3D71';
      icon = 'alert-circle-outline';
      break;

    default:
      color = '#0172FB';
      icon = 'information-outline';
  }

  return (
    <View
      style={[
        styles.container,
        { backgroundColor: `${color}${transparency}` },
      ]}>
      <Icon name={icon} size={30} color={color} />
      <Text style={styles.text}>{children}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: 5,
    padding: 8,
    marginVertical: 5,
  },

  text: {
    flex: 0.9,
    color: '#6D6D6D',
  },
});
