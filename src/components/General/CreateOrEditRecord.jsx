import React from 'react';
import { Image, StyleSheet, ScrollView, View } from 'react-native';
import { Layout, Button } from '@ui-kitten/components';
import { NavBar, ErrorsModal } from '.';
import { Button as PaperButton } from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { theme } from '../../styles/theme';

export default function CreateOrEditRecord(props) {
  return (
    <Layout style={styles.layout}>
      <ScrollView
        keyboardShouldPersistTaps='handled'
        showsVerticalScrollIndicator={false}>
        <NavBar
          title={props.navBarTitle}
          category={props.navBarCategory}
          menu={props.navBarMenu}
        />

        {props.image && (
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={props.image} />
          </View>
        )}

        {
          // Filters
          props.children
        }

        <ErrorsModal
          closeModal={props.closeModal}
          show={props.showModal}
          errors={props.errors}
        />

        {props.buttons && (
          <View style={styles.buttons}>
            <PaperButton
              /*status={props.buttons.cancel.status || 'danger'}
              appearance='outline'
              */

              mode='outlined'
              onPress={props.buttons.cancel.handler}
              color={theme.palette.error.main}
              style={styles.button}>
              {props.buttons.cancel.title}
            </PaperButton>

            <PaperButton
              /* status={props.buttons.ok.status || 'primary'}
              appearance='outline'
              */

              mode='outlined'
              onPress={props.buttons.ok.handler}
              color={theme.palette.primary.main}
              style={styles.button}>
              {props.buttons.ok.title}
            </PaperButton>
          </View>
        )}

        {props.backButton && (
          <PaperButton
            mode='outlined'
            onPress={props.backButton}
            color={theme.palette.primary.main}
            style={styles.button}>
            Atrás
          </PaperButton>
        )}
      </ScrollView>
    </Layout>
  );
}

const styles = StyleSheet.create({
  layout: {
    flex: 1,
    paddingHorizontal: 20,
  },

  imageContainer: {
    alignItems: 'center',
  },

  image: {
    width: wp('50%'),
    height: hp('25%'),
    marginTop: 15,
    overflow: 'hidden',
  },

  buttons: {
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'space-between',
    width: '100%',
  },

  button: {
    width: '48%',
  },
});
