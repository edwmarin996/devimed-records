import React from 'react';
import { StyleSheet, View, Image, Linking } from 'react-native';
import { Text, Card, Tooltip } from '@ui-kitten/components';
import syncStates from '../../api/syncStates';
import Icon from 'react-native-vector-icons/FontAwesome5';
import config from '../../config/config';
import { IconButton, TouchableRipple, HelperText } from 'react-native-paper';
import { formatDate } from '../../utils/utils';

export default function RecordCard({
  record,
  handleRemoveRecord,
  handleEditRecord,
}) {
  return (
    <Card
      disabled
      style={styles.card}
      header={() => getHeader(record, handleRemoveRecord, handleEditRecord)}
      status={
        record.syncState === syncStates.isFullySynchronized
          ? 'success'
          : record.syncState === syncStates.isPartiallySynchronized
          ? 'warning'
          : 'danger'
      }>
      <View style={styles.cardBody}>
        <View style={styles.cardLeft}>
          {
            // Show map icon with link to the record coordinates
          }
          {record.isExternalRecord && (
            <>
              <TouchableRipple
                style={styles.ripple}
                onPress={() => Linking.openURL(getUrlMaps(record.coordenada))}>
                <>
                  <Image
                    style={styles.miniature}
                    source={require('../../assets/icons/map.png')}
                  />
                  <HelperText>Ver en el mapa</HelperText>
                </>
              </TouchableRipple>
            </>
          )}
          {Object.keys(record.body).map(key => (
            <Text key={key} style={styles.textBold}>
              {key}: <Text>{record.body[key]}</Text>
            </Text>
          ))}
        </View>

        <View style={styles.cardRight}>
          {record?.images
            ?.slice(0, 4)
            .map(image => (
              <Image key={image.id} style={styles.miniature} source={image} />
            ))}
        </View>
      </View>
    </Card>
  );
}

const getUrlMaps = coordinate => {
  coordinate = coordinate.split(' ');
  const longitude = coordinate[1];
  const latitude = coordinate[2];

  const googleMapsUrl = `https://www.google.com/maps/place/${latitude},${longitude}`;
  return googleMapsUrl;
};

const getHeader = (record, handleRemoveRecord, handleEditRecord) => {
  return (
    <View style={styles.header}>
      {record.syncState === syncStates.isNotReadyToSynchronize && (
        <ToolTipWarning />
      )}

      <Text style={styles.date}>{formatDate(record.date)}</Text>

      {showButtons(record.date) && (
        <View style={styles.buttonsContainer}>
          <IconButton
            size={26}
            icon='pencil'
            color='#242ADB'
            onPress={() => handleEditRecord(record.id)}
          />

          <IconButton
            size={26}
            icon='trash-can'
            color='#DB2A2A'
            onPress={() => handleRemoveRecord(record.id, record.syncState)}
          />
        </View>
      )}
    </View>
  );
};

export function ToolTipWarning() {
  const [visible, setVisible] = React.useState(false);

  const iconWarning = () => (
    <Icon
      onPress={() => setVisible(true)}
      name='exclamation-triangle'
      color='#FFAA00'
      size={25}
    />
  );

  return (
    <Tooltip
      onBackdropPress={() => setVisible(false)}
      anchor={iconWarning}
      visible={visible}>
      {'Este registro no está completo.'}
    </Tooltip>
  );
}

function showButtons(date) {
  var dateNow = new Date();
  dateNow.setTime(
    dateNow.getTime() -
      60000 * config.MINUTES_BEFORE_DELETE_OPTION_DISAPPEAR_FROM_RECORDS,
  );
  return new Date(date) < dateNow ? false : true;
}

const styles = StyleSheet.create({
  card: {
    marginHorizontal: 6,
    marginVertical: 10,
    backgroundColor: '#FBFCFD',

    borderWidth: 1,
    borderRadius: 5,
    shadowColor: '#000',

    // ! BUG: screen blink when switch between tabs
    elevation: 5,
  },

  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },

  date: {
    width: '70%',
    fontFamily: 'sans-serif-condensed',
    fontWeight: 'bold',
    fontSize: 18,
  },

  buttonsContainer: {
    width: '30%',
    flexDirection: 'row',
  },

  textBold: {
    fontFamily: 'sans-serif-condensed',
    fontWeight: 'bold',
  },

  cardBody: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  cardLeft: {
    width: '70%',
  },

  cardRight: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },

  ripple: {
    alignItems: 'center',
  },

  miniature: {
    width: 40,
    height: 40,
    margin: 1,
    borderRadius: 2,
  },
});
