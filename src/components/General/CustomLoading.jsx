import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ProgressBar } from 'react-native-paper';
import { theme } from '../../styles/theme';

export default function CustomLoading({
  show,
  progress,
  color = theme.palette.primary.main,
}) {
  if (!show) {
    return null;
  }

  const roundedProgress = Math.round(100 * progress);

  return (
    <View style={styles.container}>
      <ProgressBar
        indeterminate={!progress}
        progress={progress}
        color={color}
      />

      <Text style={[styles.loadingText, { color }]}>
        {roundedProgress ? `${roundedProgress} %` : 'Por favor espere...'}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    paddingHorizontal: 20,
  },

  loadingText: {
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
