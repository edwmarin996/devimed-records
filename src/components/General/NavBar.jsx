import React from 'react';
import { TouchableOpacity, View, StyleSheet } from 'react-native';
import { Text } from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { theme } from '../../styles/theme';

export function NavBar(props) {
  const customStyle = props.menu ? menuStyle : noMenuStyle;
  const dividerStyle = props.divider === false ? null : styles.divider;

  return (
    <View style={[styles.container, dividerStyle]}>
      <TouchableOpacity
        onPress={props.menu}
        disabled={props.isEditingRecord}
        style={customStyle.icon}>
        <Icon
          name='bars'
          size={35}
          color={props.isEditingRecord ? '#EDEDED' : '#000'}
        />
      </TouchableOpacity>

      <View style={customStyle.titleWidth}>
        <Text category={props.category} style={styles.title}>
          {props.title}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingBottom: 10,
    marginTop: 10,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  divider: {
    borderBottomWidth: 1.5,
    borderColor: '#EDF1F7',
  },

  title: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18,
  },
});

const menuStyle = StyleSheet.create({
  icon: {
    alignItems: 'center',
    flex: 0.1,
  },

  titleWidth: {
    alignItems: 'center',
    flex: 0.9,
  },
});

const noMenuStyle = StyleSheet.create({
  icon: {
    display: 'none',
  },

  titleWidth: {
    alignItems: 'center',
    flex: 1,
  },
});

const mapStateToProps = state => ({
  isEditingRecord: state.tempData.isEditingRecord,
});

export default connect(
  mapStateToProps,
  null,
)(NavBar);
