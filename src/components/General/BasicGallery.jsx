import React from 'react';
import { View, StyleSheet, ScrollView, Image, Text } from 'react-native';
import { connect } from 'react-redux';
import { removeImage } from '../../redux/actions/filters';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { IconButton } from 'react-native-paper';

export function BasicGallery(props) {
  const removeImage = image => {
    //  Call callback to notify deleted image
    if (props.onRemoveImage) {
      props.onRemoveImage(image);
    }

    props.removeImage(image.id);
  };

  if (props.images.length > 0) {
    return (
      <View style={styles.content}>
        <ScrollView
          contentContainerStyle={styles.scrollViewContent}
          showsHorizontalScrollIndicator={false}
          horizontal>
          {props.images.map(image => (
            <View key={image.id} style={styles.imageContainer}>
              <Image style={styles.image} source={image} />

              <IconButton
                style={styles.icon}
                icon='close-circle'
                size={28}
                color='#F33'
                onPress={() => removeImage(image)}
              />
            </View>
          ))}
        </ScrollView>
      </View>
    );
  }

  return (
    <View style={styles.content}>
      <Image
        source={require('../../assets/icons/camera.png')}
        style={styles.camera}
      />
      <Text style={styles.text}>
        {'Aún no hay imágenes \n captura algunas y aparecerán aquí.'}
      </Text>
    </View>
  );
}

const mapStateToProps = state => ({
  images: state.filters.images,
});

const mapDispatchToProps = {
  removeImage,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BasicGallery);

const styles = StyleSheet.create({
  content: {
    padding: 2,
    height: wp('40%'),
    borderWidth: 1.5,
    borderStyle: 'dashed',
    borderRadius: 1,
    borderColor: '#7D7D7D',

    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },

  scrollViewContent: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageContainer: {
    backgroundColor: '#FFF',
    shadowColor: '#000',
    elevation: 4,
    borderRadius: 4,
    marginHorizontal: 10,
  },

  image: {
    width: wp('30%'),
    height: wp('30%'),
    borderRadius: 4,
  },

  icon: {
    position: 'absolute',
    right: -20,
    top: -20,
    backgroundColor: '#FFF',
  },

  camera: {
    width: wp('20%'),
    height: wp('20%'),
  },

  text: {
    textAlign: 'center',
    color: '#7D7D7D',
    fontSize: 12,
    fontFamily: 'sans-serif-condensed',
  },
});
