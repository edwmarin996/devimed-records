import React from 'react';
import { Card, Text, Modal, Button } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default function ErrorsModal({ show, errors, closeModal }) {
  return (
    <Modal
      visible={show}
      style={styles.modal}
      backdropStyle={styles.backdrop}
      onBackdropPress={closeModal}>
      <Card
        header={() => (
          <Text style={styles.cardHeader} category='h5'>
            Cuidado
          </Text>
        )}
        style={styles.errorsCard}
        status='danger'>
        {errors?.map((error, idx) => (
          <Text category='p1' style={styles.text} key={idx}>
            {'\u2022'} {error}
          </Text>
        ))}
        <Button
          style={styles.button}
          onPress={closeModal}
          appearance='outline'
          status='danger'>
          OK
        </Button>
      </Card>
    </Modal>
  );
}

const styles = StyleSheet.create({
  errorsCard: {
    width: wp('90%'),
    marginVertical: 10,
  },

  button: {
    marginTop: 20,
  },

  cardHeader: {
    textAlign: 'center',
    marginVertical: 10,
    fontFamily: 'Montserrat-Regular',
  },

  text: {
    fontFamily: 'sans-serif-condensed',
  },

  modal: {
    paddingHorizontal: 25,
    alignItems: 'center',
  },

  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
});
