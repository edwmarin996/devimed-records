import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Text } from '@ui-kitten/components';

export default function EmptyRecordList() {
  return (
    <View style={styles.voidContent}>
      <Image
        style={styles.image}
        source={require('../../assets/icons/camera.png')}
      />
      <Text appearance='hint' category='h5'>
        No hay imágenes
      </Text>
      <Text style={styles.textAlert} appearance='hint'>
        Capture algunas imágenes o {'\n'} selecciónelas desde su galería.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  voidContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },

  textAlert: {
    textAlign: 'center',
  },
});
