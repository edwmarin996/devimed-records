import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Text, Divider } from '@ui-kitten/components';

export default function SyncStatusLabels() {
  return (
    <View style={styles.container}>
      <Text style={styles.success}>
        {'\u2501\u2501'}
        <Text> Completamente sincronizado</Text>
      </Text>

      <Text style={styles.warning}>
        {'\u2501\u2501'}
        <Text> Parcialmente sincronizado</Text>
      </Text>

      <Text style={styles.danger}>
        {'\u2501\u2501'}
        <Text> Sin sincronizar</Text>
      </Text>

      <Divider style={styles.divider} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 7,
  },

  success: {
    color: '#00E096',
  },

  warning: {
    color: '#FFAA00',
  },

  danger: {
    color: '#FF3D71',
  },

  divider: {
    height: 1.5,
  },
});
