import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import { Text } from '@ui-kitten/components';

export default function EmptyRecordList() {
  return (
    <View style={styles.voidContent}>
      <Image
        style={styles.image}
        source={require('../../assets/icons/clipboard.png')}
      />
      <Text category='h6'>¡Vaya! aún no existen registros.</Text>
      <Text style={styles.textAlert} appearance='hint'>
        Puedes crear uno en la pestaña {'\n'} Agregar - Editar.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  voidContent: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },

  textAlert: {
    textAlign: 'center',
  },
});
