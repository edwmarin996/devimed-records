import React from 'react';
import EmptyRecordList from './EmptyRecordList';
import RecordCard from './RecordCard';
import SyncStatusLabels from './SyncStatusLabels';
import { FlatList } from 'react-native-gesture-handler';

export default function RecordList(props) {
  if (props.records.length === 0) return <EmptyRecordList />;

  const Item = ({ item: record }) => (
    <RecordCard
      key={record.id}
      record={props.formatRecord(record)}
      handleRemoveRecord={props.handleRemoveRecord}
      handleEditRecord={props.handleEditRecord}
    />
  );

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      ListHeaderComponent={SyncStatusLabels}
      renderItem={Item}
      data={props.records}
      keyExtractor={record => record.id}
      initialNumToRender={10}
    />
  );
}
