import React, { Component } from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { IconButton } from 'react-native-paper';
import {
  removeImage,
  updateImage,
  clearTags,
  setTags,
} from '../../redux/actions/filters';
import { SelectTags } from '../Filters';
import syncStates from '../../api/syncStates';

export class CarouselGallery extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
      tagsModalVisible: false,
    };

    this.imageToAddTags = {};
  }

  removeImage = image => {
    //  Call callback function to notify deleted image
    if (this.props.onRemoveImage) {
      this.props.onRemoveImage(image);
    }

    this.props.removeImage(image.id);
  };

  onSaveTags = tags => {
    this.imageToAddTags.tags = tags;
    this.imageToAddTags.meta.syncState = syncStates.isNotSynchronized;
    this.props.updateImage(this.imageToAddTags);
    this.setState({ tagsModalVisible: false });
    this.imageToAddTags = {};
  };

  openTagsModal = image => {
    this.imageToAddTags = image;
    this.props.setTags(image.tags || []);
    this.setState({ tagsModalVisible: true });
  };

  getTagsNames = tagIdArr => {
    const tagNames = tagIdArr.map(
      tagId => this.props.tags.find(tag => tag.id === tagId).nombre,
    );

    return tagNames.join(', ').substring(0, 20) + '...';
  };

  renderItem = ({ item: image }) => {
    return (
      <View style={styles.card}>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={image} />
        </View>

        <View style={styles.footer}>
          <IconButton
            color={image.tags && image.tags.length > 0 ? '#FFF' : '#F00'}
            onPress={() => this.openTagsModal(image)}
            size={30}
            icon={
              image.tags && image.tags.length > 0
                ? 'tag-text-outline'
                : 'tag-outline'
            }
          />

          {image.tags && image.tags.length > 0 && (
            <Text style={styles.tags}>{this.getTagsNames(image.tags)}</Text>
          )}

          <IconButton
            color='#FFF'
            onPress={() => this.removeImage(image)}
            size={30}
            icon='trash-can-outline'
          />
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.carousel}>
        <SelectTags
          onSaveTags={this.onSaveTags}
          visible={this.state.tagsModalVisible}
        />

        <Carousel
          layout={'default'}
          ref={ref => (this.carousel = ref)}
          data={this.props.images}
          sliderWidth={wp('100%')}
          itemWidth={wp('80%')}
          renderItem={this.renderItem}
          onSnapToItem={index => this.setState({ activeIndex: index })}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  carousel: {
    alignItems: 'center',
  },

  card: {
    backgroundColor: '#1E1D1F',
    padding: 1.5,
    width: wp('80%'),
    height: hp('55%'),

    borderRadius: 10,
  },

  imageContainer: {
    flex: 0.9,
  },

  image: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },

  footer: {
    flex: 0.12,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  tags: {
    color: '#DDD',
    width: '50%',
  },
});

const mapStateToProps = state => ({
  images: state.filters.images,
  tags: state.appData.tags,
});

const mapDispatchToProps = {
  removeImage,
  updateImage,
  clearTags,
  setTags,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CarouselGallery);
