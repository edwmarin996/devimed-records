import React, { useState } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, View } from 'react-native';
import { Input, Modal, Card, Text } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { changeComment } from '../../redux/actions/filters';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Button as PaperButton } from 'react-native-paper';

export function AddComment(props) {
  const [visible, setVisible] = useState(false);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => setVisible(true)}
        style={styles.customButton}>
        <Text style={styles.buttonText}>
          {props.comment
            ? props.comment.substring(0, 40) + '...'
            : 'Agregue información adicional'}
        </Text>
      </TouchableOpacity>

      <Modal
        onBackdropPress={() => setVisible(false)}
        backdropStyle={styles.backdrop}
        style={styles.modal}
        visible={visible}>
        <Card style={styles.card} disabled>
          <ScrollView
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps='handled'>
            <Text style={styles.title} category='h6' appearance='hint'>
              Ingrese la información adicional a continuación
            </Text>

            <Input
              onChangeText={props.changeComment}
              value={props.comment}
              multiline
              maxLength={1500}
              textAlignVertical='top'
              style={styles.input}
              placeholder={'\n\n\n\n\n\n'}
              autoFocus={true}
            />

            <PaperButton
              mode='text'
              color='black'
              onPress={() => setVisible(false)}>
              Ok
            </PaperButton>
          </ScrollView>
        </Card>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },

  card: {
    height: hp('40%'),
  },

  customButton: {
    marginVertical: 5,
    backgroundColor: '#F8F9FD',
    borderColor: '#E4E9F2',
    borderWidth: 1,
    borderRadius: 5,
    height: 40,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: 20,
  },

  buttonText: {
    color: '#8F9AB1',
  },

  modal: {
    paddingHorizontal: 20,
  },

  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },

  title: {
    textAlign: 'center',
    paddingBottom: 5,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1.5,
  },

  input: {
    marginVertical: 15,
  },
});

const mapStateToProps = state => ({
  comment: state.filters.comment,
});

const mapDispatchToProps = {
  changeComment,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddComment);
