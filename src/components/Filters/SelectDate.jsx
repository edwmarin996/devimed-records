import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Platform, Text } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { formatDate } from '../../utils/utils';
import { StyleSheet } from 'react-native';
import { setDate } from '../../redux/actions/filters';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { TouchableRipple } from 'react-native-paper';

const SelectDate = ({ selectedDate, setDate }) => {
  const [show, setShow] = useState(false);

  const onChange = (_, date) => {
    setShow(Platform.OS === 'ios');

    if (!date) {
      return;
    }

    date.setHours(0, 0, 0, 0);
    setDate(date.toISOString());
  };

  return (
    <>
      <TouchableRipple
        rippleColor='#8F9BB3'
        onPress={() => setShow(true)}
        style={styles.customButton}>
        <>
          <Text style={styles.buttonText}>
            {selectedDate
              ? formatDate(selectedDate, true)
              : 'Seleccione una fecha'}
          </Text>
          <Icon size={25} color='#8F9BB3' style={styles.icon} name='calendar' />
        </>
      </TouchableRipple>

      {show && (
        <DateTimePicker
          value={selectedDate ? new Date(selectedDate) : new Date()}
          mode='date'
          onChange={onChange}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  customButton: {
    marginVertical: 5,
    backgroundColor: '#F8F9FD',
    borderColor: '#E4E9F2',
    borderWidth: 1,
    borderRadius: 5,
    height: 40,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 20,
    flexDirection: 'row',
  },

  icon: {
    right: 8,
    position: 'absolute',
  },

  buttonText: {
    color: '#8F9AB1',
  },
});

const mapStateToProps = state => ({
  selectedDate: state.filters.selectedDate,
});

const mapDispatchToProps = {
  setDate,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectDate);
