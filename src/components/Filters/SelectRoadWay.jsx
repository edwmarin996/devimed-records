import React, { Component } from 'react';
import { Select, SelectItem } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { setRoadWayId } from '../../redux/actions/filters';

export class SelectRoadWay extends Component {
  selectRoadWay = selectedIndex => {
    const selectedRoadWayId = this.props.roadWays[selectedIndex.row].id;
    this.props.setRoadWayId(selectedRoadWayId);
  };

  render() {
    return (
      <Select
        placeholder='Seleccione una calzada'
        value={this.props.selectedRoadWay?.nombre || ''}
        onSelect={this.selectRoadWay}
        style={styles.select}>
        {this.props.roadWays.map(roadWay => (
          <SelectItem key={roadWay.id} title={roadWay.nombre} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => ({
  roadWays: Object.values(state.appData.roadWays),
  selectedRoadWay: state.appData.roadWays[state.filters.selectedRoadWayId],
});

const mapDispatchToProps = {
  setRoadWayId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectRoadWay);
