import React, { Component } from 'react';
import { Select, SelectItem } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { setRoadSideId } from '../../redux/actions/filters';

export class SelectRoadSide extends Component {
  selectRoadSide = selectedIndex => {
    const selectedRoadSideId = this.props.roadSides[selectedIndex.row].id;
    this.props.setRoadSideId(selectedRoadSideId);
  };

  render() {
    return (
      <Select
        placeholder='Seleccione un costado'
        value={this.props.selectedRoadSide?.nombre || ''}
        onSelect={this.selectRoadSide}
        style={styles.select}>
        {this.props.roadSides.map(roadSide => (
          <SelectItem key={roadSide.id} title={roadSide.nombre} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => ({
  roadSides: Object.values(state.appData.roadSides),
  selectedRoadSide: state.appData.roadSides[state.filters.selectedRoadSideId],
});

const mapDispatchToProps = {
  setRoadSideId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectRoadSide);
