import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { addImage } from '../../redux/actions/filters';
import { v4 as uuid } from 'uuid';
import syncStates from '../../api/syncStates';
import ImageResizer from 'react-native-image-resizer';
import config from '../../config/config';
import { IconButton, Button } from 'react-native-paper';
import { theme } from '../../styles/theme';

const imagePickerOptions = {
  noData: true,
  storageOptions: {
    skipBackup: true,
    path: 'DevimedImages',
  },
};

export class SelectImages extends Component {
  async OptimizeImage(image) {
    // Compress the image if necessary
    if (image.fileSize / 1000 > config.MAX_IMAGE_SIZE_KB) {
      try {
        const imageResizerResponse = await ImageResizer.createResizedImage(
          image.uri,
          1240,
          1240,
          'JPEG',
          config.COMPRESSED_IMAGES_QUALITY,
          image.originalRotation,
          null,
          true,
        );

        const compressedImage = {
          fileName: imageResizerResponse.name,
          fileSize: imageResizerResponse.size,
          height: imageResizerResponse.height,
          width: imageResizerResponse.width,
          uri: imageResizerResponse.uri,
          //originalRotation: imageResizerResponse.rotation,
          type: image.type,
        };

        return compressedImage;
      } catch (error) {
        return image;
      }
    }

    return image;
  }

  // Take a picture from device camera
  takePicture = () => {
    ImagePicker.launchCamera(imagePickerOptions, async response => {
      if (response.uri) {
        const optimizedImage = await this.OptimizeImage(response);

        this.props.addImage({
          ...optimizedImage,
          id: uuid(),
          meta: { syncState: syncStates.isNotSynchronized },
        });
      }
    });
  };

  // Select a image from device gallery
  selectImage = () => {
    ImagePicker.launchImageLibrary(imagePickerOptions, async response => {
      if (response.uri) {
        response.uri = `file://${response.path}`;
        const optimizedImage = await this.OptimizeImage(response);

        this.props.addImage({
          ...optimizedImage,
          id: uuid(),
          meta: { syncState: syncStates.isNotSynchronized },
        });
      }
    });
  };

  render() {
    if (this.props.style === 'modern') {
      return (
        <View
          style={{
            flexDirection: 'row',
            //justifyContent: 'space-around',
            width: '50%',
            //backgroundColor: 'red',
          }}>
          <IconButton
            size={30}
            icon='camera'
            color='#6D6D6D'
            onPress={this.takePicture}
          />
          <IconButton
            size={30}
            icon='image'
            color='#6D6D6D'
            onPress={this.selectImage}
          />
        </View>
      );
    }

    return (
      <View style={styles.buttonContainer}>
        <Button
          mode='outlined'
          color={theme.palette.primary.main}
          onPress={this.takePicture}
          style={styles.button}>
          <Icon color={theme.palette.primary.main} name='camera' size={20} />
        </Button>

        <Button
          mode='outlined'
          color={theme.palette.primary.main}
          onPress={this.selectImage}
          style={styles.button}>
          <Icon color={theme.palette.primary.main} name='image' size={20} />
        </Button>
      </View>
    );

    return (
      <View style={styles.container}>
        <View style={styles.buttonContainer}>
          <Button
            style={styles.button}
            appearance='outline'
            onPress={this.takePicture}
            accessoryLeft={() => (
              <Icon
                color={theme.palette.primary.main}
                name='camera'
                size={20}
              />
            )}
          />

          <Button
            style={styles.button}
            appearance='outline'
            onPress={this.selectImage}
            accessoryLeft={() => (
              <Icon color={theme.palette.primary.main} name='image' size={20} />
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  button: {
    width: '48%',
    marginVertical: 5,
    //borderColor: theme.palette.primary.main,
    //backgroundColor: '#FFF', //theme.palette.secondary.main,
    //color: 'red',
  },
});

const mapStateToProps = state => ({
  images: state.filters.images,
});

const mapDispatchToProps = {
  addImage,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectImages);
