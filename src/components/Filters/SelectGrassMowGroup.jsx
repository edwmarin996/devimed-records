import React, { Component } from 'react';
import { IndexPath, Select, SelectItem } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { setGrassMowGroups } from '../../redux/actions/filters';

export class SelectGrassMowGroup extends Component {
  constructor(props) {
    super(props);
    this.select = React.createRef();
  }

  selectGrassMowGroup = selectedIndex => {
    const selectedGrassMowGroups = selectedIndex.map(({ row }) => {
      return this.props.grassMowGroups[row].id;
    });

    this.props.setGrassMowGroups(selectedGrassMowGroups)
    this.select.current.blur();
  };

  render() {
    return (
      <Select
        multiSelect
        ref={this.select}
        placeholder='Seleccione las cuadrillas'
        value={this.props.fieldValue}
        selectedIndex={this.props.indexPaths}
        onSelect={this.selectGrassMowGroup}
        style={styles.select}>
        {this.props.grassMowGroups.map(group => (
          <SelectItem key={group.id} title={group.nombre} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => {
  const grassMowGroups = state.appData.grassMowGroups;
  const selectedGrassMowGroups = state.filters.selectedGrassMowGroups;

  const indexPaths = selectedGrassMowGroups.map(id => {
    return new IndexPath(grassMowGroups.findIndex(group => group.id === id));
  });

  const fieldValue = selectedGrassMowGroups
    .map(id => {
      return grassMowGroups.find(group => group.id === id).nombre;
    })
    .join(', ');

  return {
    indexPaths,
    fieldValue,
    selectedGrassMowGroups,
    grassMowGroups,
  };
};

const mapDispatchToProps = {
  setGrassMowGroups,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectGrassMowGroup);
