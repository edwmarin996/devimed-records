import React, { Component } from 'react';
import { Select, SelectItem } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { setRoadId } from '../../redux/actions/filters';

export class SelectRoad extends Component {
  selectRoad = selectedIndex => {
    const selectedRoadId = this.props.roads[selectedIndex.row].id;
    this.props.setRoadId(selectedRoadId);
  };

  render() {
    return (
      <Select
        placeholder='Seleccione una vía'
        value={this.props.selectedRoad?.nombre || ''}
        onSelect={this.selectRoad}
        style={styles.select}>
        {this.props.roads.map(road => (
          <SelectItem key={road.id} title={road.nombre} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => ({
  roads: Object.values(state.appData.roads).filter(
    ({ sector_id }) => sector_id === state.filters.selectedSectorId,
  ),

  selectedRoad: state.appData.roads[state.filters.selectedRoadId],
});

const mapDispatchToProps = {
  setRoadId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectRoad);
