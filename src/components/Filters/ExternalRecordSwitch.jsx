import React from 'react';
import { Switch, HelperText } from 'react-native-paper';
import { connect } from 'react-redux';
import { setExternalRecordSwitch } from '../../redux/actions/filters';
import { StyleSheet, View } from 'react-native';

export function ExternalRecordSwitch(props) {
  return (
    <View style={styles.switch}>
      <HelperText>Registro Externo</HelperText>
      <Switch
        value={props.isExternalRecord}
        onValueChange={isExternalRecord =>
          props.setExternalRecordSwitch(isExternalRecord)
        }
        color='#0172FB'
      />
    </View>
  );
}

const styles = StyleSheet.create({
  switch: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    paddingBottom: 10,
  },
});

const mapStateToProps = state => ({
  isExternalRecord: state.filters.isExternalRecord,
});

const mapDispatchToProps = {
  setExternalRecordSwitch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ExternalRecordSwitch);
