import React, { Component } from 'react';
import { Select, SelectItem } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { setLineSideId } from '../../redux/actions/filters';

export class SelectLineSide extends Component {
  selectLineSide = selectedIndex => {
    const selectedLineSideId = this.props.lineSides[selectedIndex.row].id;
    this.props.setLineSideId(selectedLineSideId);
  };

  render() {
    return (
      <Select
        placeholder='Seleccione una línea'
        value={this.props.selectedLineSide?.nombre || ''}
        onSelect={this.selectLineSide}
        style={styles.select}>
        {this.props.lineSides.map(lineSide => (
          <SelectItem key={lineSide.id} title={lineSide.nombre} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => ({
  lineSides: Object.values(state.appData.lineSides),
  selectedLineSide: state.appData.lineSides[state.filters.selectedLineSideId],
});

const mapDispatchToProps = {
  setLineSideId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectLineSide);
