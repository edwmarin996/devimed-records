import React, { Component } from 'react';
import { Input } from '@ui-kitten/components';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import {
  setInitialAbscissa,
  setFinalAbscissa,
} from '../../redux/actions/filters';

export class SelectAbscissas extends Component {
  setInitialAbscissa = abscissa => {
    //abscissa = isNaN(abscissa) ? null : parseInt(abscissa);

    if (abscissa !== null && !isNaN(abscissa)) {
      this.props.setInitialAbscissa(abscissa);
    }
  };

  setFinalAbscissa = abscissa => {
    // abscissa = isNaN(abscissa) ? null : parseInt(abscissa);

    if (abscissa !== null && !isNaN(abscissa)) {
      this.props.setFinalAbscissa(abscissa);
    }
  };

  render() {
    const customStyle = {};
    let initialAbscissaTitle = 'Abscisa inicial (m)';

    if (this.props.hideFinalAbscissa) {
      customStyle.width = '100%';
      initialAbscissaTitle = 'Abscissa (m)';
    }

    return (
      <View style={styles.abscissas}>
        <Input
          placeholder={initialAbscissaTitle}
          keyboardType='phone-pad'
          value={
            this.props.initialAbscissa || this.props.initialAbscissa === 0
              ? this.props.initialAbscissa.toString()
              : ''
          }
          onChangeText={this.setInitialAbscissa}
          style={[styles.select, customStyle]}
        />

        {!this.props.hideFinalAbscissa && (
          <Input
            placeholder='Abscisa final (m)'
            keyboardType='phone-pad'
            value={
              this.props.finalAbscissa || this.props.finalAbscissa === 0
                ? this.props.finalAbscissa.toString()
                : ''
            }
            onChangeText={this.setFinalAbscissa}
            style={styles.select}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '48%',
  },

  abscissas: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const mapStateToProps = state => ({
  initialAbscissa: state.filters.initialAbscissa,
  finalAbscissa: state.filters.finalAbscissa,
});

const mapDispatchToProps = {
  setInitialAbscissa,
  setFinalAbscissa,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectAbscissas);
