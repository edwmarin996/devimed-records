import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { Button as PaperButton } from 'react-native-paper';
import { Modal, Card, CheckBox, Text } from '@ui-kitten/components';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {
  addTagToCheckedList,
  removeTagFromCheckedList,
} from '../../redux/actions/filters';

export function SelectTags(props) {
  const onChangeTag = (tagId, value) => {
    if (value) {
      props.addTagToCheckedList(tagId);
      return;
    }

    props.removeTagFromCheckedList(tagId);
  };

  const verifyChecked = tagId => {
    const index = props.checkedTags.findIndex(element => element === tagId);
    if (index === -1) {
      return false;
    }

    return true;
  };

  return (
    <Modal
      backdropStyle={styles.backdrop}
      style={styles.modal}
      visible={props.visible}>
      <Card style={styles.card} disabled>
        <Text style={styles.title} category='h6' appearance='hint'>
          Seleccione una o varias categorías
        </Text>

        <View style={styles.content}>
          {props.tags.map(tag => (
            <CheckBox
              onChange={value => onChangeTag(tag.id, value)}
              checked={verifyChecked(tag.id)}
              style={styles.tag}
              key={tag.id}>
              {tag.nombre}
            </CheckBox>
          ))}
        </View>
      </Card>

      <PaperButton
        style={styles.button}
        mode='text'
        color='black'
        onPress={() => props.onSaveTags(props.checkedTags)}>
        Ok
      </PaperButton>
    </Modal>
  );
}

const styles = StyleSheet.create({
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },

  modal: {
    paddingHorizontal: 15,
  },

  card: {
    height: hp('50%'),
    width: wp('95%'),
  },

  title: {
    textAlign: 'center',
    paddingBottom: 10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: 1.5,
  },

  content: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
  },

  tag: {
    width: '30%',
    paddingHorizontal: 2,
    marginVertical: 8,
    margin: '1.5%',
  },

  button: {
    bottom: 50,
  },
});

const mapStateToProps = state => ({
  tags: state.appData.tags,
  checkedTags: state.filters.checkedTags,
});

const mapDispatchToProps = {
  addTagToCheckedList,
  removeTagFromCheckedList,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectTags);
