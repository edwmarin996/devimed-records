import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Select, SelectItem } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { setSectorId } from '../../redux/actions/filters';

export class SelectSector extends Component {
  selectSector = selectedIndex => {
    const selectedSectorId = this.props.sectors[selectedIndex.row].id;
    this.props.setSectorId(selectedSectorId);
  };

  render() {
    return (
      <Select
        placeholder='Seleccione un sector'
        value={this.props.selectedSector?.codigo || ''}
        onSelect={this.selectSector}
        style={styles.select}>
        {this.props.sectors.map(sector => (
          <SelectItem key={sector.id} title={sector.codigo} />
        ))}
      </Select>
    );
  }
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
    width: '100%',
  },
});

const mapStateToProps = state => ({
  sectors: Object.values(state.appData.sectors),
  selectedSector: state.appData.sectors[state.filters.selectedSectorId],
});

const mapDispatchToProps = {
  setSectorId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectSector);
