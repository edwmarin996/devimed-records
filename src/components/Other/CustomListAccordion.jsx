import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { View, Text } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function CustomListAccordion({ title, icon, children, color }) {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <>
      <TouchableRipple
        rippleColor='#777'
        onPress={() => setCollapsed(!collapsed)}
        style={styles.container}>
        <>
          <Icon
            name={icon}
            size={24}
            color={collapsed ? '#777' : color}
            style={styles.icon}
          />

          <Text style={[styles.title, !collapsed && { color }]}>{title}</Text>

          <Icon
            name={collapsed ? 'chevron-down' : 'chevron-up'}
            size={24}
            color='#000'
            style={styles.rightIcon}
          />
        </>
      </TouchableRipple>

      <Collapsible style={styles.collapsible} collapsed={collapsed}>
        {children}
      </Collapsible>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 70,
    alignItems: 'center',
    padding: 8,
  },

  icon: {
    marginLeft: 16,
  },

  title: {
    fontSize: 16,
    marginLeft: 24,
  },

  rightIcon: {
    position: 'absolute',
    marginHorizontal: 16,
    right: 0,
  },

  collapsible: {
    paddingHorizontal: 10,
    paddingLeft: 20,
    paddingBottom: 10,
  },
});
