import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { getDataFromServer, resetStore } from '../../redux/actions/appData';
import { Button, List } from 'react-native-paper';
import synchronize from '../../api/syncModule';
import { Text } from 'react-native';
import { theme } from '../../styles/theme';

export class Config extends Component {
  render() {
    return (
      <List.Accordion
        title='Configuración'
        theme={{ colors: { primary: theme.palette.primary.main } }}
        left={props => <List.Icon {...props} icon='settings' />}>
        <List.Item
          title='Obtener datos de aplicación'
          description='Descargar datos de sectores, vías, calzadas etc. '
          left={props => <List.Icon {...props} icon='cloud-download' />}
          onPress={this.props.getDataFromServer}
        />

        <List.Item
          title='Sincronizar registros'
          description='Enviar registros locales a los servidores remotos.'
          left={props => <List.Icon {...props} icon='cloud-upload' />}
          onPress={synchronize}
        />

        <List.Item
          title='Borrar datos de aplicación'
          description='Eliminar todos los datos de la aplicación, y restablecerla a su estado inicial.'
          left={props => <List.Icon {...props} icon='delete' />}
          onPress={this.props.resetStore}
        />
      </List.Accordion>
    );
  }
}

const mapDispatchToProps = {
  getDataFromServer,
  resetStore,
};

export default connect(
  null,
  mapDispatchToProps,
)(Config);
