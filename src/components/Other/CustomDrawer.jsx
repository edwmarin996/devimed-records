import React from 'react';
import { Text, Divider } from '@ui-kitten/components';
import { View, Image, StyleSheet } from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { connect } from 'react-redux';
import { userLogout } from '../../redux/actions/auth';

const CustomDrawer = props => {
  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.header}>
        <Text category='h6' appearance='hint'>
          Seleccione una categoría
        </Text>

        <Image
          style={styles.image}
          source={require('../../assets/icons/workerDevimed.png')}
        />

        <Text category='label' style={styles.name}>
          {props.name}
        </Text>

        <Divider style={styles.divider} />
      </View>

      <DrawerItemList {...props} />

      <DrawerItem
        onPress={props.userLogout}
        label='Cerrar sesión'
        icon={() => (
          <Image
            source={require('../../assets/icons/logout.png')}
            style={styles.miniature}
          />
        )}
      />
    </DrawerContentScrollView>
  );
};

const styles = StyleSheet.create({
  header: {
    marginHorizontal: 20,
    marginVertical: 20,
    alignItems: 'center',
  },

  name: {
    fontSize: 15,
    fontFamily: 'Montserrat-SemiBold',
  },

  image: {
    height: 170,
    width: 170,
    margin: 10,
    overflow: 'hidden',
  },

  divider: {
    marginVertical: 5,
    width: '100%',
    height: 1.5,
  },

  miniature: {
    width: 25,
    height: 25,
  },
});

const mapDispatchToProps = { userLogout };

const mapStateToProps = state => ({
  name: state.auth.userInfo.name,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomDrawer);
