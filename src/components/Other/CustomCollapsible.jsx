import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { Button } from '@ui-kitten/components';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default function CustomCollapsible(props) {
  const [collapsed, setCollapsed] = useState(true);

  return (
    <View style={styles.body}>
      <View>
        <Button
          onPress={() => setCollapsed(!collapsed)}
          status="basic"
          style={styles.button}
          accessoryRight={() => (
            <Icon name={collapsed ? 'chevron-down' : 'chevron-up'} />
          )}>
          {props.title}
        </Button>
      </View>
      <Collapsible collapsed={collapsed}>
        <View style={styles.collapsible}>{props.children}</View>
      </Collapsible>
    </View>
  );
}

const styles = StyleSheet.create({
  body: {
    width: '100%',
    marginVertical: 2,
  },

  button: {
    justifyContent: 'space-between',
  },

  collapsible: {
    marginVertical: 10,
  },
});
