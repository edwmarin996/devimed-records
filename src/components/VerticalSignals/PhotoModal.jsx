import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Image } from 'react-native';
import { hidePhotoModal } from '../../redux/actions/verticalSignals';
import { getVerticalSignalImage } from '../../utils/utils';
import {
  Portal,
  Modal,
  Card,
  ActivityIndicator,
  IconButton,
} from 'react-native-paper';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { View } from 'react-native';

export function PhotoModal({
  showPhotoModal,
  hidePhotoModal,
  selectedSignalId,
}) {
  const [showLoading, setShowLoading] = useState(false);

  return (
    <Portal>
      <Modal
        onDismiss={hidePhotoModal}
        visible={showPhotoModal}
        contentContainerStyle={styles.modalContainer}>
        <View style={styles.backButtonContainer}>
          <IconButton
            icon='arrow-left'
            color='#FFF'
            size={30}
            onPress={hidePhotoModal}
          />
        </View>

        <Card style={styles.card}>
          <Image
            style={styles.image}
            source={getVerticalSignalImage(selectedSignalId)}
          />

          <Image
            source={getVerticalSignalImage(selectedSignalId, {
              remote: true,
            })}
            onLoadStart={() => setShowLoading(true)}
            onLoadEnd={() => setShowLoading(false)}
            style={styles.image}
          />

          {showLoading && (
            <ActivityIndicator style={styles.loading} size={50} color='#AAA' />
          )}
        </Card>
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'black',
    width: '100%',
    height: '100%',
  },

  backButtonContainer: {
    width: '100%',
    height: '8%',
    justifyContent: 'center',
  },

  card: {
    width: '100%',
    height: '90%',
  },

  image: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },

  loadingContainer: {
    position: 'absolute',
    width: '80%',
    bottom: -8,
  },

  loading: {
    top: '50%',
  },
});

const mapStateToProps = state => {
  return {
    showPhotoModal: state.verticalSignals.showPhotoModal,
    selectedSignalId: state.verticalSignals.selectedSignalId,
  };
};

const mapDispatchToProps = {
  hidePhotoModal,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhotoModal);
