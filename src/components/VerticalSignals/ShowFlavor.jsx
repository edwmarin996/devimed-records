import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { HelperText, TouchableRipple } from 'react-native-paper';
import { getFlavorIconByCode } from '../../utils/utils';

export function ShowFlavor({ selectedFlavor, onPress }) {
  return (
    <TouchableRipple
      borderless
      rippleColor='#0041FF'
      style={styles.touchable}
      onPress={onPress}>
      <>
        <Image
          style={styles.image}
          source={
            selectedFlavor
              ? getFlavorIconByCode(selectedFlavor.codigo)
              : require('../../assets/icons/road-sign-gray.png')
          }
        />

        {!selectedFlavor?.codigo && <HelperText>Seleccione</HelperText>}
      </>
    </TouchableRipple>
  );
}

const mapStateToProps = state => {
  const selectedFlavor =
    state.verticalSignals.flavors[state.verticalSignals.selectedFlavorId];

  return { selectedFlavor };
};

export default connect(
  mapStateToProps,
  null,
)(ShowFlavor);

const styles = StyleSheet.create({
  touchable: {
    width: wp('30%'),
    height: wp('30%'),
    alignItems: 'center',
    justifyContent: 'center',

    backgroundColor: '#FFF',
    shadowColor: '#000',
    elevation: 4,

    borderRadius: 500,
  },

  image: {
    width: '60%',
    height: '60%',
  },
});
