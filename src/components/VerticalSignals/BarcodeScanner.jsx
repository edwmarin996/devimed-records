import React, { PureComponent } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { Portal, Modal, Card } from 'react-native-paper';
import { RNCamera } from 'react-native-camera';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { hideBarcodeScanner } from '../../redux/actions/verticalSignals';

const allowedCodeTypes = [RNCamera.Constants.BarCodeType.code39];
export class BarcodeScanner extends PureComponent {
  constructor(props) {
    super(props);
    this.stopScanner = false;
  }

  onBarCodeRead = ({ data, type }) => {
    if (this.stopScanner || !allowedCodeTypes.includes(type)) {
      return;
    }

    // To avoid repeated scans
    this.stopScanner = true;
    setTimeout(() => {
      this.stopScanner = false;
    }, 1500);

    this.props.onBarcodeRead(data);
  };

  onGoogleBarcodeRead = ({ barcodes }) => {
    if (this.stopScanner) {
      return;
    }

    let data;

    for (const barcode of barcodes) {
      if (barcode.type !== 'UNKNOWN_FORMAT') {
        data = barcode.data;
        break;
      }
    }

    // To avoid repeated scans
    this.stopScanner = true;
    setTimeout(() => {
      this.stopScanner = false;
    }, 1500);

    this.props.onBarcodeRead(data);
  };

  render() {
    return (
      <Portal>
        <Modal
          visible={this.props.showBarcodeScanner}
          onDismiss={this.props.hideBarcodeScanner}
          contentContainerStyle={styles.contentContainer}>
          <Card style={styles.scannerContainer}>
            <RNCamera
              captureAudio={false}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
              onGoogleVisionBarcodesDetected={this.onGoogleBarcodeRead}
              //onBarCodeRead={this.onBarCodeRead}
              //flashMode={RNCamera.Constants.FlashMode.torch}
            />

            <View style={styles.scope} />
          </Card>
        </Modal>
      </Portal>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    top: '-20%',
    alignItems: 'center',
  },

  scannerContainer: {
    width: wp('94%'),
    height: wp('70%'),
    borderRadius: 20,
  },

  scope: {
    borderWidth: 0, //0.5,
    width: '30%',
    height: '20%',
    position: 'absolute',
    bottom: '35%',
    left: '35%',
    borderColor: '#FFF',
  },

  preview: {
    flex: 1,
    borderRadius: 20,
    overflow: 'hidden',
  },
});

const mapStateToProps = state => ({
  showBarcodeScanner: state.verticalSignals.showBarcodeScanner,
});

const mapDispatchToProps = {
  hideBarcodeScanner,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BarcodeScanner);
