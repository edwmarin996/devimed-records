import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { FAB } from 'react-native-paper';
import { showBarcodeScanner } from '../../redux/actions/verticalSignals';

export class FabButtons extends Component {
  render() {
    return (
      <View style={styles.fabContainer}>
        {!this.props.showFullScreenMap &&
          !this.props.movingSignal &&
          this.props.showScope && (
            <FAB
              color='#3C3C3C'
              icon='plus'
              onPress={() => this.props.navigation.push('form')}
              style={[styles.fab, { right: 65 * 1 }]}
            />
          )}

        <FAB
          color='#3C3C3C'
          icon='target'
          onPress={this.props.goToUserLocation}
          style={[styles.fab, { right: 65 * 0 }]}
        />

        {/*
        
        <FAB
          color='#3C3C3C'
          icon='barcode-scan'
          onPress={this.props.showBarcodeScanner}
          style={[styles.fab, { right: 65 * 0 }]}
        />
        */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  fabContainer: {
    height: 70,
    justifyContent: 'center',
  },

  fab: {
    backgroundColor: '#FFF',
    position: 'absolute',
  },
});

const mapStateToProps = state => ({
  showScope: state.verticalSignals.showScope,
  showFullScreenMap: state.verticalSignals.showFullScreenMap,
  movingSignal: state.verticalSignals.movingSignal,
});

const mapDispatchToProps = {
  showBarcodeScanner,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FabButtons);
