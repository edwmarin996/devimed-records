import React from 'react';
import { connect } from 'react-redux';
import { Select, SelectItem } from '@ui-kitten/components';
import { setSelectedCompanyId } from '../../redux/actions/verticalSignals';

export function SelectCompany({
  measurementCompanies,
  selectedCompanyId,
  setSelectedCompanyId,
}) {
  const onSelect = ({ row }) => {
    const selectedCompanyId = Object.values(measurementCompanies)[row].id;
    setSelectedCompanyId(selectedCompanyId);
  };

  return (
    <Select
      placeholder='Empresa de medición'
      value={measurementCompanies?.[selectedCompanyId]?.nombre}
      onSelect={onSelect}>
      {Object.values(measurementCompanies).map(({ nombre, id }) => (
        <SelectItem title={nombre} key={id} />
      ))}
    </Select>
  );
}

const mapStateToProps = state => ({
  measurementCompanies: state.verticalSignals.measurementCompanies,
  selectedCompanyId: state.verticalSignals.selectedCompanyId,
});

const mapDispatchToProps = {
  setSelectedCompanyId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectCompany);
