import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Portal, Modal, Button } from 'react-native-paper';
import { theme } from '../../styles/theme';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function ConfirmationModal({
  hideModal,
  showModal,
  onConfirmation,
}) {
  return (
    <Portal>
      <Modal
        visible={showModal}
        onDismiss={hideModal}
        contentContainerStyle={styles.contentContainer}>
        <View style={styles.card}>
          <View style={styles.header}>
            <Icon
              name='alert'
              color={'#FFF'}
              size={60}
              style={styles.alertIcon}
            />
          </View>

          <View style={styles.body}>
            <Text style={styles.title}>Cerrar inspección</Text>

            <Text style={styles.text}>
              Al cerrar la inspección, se borrarán todas las mediciones locales
              asociadas a ella.
            </Text>

            <View style={styles.buttonContainer}>
              <Button
                mode='contained'
                onPress={hideModal}
                color={theme.palette.grey.main}
                style={styles.button}>
                Cancelar
              </Button>

              <Button
                mode='contained'
                onPress={onConfirmation}
                color={theme.palette.error.main}
                style={styles.button}>
                Cerrar
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  contentContainer: {
    alignItems: 'center',
  },

  card: {
    backgroundColor: '#FFF',
    width: '80%',
    borderRadius: 20,
  },

  header: {
    backgroundColor: theme.palette.error.main,
    alignItems: 'center',
    paddingVertical: 20,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },

  body: {
    padding: 20,
  },

  title: {
    fontFamily: 'Montserrat-Regular',
    textAlign: 'center',
    fontSize: 30,
    marginBottom: 15,
  },

  text: {
    textAlign: 'justify',
  },

  buttonContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  button: {
    borderRadius: 10,
    marginTop: 20,
    flex: 0.45,
  },
});
