import React, { useState } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import Slider from '@react-native-community/slider';
import { changeIconsSize } from '../../redux/actions/verticalSignals';
import { HelperText } from 'react-native-paper';
import { theme } from '../../styles/theme';

const x1 = 10; // Min slider value
const x2 = 60; // Max slider value

const y1 = 1; // 1%
const y2 = 100; // 100%

const formatValue = value => {
  return Math.trunc(((y2 - y1) / (x2 - x1)) * (value - x1) + y1) + '%';
};

export function IconsSizeSlider({ iconsSize, changeIconsSize }) {
  const [size, setSize] = useState(iconsSize);

  return (
    <>
      <HelperText>Tamaño de los iconos</HelperText>
      <View style={styles.container}>
        <Slider
          value={size}
          minimumTrackTintColor={theme.palette.primary.main}
          thumbTintColor={theme.palette.primary.main}
          style={styles.slider}
          minimumValue={x1}
          maximumValue={x2}
          step={1}
          onValueChange={setSize}
          onSlidingComplete={changeIconsSize}
        />
        <Text style={styles.text}>{formatValue(size)}</Text>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  slider: {
    flex: 0.85,
  },

  text: {
    flex: 0.12,
    color: '#777',
    fontFamily: 'Montserrat-SemiBold',
  },
});

const mapStateToProps = state => ({
  iconsSize: state.verticalSignals.iconsSize,
});

const mapDispatchToProps = {
  changeIconsSize,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(IconsSizeSlider);
