import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Image } from 'react-native';
import { TouchableRipple, Searchbar } from 'react-native-paper';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { FlatList } from 'react-native-gesture-handler';
import { ActivityIndicator } from 'react-native';
import { setSelectedFlavorId } from '../../redux/actions/verticalSignals';
import RBSheet from 'react-native-raw-bottom-sheet';

const openDuration = 600;
export class SelectFlavor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      ready: false,
    };
  }

  onChangeText = query => {
    this.setState({ query });
  };

  open = () => {
    this.RBSheet.open();

    setTimeout(() => {
      this.setState({ ready: true });
    }, openDuration * 1);
  };

  onClose = () => {
    this.setState({ ready: false, query: '' });
  };

  onFlavorPress = id => {
    this.props.setSelectedFlavorId(id);
    this.RBSheet.close();
    this.setState({ ready: false, query: '' });
  };

  render() {
    let flavors = Object.values(this.props.flavors)
      .filter(({ codigo }) =>
        codigo.toLowerCase().includes(this.state.query.toLowerCase()),
      )
      .sort((a, b) => {
        return a.nombre < b.nombre ? -1 : 1;
      });

    return (
      <RBSheet
        closeOnDragDown
        ref={ref => (this.RBSheet = ref)}
        onClose={this.onClose}
        height={heightPercentageToDP(70)}
        openDuration={openDuration}
        closeDuration={openDuration}
        customStyles={{ container: styles.bottomSheetContainer }}>
        <Searchbar
          value={this.state.query}
          style={styles.searchBar}
          onChangeText={this.onChangeText}
          placeholder='Buscar por código'
        />

        {this.state.ready ? (
          <FlatList
            keyboardShouldPersistTaps='handled'
            showsVerticalScrollIndicator={false}
            numColumns={3}
            data={flavors}
            initialNumToRender={12}
            keyExtractor={item => item.codigo}
            renderItem={({ item: { codigo: code, id } }) => (
              <Flavor code={code} onPress={() => this.onFlavorPress(id)} />
            )}
          />
        ) : (
          <ActivityIndicator color='#444' size={60} style={styles.loading} />
        )}
      </RBSheet>
    );
  }
}

const Flavor = ({ code, onPress }) => (
  <TouchableRipple
    rippleColor='#444'
    onPress={onPress}
    style={styles.flavorCard}>
    <>
      <Image
        style={styles.image}
        source={{ uri: `asset:/vertical_signals/${code}.png` }}
      />
      <Text style={styles.footer}>{code}</Text>
    </>
  </TouchableRipple>
);

const styles = StyleSheet.create({
  bottomSheetContainer: {
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,

    alignItems: 'center',
  },

  loading: {
    flex: 1,
  },

  searchBar: {
    width: '80%',
    marginVertical: 25,
  },

  scrollView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  flavorCard: {
    alignItems: 'center',
    padding: 2,
    margin: 5,
    width: 100,

    backgroundColor: '#FFF',
    shadowColor: '#000',
    borderRadius: 5,
    elevation: 2,
  },

  image: {
    width: 65,
    height: 65,
  },

  footer: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
  },
});

const mapStateToProps = state => ({
  flavors: state.verticalSignals.flavors,
});

const mapDispatchToProps = {
  setSelectedFlavorId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true },
)(SelectFlavor);
