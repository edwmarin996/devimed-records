import React from 'react';
import { connect } from 'react-redux';
import { Select, SelectItem } from '@ui-kitten/components';
import { setSelectedPaperId } from '../../redux/actions/verticalSignals';
import { StyleSheet } from 'react-native';

export function SelectPaperType({
  paperTypes,
  selectedPaperTypeId,
  setSelectedPaperId,
}) {
  const onSelect = ({ row }) => {
    const selectedPaperTypeId = Object.values(paperTypes)[row].id;
    setSelectedPaperId(selectedPaperTypeId);
  };

  return (
    <Select
      style={styles.select}
      placeholder='Tipo de papel'
      value={paperTypes?.[selectedPaperTypeId]?.nombre}
      onSelect={onSelect}>
      {Object.values(paperTypes).map(({ nombre, id }) => (
        <SelectItem title={nombre} key={id} />
      ))}
    </Select>
  );
}

const styles = StyleSheet.create({
  select: {
    marginVertical: 5,
  },
});

const mapStateToProps = state => ({
  paperTypes: state.verticalSignals.paperTypes,
  selectedPaperTypeId: state.verticalSignals.selectedPaperTypeId,
});

const mapDispatchToProps = {
  setSelectedPaperId,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SelectPaperType);
