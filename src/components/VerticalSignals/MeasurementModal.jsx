import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { Card, Input } from '@ui-kitten/components';
import { Modal, Button, Divider, Portal } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  hideMeasurementModal,
  addMeasurement,
  changeReflectometryMeasurement,
  changeReflectometryComment,
} from '../../redux/actions/verticalSignals';
import { showMessage } from '../../utils/utils';

export const Accessory = props => (
  <Icon
    name={props.name}
    color={props.style.tintColor}
    size={props.style.height}
  />
);

export class MeasurementModal extends Component {
  onChangeMeasurement = measurement => {
    if (measurement === '') {
      this.props.changeReflectometryMeasurement('');
      return;
    }

    if (!isNaN(measurement)) {
      this.props.changeReflectometryMeasurement(parseInt(measurement));
    }
  };

  save = () => {
    if (!this.props.reflectometryMeasurement) {
      this.props.hideModal();
      return;
    }

    const measurement = {
      senal_inventario_id: this.props.selectedSignalId,
      inspeccion_id: this.props.inspectionId,
      medicion: this.props.reflectometryMeasurement,
      observacion: this.props.reflectometryComment,
      fecha_medicion: new Date().toISOString(),
    };

    this.props.addMeasurement(measurement);
    this.props.hideModal();
    showMessage('La medicion se guardó correctamente');
  };

  render() {
    return (
      <Portal>
        <Modal
          visible={this.props.showModal}
          onDismiss={this.props.hideModal}
          contentContainerStyle={styles.contentContainer}>
          <Card style={styles.card}>
            <Text style={styles.headerText} appearance='hint' category='h6'>
              Agregar Medicion.
            </Text>

            <Divider style={styles.divider} />

            <Input
              autoFocus
              onChangeText={this.onChangeMeasurement}
              value={this.props.reflectometryMeasurement || ''}
              placeholder='Medición'
              keyboardType='phone-pad'
              accessoryRight={props => (
                <Accessory {...props} name='hazard-lights' />
              )}
            />

            <Input
              multiline
              onChangeText={this.props.changeReflectometryComment}
              value={this.props.reflectometryComment}
              textAlignVertical='top'
              placeholder={'Observación\n\n'}
              accessoryRight={props => (
                <Accessory {...props} name='comment-processing-outline' />
              )}
            />

            <View style={styles.buttonContainer}>
              <Button
                style={styles.button}
                onPress={this.props.hideModal}
                color='#000'>
                Cancelar
              </Button>

              <Button
                style={[styles.button, styles.verticalDivider]}
                onPress={this.save}
                color='#000'>
                Guardar
              </Button>
            </View>
          </Card>
        </Modal>
      </Portal>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    top: '-15%',
    alignItems: 'center',
  },

  card: {
    backgroundColor: '#FFF',
    width: '80%',
    borderRadius: 20,
  },

  headerText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },

  divider: {
    marginVertical: 20,
  },

  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },

  button: {
    width: '50%',
  },

  verticalDivider: {
    borderLeftWidth: 0.6,
    borderColor: '#000',
    borderRadius: 0,
  },
});

const mapStateToProps = state => {
  return {
    showModal: state.verticalSignals.showMeasurementModal,
    inspectionId: state.verticalSignals.inspection.id,
    selectedSignalId: state.verticalSignals.selectedSignalId,
    reflectometryComment: state.verticalSignals.reflectometryComment,
    reflectometryMeasurement: state.verticalSignals?.reflectometryMeasurement?.toString(),
  };
};

const mapDispatchToProps = {
  hideModal: hideMeasurementModal,
  addMeasurement,
  changeReflectometryMeasurement,
  changeReflectometryComment,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MeasurementModal);
