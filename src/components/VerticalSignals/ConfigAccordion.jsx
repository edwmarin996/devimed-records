import React, { Component } from 'react';
import { Alert, Text } from 'react-native';
import { connect } from 'react-redux';
import { List, Divider } from 'react-native-paper';
import { HelperText } from 'react-native-paper';
import { IconsSizeSlider, ScopeSizeSlider } from '.';
import { SelectCompany } from '.';
import { StyleSheet } from 'react-native';
import ListAccordion from '../Other/CustomListAccordion';
import {
  downloadInventory,
  downloadImages,
  uploadInventory,
  downloadOrCreateInspection,
  closeInspection,
  uploadMeasurements,
  downloadMeasurements,
} from '../../redux/actions/verticalSignals';
import { theme } from '../../styles/theme';
import ConfirmationModal from './ConfirmationModal';

export class ConfigAccordion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  getInventory = () => {
    Alert.alert(
      '¿Descargar inventario?',
      'Esta acción sobrescribirá todas las señales verticales almacenadas localmente ¿desea continuar?',
      [
        {
          text: 'Cancelar',
        },
        {
          text: 'Importar',
          onPress: () => this.props.downloadInventory(),
        },
      ],
      { cancelable: true },
    );
  };

  getImages = () => {
    Alert.alert(
      '¿Descargar imágenes?',
      'Asegúrese de tener una buena conexión a internet. Esta acción puede tomar un tiempo.',
      [
        {
          text: 'Cancelar',
        },
        {
          text: 'Importar',
          onPress: () => this.props.downloadImages(),
        },
      ],
      { cancelable: true },
    );
  };

  getInspection = () => {
    if (!this.props.selectedCompanyId) {
      Alert.alert(
        'Seleccione una empresa de medición.',
        'Debe seleccionar una empresa de medicion en "Otros Ajustes > Mediciones" antes de crear una inspeccion.',
      );
      return;
    }

    this.props.downloadOrCreateInspection(this.props.selectedCompanyId);
  };

  askForCloseInspection = () => {
    if (this.props.unSyncMeasurements) {
      this.setState({ showModal: true });
      return;
    }

    this.props.closeInspection(this.props.inspection.id);
  };

  getMeasurements = () => {
    Alert.alert(
      '¡Atención!',
      'Esta acción sobrescribirá todas las mediciones locales ¿desea continuar?',
      [
        {
          text: 'Cancelar',
        },
        {
          text: 'Importar',
          onPress: () =>
            this.props.downloadMeasurements(this.props.inspection.id),
        },
      ],
      { cancelable: true },
    );
  };

  render() {
    return (
      <>
        <ConfirmationModal
          showModal={this.state.showModal}
          hideModal={() => this.setState({ showModal: false })}
          onConfirmation={() => {
            this.props.closeInspection(this.props.inspection.id);
            this.setState({ showModal: false });
          }}
        />

        <Text style={styles.subTitle}>Ajustes de Sincronización</Text>

        <List.AccordionGroup>
          <List.Accordion
            left={props => <List.Icon {...props} icon='sign-direction' />}
            theme={accordionStyle}
            title='Inventario'
            id='1'>
            <List.Item
              title='Importar'
              description='Descargar en inventario de señales verticales.'
              left={props => <List.Icon {...props} icon='cloud-download' />}
              onPress={this.getInventory}
            />

            <List.Item
              title='Exportar'
              description='Exportar el inventario de señales verticales.'
              left={props => <List.Icon {...props} icon='cloud-upload' />}
              onPress={this.props.uploadInventory}
            />

            <List.Item
              title='Importar Imágenes'
              description='Descargar las imágenes de las señales verticales.'
              left={props => <List.Icon {...props} icon='file-download' />}
              onPress={this.getImages}
            />
          </List.Accordion>

          <Divider />

          {this.props.inspection?.id && (
            <>
              <List.Accordion
                left={props => <List.Icon {...props} icon='eye' />}
                theme={accordionStyle}
                title='Mediciones'
                id='2'>
                <List.Item
                  title='Importar'
                  description='Importar las mediciones de reflectometría.'
                  left={props => <List.Icon {...props} icon='cloud-download' />}
                  onPress={this.getMeasurements}
                />

                <List.Item
                  title='Exportar'
                  description='Exportar las mediciones de reflectometría.'
                  left={props => <List.Icon {...props} icon='cloud-upload' />}
                  onPress={this.props.uploadMeasurements}
                />
              </List.Accordion>

              <Divider />
            </>
          )}

          <List.Accordion
            left={props => (
              <List.Icon {...props} icon='clipboard-text-outline' />
            )}
            theme={accordionStyle}
            title='Inspección'
            id='3'>
            <List.Item
              title='Obtener'
              left={props => (
                <List.Icon {...props} icon='plus-circle-outline' />
              )}
              description='Busca y descarga la inspección actual o crea una nueva.'
              onPress={this.getInspection}
            />

            {this.props.inspection.id && (
              <List.Item
                title='Cerrar'
                description='Cerrar la inspección actual.'
                left={props => (
                  <List.Icon {...props} icon='minus-circle-outline' />
                )}
                onPress={this.askForCloseInspection}
              />
            )}
          </List.Accordion>
        </List.AccordionGroup>

        {/* Custom list accordion */}

        <Text style={styles.subTitle}>Otros Ajustes</Text>
        <ListAccordion
          title='Mediciones'
          icon='eye'
          color={theme.palette.primary.main}>
          <HelperText>Empresa de medición</HelperText>
          <SelectCompany />
        </ListAccordion>

        <Divider />

        <ListAccordion
          title='Mapa'
          icon='map'
          color={theme.palette.primary.main}>
          <IconsSizeSlider />
          <ScopeSizeSlider />
        </ListAccordion>
      </>
    );
  }
}

const accordionStyle = {
  colors: { primary: theme.palette.primary.main },
};

const styles = StyleSheet.create({
  subTitle: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    color: '#444',
  },
});

const mapStateToProps = state => ({
  inventory: state.verticalSignals.inventory,
  measurementsList: state.verticalSignals.measurementsList || {},
  inspection: state.verticalSignals.inspection,
  loading: state.verticalSignals.loading,

  selectedCompanyId: state.verticalSignals.selectedCompanyId,
});

const mapDispatchToProps = {
  downloadInventory,
  downloadImages,
  downloadOrCreateInspection,
  closeInspection,
  uploadInventory,
  uploadMeasurements,
  downloadMeasurements,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConfigAccordion);
