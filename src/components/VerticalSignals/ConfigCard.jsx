import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Card, Divider, Text } from '@ui-kitten/components';
import { formatDate } from '../../utils/utils';
import { theme } from '../../styles/theme';

export default function ConfigCard({
  inspection,
  inventoryLength,
  measurementsLength,
  unSyncSignals,
  unSyncMeasurements,
}) {
  return (
    <Card style={styles.card}>
      <Text style={styles.header}>
        {`Inspección \u2022  ${
          inspection.id ? formatDate(inspection.fecha_inicial) : 'Ninguna'
        }`}
      </Text>

      <Divider style={styles.divider} />

      <View style={styles.row}>
        <Text style={styles.title}>Señales</Text>
        <Text style={styles.title}>Mediciones</Text>
      </View>

      <View style={styles.row}>
        <Text style={styles.text}>{inventoryLength || '-'}</Text>
        <Text style={styles.text}>{measurementsLength || '-'}</Text>
      </View>

      <View style={styles.row}>
        <Text style={styles.title}>{'Señales sin\nsincronizar'}</Text>
        <Text style={styles.title}>{'Mediciones sin\nsincronizar'}</Text>
      </View>

      <View style={styles.row}>
        <Text style={styles.text}>{unSyncSignals || '-'}</Text>
        <Text style={styles.text}>{unSyncMeasurements || '-'}</Text>
      </View>
    </Card>
  );
}

const styles = StyleSheet.create({
  card: {
    width: '85%',
    marginLeft: '12%',

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,

    elevation: 8,

    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,

    marginBottom: 20,
  },

  header: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 6,
  },

  divider: {
    marginVertical: 5,
  },

  title: {
    width: '50%',
    textAlign: 'center',
    fontSize: 15,
    fontFamily: 'sans-serif-condensed',
    marginTop: 10,
  },

  text: {
    width: '50%',
    textAlign: 'center',
    fontSize: 20,
    color: theme.palette.primary.main,
  },

  row: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  column: {
    width: '50%',
    alignItems: 'center',
  },
});
