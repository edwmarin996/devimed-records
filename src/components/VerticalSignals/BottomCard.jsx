import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from '@ui-kitten/components';
import { getVerticalSignalImage, uuidToShortBase64 } from '../../utils/utils';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { getCoordinatesFromRegion, formatAbscissa } from '../../utils/utils';
import { StyleSheet, View, ScrollView, Image, Alert, Clipboard, ToastAndroid} from 'react-native';
// import Clipboard from '@react-native-clipboard/clipboard';
import {
  Chip,
  Title,
  Text,
  IconButton,
  TouchableRipple,
} from 'react-native-paper';
import {
  getFlavorNameById,
  getPaperTypeNameById,
  getRoadNameById,
  getRoadSideNameById,
} from '../../utils/helpers';
import {
  addOrUpdateVerticalSignal,
  deleteVerticalSignal,
  showMeasurementModal,
  setConfig,
  showPhotoModal,
  searchAndSetSignalMeasurement,
} from '../../redux/actions/verticalSignals';
import {
  setSelectedPaperId,
  setSelectedFlavorId,
} from '../../redux/actions/verticalSignals';
import { setFilters } from '../../utils/utils';
import { editRecord } from '../../redux/actions/tempData';
import { theme } from '../../styles/theme';

export class BottomCard extends Component {
  moveSignal = () => {
    if (!this.props.movingSignal) {
      const config = {
        movingSignal: true,
        showScope: true,
      };

      this.props.setConfig(config);
      return;
    }

    const config = {
      movingSignal: false,
      showScope: false,
    };

    this.props.setConfig(config);
    const coordinates = getCoordinatesFromRegion(this.props.region);

    const signal = {
      ...this.props.selectedSignal,
      coordinates,
    };

    this.props.addOrUpdateVerticalSignal(signal);
  };

  editSignal = () => {
    this.props.navigation.push('form');

    const images = this.props.selectedSignal.photoNameAlt
      ? [this.props.selectedSignal.photoNameAlt]
      : null;

    setFilters({
      roadId: this.props.selectedSignal.via_id,
      roadSideId: this.props.selectedSignal.costado_via_id,
      initialAbscissa: this.props.selectedSignal.abscisa,
      date: this.props.selectedSignal.fecha_instalacion,
      images,
    });

    this.props.setSelectedFlavorId(this.props.selectedSignal.senal_id);
    this.props.setSelectedPaperId(this.props.selectedSignal.papel_id);
    this.props.editRecord(this.props.selectedSignal);
  };

  askForDeleteVerticalSignal = () => {
    Alert.alert(
      '¡Atención!',
      '¿Esta seguro que desea eliminar esta señal?',
      [
        {
          text: 'Cancelar',
        },
        {
          text: 'Eliminar',
          onPress: () =>
            this.props.deleteVerticalSignal(this.props.selectedSignalId),
        },
      ],
      { cancelable: true },
    );
  };

  copyIdToClipboard = (id) => () => {
    ToastAndroid.show('Id copiado', ToastAndroid.SHORT, ToastAndroid.BOTTOM)
    Clipboard.setString(id)
  }

  onAddMeasurementPress = () => {
    this.props.searchAndSetSignalMeasurement(this.props.selectedSignalId);
    this.props.showMeasurementModal();
  };

  render() {
    if (
      !this.props.showBottomCard ||
      !this.props.selectedSignal ||
      this.props.showFullScreenMap
    ) {
      return null;
    }

    const CardHeader = () => (
      <View style={styles.cardHeader}>
        <Title>{getFlavorNameById(this.props.selectedSignal.senal_id)}</Title>

        <IconButton
          onPress={() => this.props.setConfig({ showBottomCard: false })}
          color='gray'
          icon='close'
          style={styles.closeButton}
          size={20}
        />
      </View>
    );

    const shortId = uuidToShortBase64(this.props.selectedSignal.id)

    return (
      <Card disabled style={styles.card} header={CardHeader}>
        <View style={styles.content}>
          <View style={styles.leftSide}>
            <View style={styles.idContainer}>
              <Text style={styles.boldText}>
                ID
                <Text>
                  {' \u2022 ' + shortId}
                </Text>
              </Text>

              <IconButton
                icon='content-copy'
                color={theme.palette.grey.main}
                style={{backgroundColor: theme.palette.primary.main, margin: 0, marginLeft: 20 }}
                size={15}
                onPress={this.copyIdToClipboard(shortId)}
              />
            </View>

            {this.props.selectedSignal.barcode && (
              <Text style={styles.boldText}>
                Código
                <Text>
                  {' \u2022 ' + this.props.selectedSignal.barcode}
                </Text>
              </Text>
            )}

            <Text style={styles.boldText}>
              Vía
              <Text>
                {' \u2022 ' + getRoadNameById(this.props.selectedSignal.via_id)}
              </Text>
            </Text>

            <Text style={styles.boldText}>
              Abscisa
              <Text>
                {' \u2022 ' + formatAbscissa(this.props.selectedSignal.abscisa)}
              </Text>
            </Text>

            <Text style={styles.boldText}>
              Costado
              <Text>
                {' \u2022 ' +
                  getRoadSideNameById(this.props.selectedSignal.costado_via_id)}
              </Text>
            </Text>

            <Text style={styles.boldText}>
              Tipo de señal
              <Text>
                {' \u2022 ' +
                  getFlavorNameById(this.props.selectedSignal.senal_id)}
              </Text>
            </Text>

            <Text style={styles.boldText}>
              Tipo de papel
              <Text>
                {' \u2022 ' +
                  getPaperTypeNameById(this.props.selectedSignal.papel_id)}
              </Text>
            </Text>
          </View>

          <View style={styles.rightSide}>
            <View style={styles.imageContainer}>
              <TouchableRipple
                onPress={this.props.showPhotoModal}
                rippleColor='#FFF'
                borderless={true}>
                <Image
                  style={styles.image}
                  source={getVerticalSignalImage(this.props.selectedSignalId)}
                />
              </TouchableRipple>
            </View>
          </View>
        </View>

        {
          <View style={styles.buttonsContainer}>
            {this.props.inspection.id && (
              <IconButton
                icon='plus'
                color='#FFF'
                style={{ backgroundColor: theme.palette.primary.main }}
                onPress={this.onAddMeasurementPress}
              />
            )}
            <IconButton
              icon={this.props.movingSignal ? 'map-marker' : 'cursor-move'}
              color='#FFF'
              style={{ backgroundColor: theme.palette.primary.main }}
              onPress={this.moveSignal}
            />

            <IconButton
              icon='pen'
              color='#FFF'
              style={{ backgroundColor: theme.palette.primary.main }}
              onPress={this.editSignal}
            />

            <IconButton
              icon='delete'
              color='#FFF'
              style={{ backgroundColor: theme.palette.error.main }}
              onPress={this.askForDeleteVerticalSignal}
            />
          </View>
        }

        {/*
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.buttonsContainer}>
            {this.props.inspection.id && (
              <Chip
                onPress={this.onAddMeasurementPress}
                mode='outlined'
                icon={props => <Icon name='plus' {...props} color='#FFF' />}
                selectedColor='#FFF'
                style={[
                  styles.chip,
                  { backgroundColor: theme.palette.primary.main },
                ]}>
                Agregar Medición
              </Chip>
            )}

            <Chip
              onPress={this.moveSignal}
              mode='outlined'
              icon={this.props.movingSignal ? 'map-marker' : 'cursor-move'}
              style={styles.chip}>
              {this.props.movingSignal ? 'Mover Aquí' : 'Mover Señal'}
            </Chip>

            <Chip
              onPress={this.editSignal}
              mode='outlined'
              icon={'pen'}
              style={styles.chip}>
              Editar Señal
            </Chip>

            <Chip
              onPress={this.askForDeleteVerticalSignal}
              mode='outlined'
              selectedColor='#FFF'
              icon={props => <Icon name='delete' {...props} color='#FFF' />}
              style={[
                styles.chip,
                { backgroundColor: theme.palette.error.main },
              ]}>
              Eliminar Señal
            </Chip>
          </ScrollView>
            */}
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },

  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  closeButton: {
    position: 'absolute',
    right: 5,
    marginVertical: 5,
  },

  content: {
    flexDirection: 'row',
  },

  leftSide: {
    flex: 0.7,
    justifyContent: 'center',
  },

  rightSide: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
  },

  imageContainer: {
    width: 80,
    height: 80,
    borderRadius: 40,
    backgroundColor: '#E1E4E8',
    elevation: 4,
  },

  image: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },

  buttonsContainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 2,
  },

  chip: {
    marginHorizontal: 5,
  },

  boldText: {
    fontWeight: 'bold',
    fontFamily: 'sans-serif-condensed',
  },

  idContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  }
});

const mapStateToProps = state => {
  const inventory = state.verticalSignals.inventory;
  const selectedSignalId = state.verticalSignals.selectedSignalId;
  const selectedSignal = inventory[selectedSignalId];

  return {
    inspection: state.verticalSignals.inspection,
    showFullScreenMap: state.verticalSignals.showFullScreenMap,
    region: state.verticalSignals.region,
    showBottomCard: state.verticalSignals.showBottomCard,
    movingSignal: state.verticalSignals.movingSignal,
    selectedSignal,
    selectedSignalId,
  };
};

const mapDispatchToProps = {
  setConfig,
  searchAndSetSignalMeasurement,
  showMeasurementModal,
  showPhotoModal,
  addOrUpdateVerticalSignal,
  deleteVerticalSignal,
  setSelectedFlavorId,
  setSelectedPaperId,
  editRecord,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BottomCard);
