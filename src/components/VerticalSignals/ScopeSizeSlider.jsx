import React, { useState } from 'react';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import Slider from '@react-native-community/slider';
import { changeScopeSize } from '../../redux/actions/verticalSignals';
import { HelperText } from 'react-native-paper';
import { theme } from '../../styles/theme';

const x1 = 30; // Min slider value
const x2 = 300; // Max slider value

const y1 = 1; // 1%
const y2 = 100; // 100%

const formatValue = value => {
  return Math.trunc(((y2 - y1) / (x2 - x1)) * (value - x1) + y1) + '%';
};

export function ScopeSizeSlider({ scopeSize, changeScopeSize }) {
  const [size, setSize] = useState(scopeSize);

  return (
    <>
      <HelperText>Tamaño del objetivo</HelperText>

      <View style={styles.container}>
        <Slider
          value={size}
          minimumTrackTintColor={theme.palette.primary.main}
          thumbTintColor={theme.palette.primary.main}
          style={styles.slider}
          minimumValue={x1}
          maximumValue={x2}
          step={1}
          onValueChange={setSize}
          onSlidingComplete={changeScopeSize}
        />
        <Text style={styles.text}>{formatValue(size)}</Text>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  slider: {
    flex: 0.85,
  },

  text: {
    flex: 0.15,
    color: '#777',
    fontFamily: 'Montserrat-SemiBold',
  },
});

const mapStateToProps = state => ({
  scopeSize: state.verticalSignals.scopeSize,
});

const mapDispatchToProps = {
  changeScopeSize,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScopeSizeSlider);
