export default {
  GRASS_MOW_INSPECTOR: 'registrar_avance_roceria',
  HORIZONTAL_PAINTING: 'registrar_pintura_horizontal',
  PHOTOS_RECORDS: 'registrar_fotografias',
  VERTICAL_SIGNALS: 'registrar_senales_verticales',
};
