let SERVER_URL = 'https://api.devimed.com.co';
let AUTH_SERVER_URL = 'https://auth.devimed.com.co/auth/';
let TILES_URL =
  'https://api.devimed.com.co/media/teselas/ortofotos/{z}/{x}/{y}.png';
let MINUTES_BEFORE_DELETE_OPTION_DISAPPEAR_FROM_RECORDS = 2160;

if (__DEV__) {
  SERVER_URL = 'https://pruebas.devimed.com.co/django_api';
  AUTH_SERVER_URL = 'https://pruebas.devimed.com.co/auth/';
  MINUTES_BEFORE_DELETE_OPTION_DISAPPEAR_FROM_RECORDS = 21600;
}

export default {
  // Users have 2160 min (36h) to delete the records after created
  MINUTES_BEFORE_DELETE_OPTION_DISAPPEAR_FROM_RECORDS,

  // After this size(KB), the images will be compressed using JPEG algorithm
  MAX_IMAGE_SIZE_KB: 256,

  // Quality of compressed images (0% - 100%)
  COMPRESSED_IMAGES_QUALITY: 75,

  // Previous versión -> v1
  REDUX_TREE_VERSIÓN: 'v1.1',

  // Map Tiles
  TILES_URL,

  // Vertical signals map config
  SEARCH_REGION_RADIUS: 300,
  ZOOM_TO_SHOW_SIGNALS: 16,
  LOCAL_IMAGE_RESOLUTION: 200,

  // Sync config
  SERVER_URL,

  // General endpoints
  ROADS_ENDPOINT: '/v2/vias/vias',
  SECTORS_ENDPOINT: '/v2/vias/sectores',
  ROADWAYS_ENDPOINT: '/v2/inventario/calzadas',
  ROAD_SIDES_ENDPOINT: '/v2/inventario/costados',
  LINE_SIDES_ENDPOINT: '/v2/inventario/senales_horizontales_costados',
  PHOTO_RECORDS_TAGS: '/v2/registros/registros_fotograficos/etiquetas',
  MEASUREMENT_COMPANIES: '/v2/inventario/empresa_medicion',
  INSPECTIONS_ENDPOINT: '/v2/inventario/inspecciones',

  // Endpoints to old road sides config
  OLD_ROAD_SIDES_ENDPOINT: '/v2/vias/costados',
  ROAD_SIDE_TYPES_ENDPOINT: '/v2/vias/tipo_costados',

  // GrassMow endpoints
  GRASS_MOW_RECORD_ENDPOINT: '/v2/mantenimiento/roceria_cunetas',
  GRASS_MOW_IMAGE_ENDPOINT: '/v2/mantenimiento/roceria_cunetas/fotos',
  GRASS_MOW_GROUPS_ENDPOINT: '/v2/mantenimiento/cuadrillas',

  // Horizontal paint endpoints
  HORIZONTAL_PAINT_RECORD_ENDPOINT: '/v2/registros/pintura_horizontal',
  HORIZONTAL_PAINT_IMAGE_ENDPOINT: '/v2/registros/pintura_horizontal/fotos',

  // Photos records endpoints
  PHOTOS_RECORDS_ENDPOINT: '/v2/registros/registros_fotograficos',
  PHOTOS_RECORDS_IMAGE_ENDPOINT: '/v2/registros/registros_fotograficos/fotos',

  // Vertical signals endpoints
  VERTICAL_SIGNALS_INSP_CODE: 'senales_verticales',
  VERTICAL_SIGNALS_INVENTORY: '/v2/inventario/senales_verticales_inventarios',
  VERTICAL_SIGNALS_FLAVORS: '/v2/inventario/senales_verticales',
  VERTICAL_SIGNALS_PAPER_TYPES: '/v2/inventario/senales_verticales_papeles',
  VERTICAL_SIGNALS_IMAGES: `/media/senal_vertical_inventario`,
  VERTICAL_SIGNALS_MEASUREMENTS:
    '/v2/inventario/senales_verticales_inventarios/mediciones',
};

export const keyCloakConfig = {
  AUTH_SERVER_URL,
  realm: 'devimed',
  resource: 'registros_app',
};
