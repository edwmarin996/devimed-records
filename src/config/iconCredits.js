export default [
  {
    href: 'https://www.flaticon.es/autores/dinosoftlabs',
    author: 'DinosoftLabs',
  },
  {
    href: 'https://www.flaticon.es/autores/freepik',
    author: 'Freepik',
  },
  {
    href: 'https://www.flaticon.es/autores/pixel-perfect',
    author: 'Pixel perfect',
  },
  {
    href: 'https://www.flaticon.es/autores/smalllikeart',
    author: 'Smalllikeart',
  },
  {
    href: 'https://www.flaticon.es/autores/smashicons',
    author: 'Smashicons',
  },
  {
    href: 'https://www.flaticon.es/autores/xnimrodx',
    author: 'xnimrodx',
  },
  {
    href: 'https://www.flaticon.es/autores/vignesh-oviyan',
    author: 'Vignesh Oviyan',
  },
];
