import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { actions } from '../actions/appData';
import thunk from 'redux-thunk';
import reducers from '../reducers/reducers';
import storage from 'redux-persist-filesystem-storage';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['filters', 'tempData'],
};

const rootReducer = (state, action) => {
  if (action.type === actions.RST_REDUX_TREE) {
    // To remove state from async storage
    storage.removeItem('persist:root');

    // To return initialState from all reducers
    state = undefined;
  }

  return reducers(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// To use redux dev tools
/*
let store;

if (__DEV__) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(thunk)),
  );
} else {
  store = createStore(persistedReducer, applyMiddleware(thunk));
}*/

export let store = createStore(persistedReducer, applyMiddleware(thunk));
export let persistor = persistStore(store);
