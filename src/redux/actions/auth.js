export const actions = {
  UPDATE_AUTH_STATE: 'UPDATE_AUTH_STATE',
  USER_LOGOUT: 'USER_LOGOUT',
};

export const updateAuthState = userInfo => {
  return {
    type: actions.UPDATE_AUTH_STATE,
    payload: userInfo,
  };
};

export const userLogout = () => {
  return {
    type: actions.USER_LOGOUT,
  };
};
