export const actions = {
  ADD_GRASS_MOW_RECORD: 'ADD_GRASS_MOW_RECORD',
  UPDATE_GRASS_MOW_RECORD: 'UPDATE_RECORD_STATUS',
  REMOVE_GRASS_MOW_RECORD: 'REMOVE_GRASS_MOW_RECORD',

  ADD_GRASS_MOW_RECORD_TO_DELETED_LIST: 'ADD_GRASS_MOW_RECORD_TO_DELETED_LIST',
  ADD_GRASS_MOW_IMAGE_TO_DELETED_LIST: 'ADD_GRASS_MOW_IMAGE_TO_DELETED_LIST',

  REMOVE_GRASS_MOW_RECORD_FROM_DELETED_LIST:
    'REMOVE_GRASS_MOW_RECORD_FROM_DELETED_LIST',
  REMOVE_GRASS_MOW_IMAGE_FROM_DELETED_LIST:
    'REMOVE_GRASS_MOW_IMAGE_FROM_DELETED_LIST',
};

export const addGrassMowRecord = record => ({
  type: actions.ADD_GRASS_MOW_RECORD,
  payload: record,
});

export const updateGrassMowRecord = record => ({
  type: actions.UPDATE_GRASS_MOW_RECORD,
  payload: record,
});

export const removeGrassMowRecord = (recordId, addRecordToPendingSyncList) => {
  return dispatch => {
    if (addRecordToPendingSyncList) {
      // Add the record to the deleted list, to synchronize later
      dispatch(addGrassMowRecordToDeletedList(recordId));
    }

    // Remove record  list
    dispatch({
      type: actions.REMOVE_GRASS_MOW_RECORD,
      payload: recordId,
    });
  };
};

export const addGrassMowRecordToDeletedList = recordId => ({
  type: actions.ADD_GRASS_MOW_RECORD_TO_DELETED_LIST,
  payload: recordId,
});

export const removeGrassMowRecordFromDeletedList = recordId => ({
  type: actions.REMOVE_GRASS_MOW_RECORD_FROM_DELETED_LIST,
  payload: recordId,
});

export const addGrassMowImageToDeletedList = recordId => ({
  type: actions.ADD_GRASS_MOW_IMAGE_TO_DELETED_LIST,
  payload: recordId,
});

export const removeGrassMowImageFromDeletedList = imageId => ({
  type: actions.REMOVE_GRASS_MOW_IMAGE_FROM_DELETED_LIST,
  payload: imageId,
});
