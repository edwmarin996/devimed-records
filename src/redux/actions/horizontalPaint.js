export const actions = {
  ADD_HORIZONTAL_PAINT_RECORD: 'ADD_HORIZONTAL_PAINT_RECORD',
  UPDATE_HORIZONTAL_PAINT_RECORD: 'UPDATE_HORIZONTAL_PAINT_RECORD_STATUS',
  REMOVE_HORIZONTAL_PAINT_RECORD: 'REMOVE_HORIZONTAL_PAINT_RECORD',

  ADD_HORIZONTAL_PAINT_RECORD_TO_DELETED_LIST:
    'ADD_HORIZONTAL_PAINT_RECORD_TO_DELETED_LIST',
  REMOVE_HORIZONTAL_PAINT_RECORD_FROM_DELETED_LIST:
    'REMOVE_HORIZONTAL_PAINT_RECORD_FROM_DELETED_LIST',

  ADD_HORIZONTAL_PAINT_IMAGE_TO_DELETED_LIST:
    'ADD_HORIZONTAL_PAINT_MAGE_TO_DELETED_LIST',
  REMOVE_HORIZONTAL_PAINT_IMAGE_FROM_DELETED_LIST:
    'REMOVE_HORIZONTAL_PAINT_IMAGE_FROM_DELETED_LIST',
};

export const addHorizontalPaintRecord = record => ({
  type: actions.ADD_HORIZONTAL_PAINT_RECORD,
  payload: record,
});

export const updateHorizontalPaintRecord = record => ({
  type: actions.UPDATE_HORIZONTAL_PAINT_RECORD,
  payload: record,
});

export const removeHorizontalPaintRecord = (
  recordId,
  addRecordToPendingSyncList,
) => {
  return dispatch => {
    if (addRecordToPendingSyncList) {
      // Add the record to the deleted list, to synchronize later
      dispatch(addHorizontalPaintRecordToDeletedList(recordId));
    }

    // Remove record records list
    dispatch({
      type: actions.REMOVE_HORIZONTAL_PAINT_RECORD,
      payload: recordId,
    });
  };
};

export const addHorizontalPaintRecordToDeletedList = recordId => ({
  type: actions.ADD_HORIZONTAL_PAINT_RECORD_TO_DELETED_LIST,
  payload: recordId,
});

export const removeHorizontalPaintRecordFromDeletedList = recordId => ({
  type: actions.REMOVE_HORIZONTAL_PAINT_RECORD_FROM_DELETED_LIST,
  payload: recordId,
});

export const addHorizontalPaintImageToDeletedList = recordId => ({
  type: actions.ADD_HORIZONTAL_PAINT_IMAGE_TO_DELETED_LIST,
  payload: recordId,
});

export const removeHorizontalPaintImageFromDeletedList = imageId => ({
  type: actions.REMOVE_HORIZONTAL_PAINT_IMAGE_FROM_DELETED_LIST,
  payload: imageId,
});
