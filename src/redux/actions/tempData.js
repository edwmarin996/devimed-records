export const actions = {
  EDITING_RECORD: 'EDITING_RECORD',
  NOT_EDITING_RECORD: 'NOT_EDITING_RECORD',
  START_LOADING: 'START_LOADING',
  SET_LOADING_PROGRESS: 'SET_LOADING_PROGRESS',
  END_LOADING: 'END_LOADING',
};

export const editRecord = record => {
  return {
    payload: record,
    type: actions.EDITING_RECORD,
  };
};

export const notEditRecord = () => {
  return {
    type: actions.NOT_EDITING_RECORD,
  };
};

export const startLoading = () => {
  return dispatch => {
    dispatch(setLoadingProgress(0));

    dispatch({
      type: actions.START_LOADING,
    });
  };
};

export const setLoadingProgress = progress => {
  return {
    type: actions.SET_LOADING_PROGRESS,
    payload: progress,
  };
};

export const endLoading = () => {
  return {
    type: actions.END_LOADING,
  };
};
