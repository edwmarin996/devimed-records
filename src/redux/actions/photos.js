export const actions = {
  ADD_PHOTO_RECORD: 'ADD_PHOTO_RECORD',
  UPDATE_PHOTO_RECORD: 'UPDATE_PHOTO_RECORD_STATUS',
  OVERWRITE_PHOTO_RECORD: 'OVERWRITE_PHOTO_RECORD',
  REMOVE_PHOTO_RECORD: 'REMOVE_PHOTO_RECORD',

  ADD_PHOTO_RECORD_TO_DELETED_LIST: 'ADD_PHOTO_RECORD_TO_DELETED_LIST',
  REMOVE_PHOTO_RECORD_FROM_DELETED_LIST:
    'REMOVE_PHOTO_RECORD_FROM_DELETED_LIST',

  ADD_PHOTO_IMAGE_TO_DELETED_LIST: 'ADD_PHOTO_MAGE_TO_DELETED_LIST',
  REMOVE_PHOTO_IMAGE_FROM_DELETED_LIST: 'REMOVE_PHOTO_IMAGE_FROM_DELETED_LIST',
};

export const addPhotoRecord = record => {
  return {
    type: actions.ADD_PHOTO_RECORD,
    payload: record,
  };
};

export const updatePhotoRecord = record => ({
  type: actions.UPDATE_PHOTO_RECORD,
  payload: record,
});

export const overwritePhotoRecord = record => ({
  type: actions.OVERWRITE_PHOTO_RECORD,
  payload: record,
});

export const removePhotoRecord = (recordId, addRecordToPendingSyncList) => {
  return dispatch => {
    if (addRecordToPendingSyncList) {
      // Add the record to the deleted list, to synchronize later
      dispatch(addPhotoRecordToDeletedList(recordId));
    }

    // Remove record records list
    dispatch({
      type: actions.REMOVE_PHOTO_RECORD,
      payload: recordId,
    });
  };
};

export const addPhotoRecordToDeletedList = recordId => ({
  type: actions.ADD_PHOTO_RECORD_TO_DELETED_LIST,
  payload: recordId,
});

export const removePhotoRecordFromDeletedList = recordId => ({
  type: actions.REMOVE_PHOTO_RECORD_FROM_DELETED_LIST,
  payload: recordId,
});

export const addPhotoImageToDeletedList = recordId => ({
  type: actions.ADD_PHOTO_IMAGE_TO_DELETED_LIST,
  payload: recordId,
});

export const removePhotoImageFromDeletedList = imageId => ({
  type: actions.REMOVE_PHOTO_RECORD_FROM_DELETED_LIST,
  payload: imageId,
});
