import * as api from '../../api/appData';
import roles from '../../config/roles';

// Action Types
export const actions = {
  SET_APP_DATA: 'SET_APP_DATA',

  RST_REDUX_TREE: 'RST_REDUX_TREE',
  SET_REDUX_TREE_VERSION: 'SET_REDUX_STORE_VERSION',
};

const dataSources = [
  {
    field: 'sectors',
    source: api.getSectors,
    roles: [],
  },
  {
    field: 'roads',
    source: api.getRoads,
    roles: [],
  },
  {
    field: 'roadWays',
    source: api.getRoadWays,
    roles: [],
  },
  {
    field: 'roadSides',
    source: api.getRoadSides,
    roles: [],
  },
  {
    field: 'oldRoadSides',
    source: api.getOldRoadSides,
    roles: [],
  },
  {
    field: 'lineSides',
    source: api.getLineSides,
    roles: [],
  },
  {
    field: 'grassMowGroups',
    source: api.getGrassMowGroups,
    roles: [roles.GRASS_MOW_INSPECTOR],
  },
  {
    field: 'tags',
    source: api.getTags,
    roles: [roles.PHOTOS_RECORDS],
  },
];

// Action Creators
export const setAppData = data => {
  return {
    type: actions.SET_APP_DATA,
    payload: data,
  };
};

// Async Action Creators
export const getDataFromServer = () => {
  return async (dispatch, getState) => {
    const {
      auth: { userRoles },
    } = getState();

    const filteredDataSources = dataSources.filter(dataSource =>
      dataSource.roles.every(role => userRoles.indexOf(role) > -1),
    );

    const fields = filteredDataSources.map(dataSource => dataSource.field);
    let dataSourcesValues = [];

    try {
      dataSourcesValues = await Promise.all(
        filteredDataSources.map(dataSource => dataSource.source()),
      );
    } catch (error) {
      console.log(error);
      return;
    }

    const data = {};
    dataSourcesValues.forEach((value, idx) => {
      data[fields[idx]] = value;
    });

    dispatch(setAppData(data));
  };
};

export const resetStore = () => {
  return {
    type: actions.RST_REDUX_TREE,
  };
};

export const setReduxTreeVersion = version => {
  return {
    type: actions.SET_REDUX_TREE_VERSION,
    payload: version,
  };
};
