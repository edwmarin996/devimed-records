import { removeEmptyProperties, showAlert } from '../../utils/utils';
import { startLoading, setLoadingProgress, endLoading } from './tempData';
import {
  getVerticalSignalsInventory,
  getVerticalSignalsFlavors,
  getVerticalSignalsPaperTypes,
  getVerticalSignalsImages,
  uploadVerticalSignal,
  getMeasurementCompanies,
  createVerticalSignalsInspection,
  closeVerticalSignalInspection,
  getVerticalSignalsInspection,
  getMeasurements,
  uploadMeasurement,
  deleteVerticalSignal as deleteVerticalSignalApi,
} from '../../api/verticalSignals';

export const actions = {
  SET_VERTICAL_SIGNALS_INVENTORY: 'SET_VERTICAL_SIGNALS_INVENTORY',
  SET_INSPECTION: 'SET_INSPECTION',

  SET_VERTICAL_SIGNALS_APP_DATA: 'SET_VERTICAL_SIGNALS_APP_DATA',

  SET_CONFIG: 'SET_CONFIG',
  SET_LOCAL_TILES: 'SET_LOCAL_TILES',

  CHANGE_REGION: 'CHANGE_REGION',
  TOGGLE_FULL_SCREEN: 'TOGGLE_FULL_SCREEN',

  SET_SELECTED_SIGNAL_ID: 'SET_SELECTED_SIGNAL_ID',
  SET_SELECTED_FLAVOR_ID: 'SET_SELECTED_FLAVOR_ID',
  SET_SELECTED_COMPANY_ID: 'SET_SELECTED_COMPANY_ID',
  SET_SELECTED_PAPER_ID: 'SET_SELECTED_PAPER_ID',

  CLEAR_VERTICAL_SIGNALS_FILTERS: 'CLEAR_VERTICAL_SIGNALS_FILTERS',

  SHOW_MEASUREMENT_MODAL: 'SHOW_MEASUREMENT_MODAL',
  HIDE_MEASUREMENT_MODAL: 'HIDE_MEASUREMENT_MODAL',

  SHOW_PHOTO_MODAL: 'SHOW_PHOTO_MODAL',
  HIDE_PHOTO_MODAL: 'HIDE_PHOTO_MODAL',

  SHOW_BARCODE_SCANNER: 'SHOW_BARCODE_SCANNER',
  HIDE_BARCODE_SCANNER: 'HIDE_BARCODE_SCANNER',

  ADD_OR_UPDATE_VERTICAL_SIGNAL: 'ADD_OR_UPDATE_VERTICAL_SIGNAL',
  DELETE_VERTICAL_SIGNAL: 'DELETE_VERTICAL_SIGNAL',

  REMOVE_ID_FROM_VERTICAL_SIGNALS_SYNC_LIST:
    'REMOVE_ID_FROM_VERTICAL_SIGNALS_SYNC_LIST',

  REMOVE_ID_FROM_DELETED_VERTICAL_SIGNALS_SYNC_LIST:
    'REMOVE_ID_FROM_DELETED_VERTICAL_SIGNALS_SYNC_LIST',

  CLEAR_VERTICAL_SIGNALS_SYNC_LIST: 'CLEAR_VS_SYNC_LIST',
  CLEAR_DELETED_VERTICAL_SIGNALS_SYNC_LIST: 'CLEAR_DELETED_VS_SYNC_LIST',

  SET_MEASUREMENTS: 'SET_MEASUREMENTS',

  CLEAR_MEASUREMENTS_LIST: 'CLEAR_MEASUREMENTS_LIST',
  CLEAR_MEASUREMENTS_SYNC_LIST: 'CLEAR_MEASUREMENTS_SYNC_LIST',

  REMOVE_ID_FROM_MEASUREMENTS_SYNC_LIST:
    'REMOVE_ID_FROM_MEASUREMENTS_SYNC_LIST',

  SEARCH_AND_SET_MEASUREMENT: 'SEARCH_AND_SET_MEASUREMENT',
  ADD_MEASUREMENT: 'ADD_MEASUREMENT',
  CHANGE_REFLECTOMETRY_MEASUREMENT: 'CHANGE_REFLECTOMETRY_MEASUREMENT',
  CHANGE_REFLECTOMETRY_COMMENT: 'CHANGE_REFLECTOMETRY_COMMENT',

  CHANGE_ICONS_SIZE: 'CHANGE_ICONS_SIZE',
  CHANGE_SCOPE_SIZE: 'CHANGE_SCOPE_SIZE',
};

export const setInventory = inventory => {
  return {
    type: actions.SET_VERTICAL_SIGNALS_INVENTORY,
    payload: inventory,
  };
};

export const setInspection = inspection => {
  return {
    type: actions.SET_INSPECTION,
    payload: inspection,
  };
};

export const setVerticalSignalsAppData = appData => {
  return {
    type: actions.SET_VERTICAL_SIGNALS_APP_DATA,
    payload: appData,
  };
};

export const setConfig = config => {
  return {
    type: actions.SET_CONFIG,
    payload: config,
  };
};

export const setLocalTiles = path => {
  return {
    type: actions.SET_LOCAL_TILES,
    payload: path,
  };
};

export const changeRegion = region => {
  return {
    type: actions.CHANGE_REGION,
    payload: region,
  };
};

export const toggleFullScreen = () => {
  return {
    type: actions.TOGGLE_FULL_SCREEN,
  };
};

export const setSelectedSignalId = id => {
  return {
    type: actions.SET_SELECTED_SIGNAL_ID,
    payload: id,
  };
};

export const setSelectedFlavorId = id => {
  return {
    type: actions.SET_SELECTED_FLAVOR_ID,
    payload: id,
  };
};

export const setSelectedCompanyId = id => {
  return {
    type: actions.SET_SELECTED_COMPANY_ID,
    payload: id,
  };
};

export const setSelectedPaperId = id => {
  return {
    type: actions.SET_SELECTED_PAPER_ID,
    payload: id,
  };
};

export const clearVerticalSignalFilters = () => {
  return {
    type: actions.CLEAR_VERTICAL_SIGNALS_FILTERS,
  };
};

export const showMeasurementModal = () => {
  return {
    type: actions.SHOW_MEASUREMENT_MODAL,
  };
};

export const hideMeasurementModal = () => {
  return {
    type: actions.HIDE_MEASUREMENT_MODAL,
  };
};

export const showPhotoModal = () => {
  return {
    type: actions.SHOW_PHOTO_MODAL,
  };
};

export const hidePhotoModal = () => {
  return {
    type: actions.HIDE_PHOTO_MODAL,
  };
};

export const showBarcodeScanner = () => {
  return {
    type: actions.SHOW_BARCODE_SCANNER,
  };
};

export const hideBarcodeScanner = () => {
  return {
    type: actions.HIDE_BARCODE_SCANNER,
  };
};

/**
 * Vertical signals
 */
export const addOrUpdateVerticalSignal = signal => {
  return {
    type: actions.ADD_OR_UPDATE_VERTICAL_SIGNAL,
    payload: signal,
  };
};

export const removeIdFromVSSyncLIst = id => {
  return {
    type: actions.REMOVE_ID_FROM_VERTICAL_SIGNALS_SYNC_LIST,
    payload: id,
  };
};

export const deleteVerticalSignal = id => {
  return {
    type: actions.DELETE_VERTICAL_SIGNAL,
    payload: id,
  };
};

export const removeIdFromDeletedVSSyncLIst = id => {
  return {
    type: actions.REMOVE_ID_FROM_DELETED_VERTICAL_SIGNALS_SYNC_LIST,
    payload: id,
  };
};

export const clearVerticalSignalsSyncList = () => {
  return {
    type: actions.CLEAR_VERTICAL_SIGNALS_SYNC_LIST,
  };
};

export const clearDeletedVerticalSignalsSyncList = () => {
  return {
    type: actions.CLEAR_DELETED_VERTICAL_SIGNALS_SYNC_LIST,
  };
};

/**
 * Measurements
 */

export const setMeasurements = measurements => {
  return {
    type: actions.SET_MEASUREMENTS,
    payload: measurements,
  };
};

export const searchAndSetSignalMeasurement = id => {
  return {
    type: actions.SEARCH_AND_SET_MEASUREMENT,
    payload: id,
  };
};

export const addMeasurement = measurement => {
  return {
    type: actions.ADD_MEASUREMENT,
    payload: measurement,
  };
};

export const changeReflectometryMeasurement = measurement => {
  return {
    type: actions.CHANGE_REFLECTOMETRY_MEASUREMENT,
    payload: measurement,
  };
};

export const changeReflectometryComment = comment => {
  return {
    type: actions.CHANGE_REFLECTOMETRY_COMMENT,
    payload: comment,
  };
};

export const clearMeasurementsList = () => {
  return {
    type: actions.CLEAR_MEASUREMENTS_LIST,
  };
};

export const clearMeasurementsSyncList = () => {
  return {
    type: actions.CLEAR_MEASUREMENTS_SYNC_LIST,
  };
};

export const removeIdFromMeasurementSyncList = id => {
  return {
    type: actions.REMOVE_ID_FROM_MEASUREMENTS_SYNC_LIST,
    payload: id,
  };
};

/**
 * Map settings
 */
export const changeIconsSize = size => {
  return {
    type: actions.CHANGE_ICONS_SIZE,
    payload: size,
  };
};

export const changeScopeSize = size => {
  return {
    type: actions.CHANGE_SCOPE_SIZE,
    payload: size,
  };
};

/*-----------------------------------------------------------------------*
 *                              Async Actions
 *-----------------------------------------------------------------------*/

/*-----------------------------------------------------------------*
 *                            Signals
 *-----------------------------------------------------------------*/

export const downloadVerticalSignalsAppData = () => {
  return async dispatch => {
    let flavors, paperTypes, measurementCompanies;

    try {
      [flavors, paperTypes, measurementCompanies] = await Promise.all([
        getVerticalSignalsFlavors(),
        getVerticalSignalsPaperTypes(),
        getMeasurementCompanies(),
      ]);
    } catch (error) {
      return;
    }

    const appData = {
      flavors,
      paperTypes,
      measurementCompanies,
    };

    dispatch(setVerticalSignalsAppData(appData));
  };
};

export const downloadInventory = () => {
  return async dispatch => {
    dispatch(startLoading());
    let inventory;

    try {
      inventory = await getVerticalSignalsInventory();
    } catch (error) {
      dispatch(endLoading());
      return;
    }

    dispatch(clearVerticalSignalsSyncList());
    dispatch(clearDeletedVerticalSignalsSyncList());
    dispatch(setInventory(inventory));
    dispatch(endLoading());
  };
};

export const downloadImages = () => {
  return async dispatch => {
    dispatch(startLoading());

    const progressCb = progress => {
      dispatch(setLoadingProgress(progress));
    };

    try {
      await getVerticalSignalsImages(progressCb);
    } catch (error) {
      showAlert(
        'Ocurrió un error al descargar las imágenes, intente nuevamente.',
      );
    }

    dispatch(endLoading());
  };
};

export const uploadInventory = () => {
  return async (dispatch, getState) => {
    dispatch(startLoading());

    const {
      verticalSignals: {
        verticalSignalsSyncList,
        deletedVerticalSignalsSyncList,
        inventory,
      },
    } = getState();

    const syncListLength =
      verticalSignalsSyncList.length + deletedVerticalSignalsSyncList.length;
    let currentItem = 0;

    /**
     * Add or update signals
     */

    await Promise.all(
      verticalSignalsSyncList.map(async id => {
        const verticalSignal = inventory[id];

        try {
          await uploadVerticalSignal(removeEmptyProperties(verticalSignal));
          dispatch(removeIdFromVSSyncLIst(id));
        } catch (error) {}

        dispatch(setLoadingProgress(++currentItem / syncListLength));
      }),
    );

    /**
     * Delete signals
     */

    await Promise.all(
      deletedVerticalSignalsSyncList.map(async id => {
        try {
          await deleteVerticalSignalApi(id);
          dispatch(removeIdFromDeletedVSSyncLIst(id));
        } catch (error) {}

        dispatch(setLoadingProgress(++currentItem / syncListLength));
      }),
    );

    dispatch(endLoading());
  };
};

/*-----------------------------------------------------------------*
 *                            Measurements
 *-----------------------------------------------------------------*/

export const downloadOrCreateInspection = companyId => {
  return async dispatch => {
    dispatch(startLoading());

    let inspection;

    try {
      inspection = await getVerticalSignalsInspection();
    } catch (error) {
      dispatch(endLoading());
      return;
    }

    if (inspection) {
      dispatch(setInspection(inspection));
      dispatch(endLoading());
      return;
    }

    try {
      inspection = await createVerticalSignalsInspection(companyId);
    } catch (error) {
      dispatch(endLoading());
      return;
    }

    dispatch(setInspection(inspection));
    dispatch(endLoading());
  };
};

export const closeInspection = inspectionId => {
  return async dispatch => {
    dispatch(startLoading());

    try {
      await closeVerticalSignalInspection(inspectionId);
    } catch (error) {
      dispatch(endLoading());
      return;
    }

    dispatch(clearMeasurementsList());
    dispatch(clearMeasurementsSyncList());
    dispatch(setInspection({}));
    dispatch(endLoading());
  };
};

export const downloadMeasurements = inspectionId => {
  return async dispatch => {
    dispatch(startLoading());

    try {
      const measurements = await getMeasurements(inspectionId);
      dispatch(setMeasurements(measurements));
    } catch (error) {}

    dispatch(clearMeasurementsSyncList());
    dispatch(endLoading());
  };
};

export const uploadMeasurements = () => {
  return async (dispatch, getState) => {
    dispatch(startLoading());

    const {
      verticalSignals: { measurements, measurementsSyncList },
    } = getState();

    const syncListLength = measurementsSyncList.length;
    let currentItem = 0;

    await Promise.all(
      measurementsSyncList.map(async id => {
        try {
          await uploadMeasurement(removeEmptyProperties(measurements[id]));
          dispatch(removeIdFromMeasurementSyncList(id));
        } catch (error) {}

        dispatch(setLoadingProgress(++currentItem / syncListLength));
      }),
    );

    dispatch(endLoading());
  };
};
