// Action Types
export const actions = {
  CLEAR_ALL_FILTERS: 'CLEAR_ALL_FILTERS',
  SET_FILTERS: 'SET_FILTERS',

  SET_SECTOR_ID: 'SET_SECTOR_ID',
  CLEAR_SECTOR_ID: 'CLEAR_SECTOR_ID',

  SET_ROAD_ID: 'SET_ROAD_ID',
  CLEAR_ROAD_ID: 'CLEAR_ROAD_ID',

  SET_ROAD_WAY_ID: 'SET_ROAD_WAY_ID',
  CLEAR_ROAD_WAY_ID: 'CLEAR_ROAD_WAY_ID',

  SET_ROAD_SIDE_ID: 'SET_ROAD_SIDE_ID',
  CLEAR_ROAD_SIDE_ID: 'CLEAR_ROAD_SIDE_ID',

  SET_LINE_SIDE_ID: 'SET_LINE_SIDE_ID',
  CLEAR_LINE_SIDE_ID: 'CLEAR_LINE_SIDE_ID',

  SET_FINAL_ABSCISSA: 'SET_FINAL_ABSCISSA',
  SET_INITIAL_ABSCISSA: 'SET_INITIAL_ABSCISSA',
  CLEAR_ABSCISSAS: 'CLEAR_ABSCISSAS',

  SET_DATE: 'SET_DATE',

  ADD_IMAGE: 'ADD_IMAGE',
  REMOVE_IMAGE: 'REMOVE_IMAGE',
  CLEAR_IMAGES: 'CLEAR_IMAGES',
  UPDATE_IMAGE: 'UPDATE_IMAGE',

  CHANGE_COMMENT: 'CHANGE_COMMENT',

  ADD_TAG_TO_CHECKED_LIST: 'ADD_TAG_TO_CHECKED_LIST',
  REMOVE_TAG_FROM_CHECKED_LIST: 'REMOVE_TAG_FROM_CHECKED_LIST',

  SET_TAGS: 'SET_TAGS',
  CLEAR_TAGS: 'CLEAR_TAGS',

  SET_GRASS_MOW_GROUPS: 'SET_GRASS_MOW_GROUPS',

  SET_EXTERNAL_RECORD_SWITCH: 'SET_EXTERNAL_RECORD_SWITCH',
};

// Action Creators
// General
export const clearAllFilters = () => {
  return {
    type: actions.CLEAR_ALL_FILTERS,
  };
};

export const setFilters = filters => {
  return {
    type: actions.SET_FILTERS,
    payload: filters,
  };
};

// Sectors
export const setSectorId = id => {
  return dispatch => {
    dispatch({ type: actions.SET_SECTOR_ID, payload: id });
    dispatch(clearRoadId());
  };
};

export const clearSectorId = () => {
  return { type: actions.CLEAR_SECTOR_ID };
};

/**
 * Roads - set road, and clear abscissas, and roadSide, roadWay,
 * and lineSide.
 */
export const setRoadId = roadId => {
  return dispatch => {
    dispatch({ type: actions.SET_ROAD_ID, payload: roadId });
    dispatch(clearRoadWayId());
    dispatch(clearRoadSideId());
    dispatch(clearLineSideId());
    dispatch(clearAbscissas());
  };
};

export const clearRoadId = () => {
  return {
    type: actions.CLEAR_ROAD_ID,
  };
};

// Road ways
export const setRoadWayId = roadWayId => {
  return {
    type: actions.SET_ROAD_WAY_ID,
    payload: roadWayId,
  };
};

export const clearRoadWayId = () => {
  return {
    type: actions.CLEAR_ROAD_WAY_ID,
  };
};

// Road sides
export const setRoadSideId = roadSideId => {
  return {
    type: actions.SET_ROAD_SIDE_ID,
    payload: roadSideId,
  };
};

export const clearRoadSideId = () => {
  return {
    type: actions.CLEAR_ROAD_SIDE_ID,
  };
};

// Line Sides
export const setLineSideId = lineSideId => {
  return {
    type: actions.SET_LINE_SIDE_ID,
    payload: lineSideId,
  };
};

export const clearLineSideId = () => {
  return {
    type: actions.CLEAR_LINE_SIDE_ID,
  };
};

// Abscissas
export const setInitialAbscissa = abscissa => {
  if (abscissa !== '') {
    abscissa = parseInt(abscissa, 10);
  } else {
    abscissa = null;
  }

  return {
    type: actions.SET_INITIAL_ABSCISSA,
    payload: abscissa,
  };
};

export const setFinalAbscissa = abscissa => {
  if (abscissa !== '') {
    abscissa = parseInt(abscissa, 10);
  } else {
    abscissa = null;
  }

  return {
    type: actions.SET_FINAL_ABSCISSA,
    payload: abscissa,
  };
};

export const clearAbscissas = () => {
  return {
    type: actions.CLEAR_ABSCISSAS,
  };
};

// Date
export const setDate = date => {
  return {
    type: actions.SET_DATE,
    payload: date,
  };
};

// Comments
export const changeComment = comment => {
  return {
    type: actions.CHANGE_COMMENT,
    payload: comment,
  };
};

// Images
export const addImage = image => {
  return {
    type: actions.ADD_IMAGE,
    payload: image,
  };
};

export const removeImage = image => {
  return {
    type: actions.REMOVE_IMAGE,
    payload: image,
  };
};

export const updateImage = image => ({
  type: actions.UPDATE_IMAGE,
  payload: image,
});

export const clearImages = () => {
  return {
    type: actions.CLEAR_IMAGES,
  };
};

// Tags
export const addTagToCheckedList = tag => {
  return {
    type: actions.ADD_TAG_TO_CHECKED_LIST,
    payload: tag,
  };
};

export const removeTagFromCheckedList = tag => {
  return {
    type: actions.REMOVE_TAG_FROM_CHECKED_LIST,
    payload: tag,
  };
};

export const setTags = tags => {
  return {
    type: actions.SET_TAGS,
    payload: tags,
  };
};

export const clearTags = () => {
  return {
    type: actions.CLEAR_TAGS,
  };
};

export const setGrassMowGroups = groups => {
  return {
    type: actions.SET_GRASS_MOW_GROUPS,
    payload: groups,
  };
};

export const setExternalRecordSwitch = switchState => {
  return {
    type: actions.SET_EXTERNAL_RECORD_SWITCH,
    payload: switchState,
  };
};
