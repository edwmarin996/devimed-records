import { actions } from '../actions/tempData';

const initialState = {
  isEditingRecord: false,
  editedRecord: null,

  showLoading: false,
  loadingProgress: 0,
};

const tempData = (state = initialState, action) => {
  switch (action.type) {
    case actions.EDITING_RECORD:
      return { ...state, isEditingRecord: true, editedRecord: action.payload };

    case actions.NOT_EDITING_RECORD:
      return { ...state, isEditingRecord: false, editedRecord: null };

    case actions.START_LOADING:
      return { ...state, showLoading: true };

    case actions.END_LOADING:
      return { ...state, showLoading: false };

    case actions.SET_LOADING_PROGRESS:
      return { ...state, loadingProgress: action.payload };

    default:
      return state;
  }
};

export default tempData;
