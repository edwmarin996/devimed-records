import { actions } from '../actions/auth';

const initialState = {
  refreshToken: null,
  accessToken: null,
  userRoles: [],
  userInfo: {},
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case actions.UPDATE_AUTH_STATE:
      return { ...state, ...action.payload };

    case actions.USER_LOGOUT:
      return { ...initialState };

    default:
      return state;
  }
};

export default auth;
