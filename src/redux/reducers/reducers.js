import { combineReducers } from 'redux';
import filters from './filters';
import appData from './appData';
import auth from './auth';
import tempData from './tempData';
import verticalSignals from './verticalSignals';

// Records reducers
import grassMow from './grassMow';
import horizontalPaint from './horizontalPaint';
import photos from './photos';

const reducers = combineReducers({
  filters,
  appData,
  auth,
  tempData,

  grassMow,
  horizontalPaint,
  photos,

  verticalSignals,
});

export default reducers;
