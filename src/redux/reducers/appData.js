import { actions } from '../actions/appData';

const initialState = {
  sectors: {},
  roads: {},
  roadWays: {},
  roadSides: {},
  oldRoadSides: {},
  lineSides: {},

  // For grassMow records
  grassMowGroups: [],

  // For photos records
  tags: [],

  reduxTreeVersion: '',
};

const appData = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_APP_DATA:
      return { ...state, ...action.payload };

    case actions.SET_REDUX_TREE_VERSION:
      return { ...state, reduxTreeVersion: action.payload };

    default:
      return state;
  }
};

export default appData;
