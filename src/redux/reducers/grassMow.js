import { actions } from '../actions/grassMow';

const initialState = {
  records: {},
  deletedRecords: [],
  deletedImages: [],
};

const grassMow = (state = initialState, action) => {
  switch (action.type) {
    // Record list
    case actions.ADD_GRASS_MOW_RECORD:
      return {
        ...state,
        records: {
          [action.payload.id]: action.payload,
          ...state.records,
        },
      };

    case actions.REMOVE_GRASS_MOW_RECORD:
      let records = { ...state.records };
      delete records[action.payload];
      return { ...state, records };

    case actions.UPDATE_GRASS_MOW_RECORD:
      const oldRecord = state.records[action.payload.id];

      // Keep fields that doesn't exists in payload,
      // (including meta field properties)
      const newRecord = {
        ...oldRecord,
        ...action.payload,

        meta: {
          ...oldRecord.meta,
          ...action.payload.meta,
        },
      };

      state.records[action.payload.id] = newRecord;
      return { ...state, records: { ...state.records } };

    // Deleted lists
    case actions.ADD_GRASS_MOW_RECORD_TO_DELETED_LIST:
      return {
        ...state,
        deletedRecords: [...state.deletedRecords, action.payload],
      };

    case actions.REMOVE_GRASS_MOW_RECORD_FROM_DELETED_LIST:
      let filteredDeletedRecords = [...state.deletedRecords].filter(
        id => id !== action.payload,
      );

      return {
        ...state,
        deletedRecords: filteredDeletedRecords,
      };

    case actions.ADD_GRASS_MOW_IMAGE_TO_DELETED_LIST:
      return {
        ...state,
        deletedImages: [...state.deletedImages, action.payload],
      };

    case actions.REMOVE_GRASS_MOW_IMAGE_FROM_DELETED_LIST:
      let filteredDeletedImages = [...state.deletedImages].filter(
        id => id !== action.payload,
      );

      return {
        ...state,
        deletedImages: filteredDeletedImages,
      };

    default:
      return state;
  }
};

export default grassMow;
