import { actions } from '../actions/filters';

const initialState = {
  selectedSectorId: null,
  selectedRoadId: null,
  selectedRoadWayId: null,
  selectedRoadSideId: null,
  selectedLineSideId: null,

  initialAbscissa: null,
  finalAbscissa: null,

  selectedDate: null,

  comment: null,
  images: [],
  isExternalRecord: false,

  // Fot grassMow records
  selectedGrassMowGroups: [],

  // For photos records
  checkedTags: [],
};

const filters = (state = initialState, action) => {
  switch (action.type) {
    // Sectors
    case actions.SET_SECTOR_ID:
      return { ...state, selectedSectorId: action.payload };

    case actions.CLEAR_SECTOR_ID:
      return { ...state, selectedSectorId: null };

    // Roads
    case actions.SET_ROAD_ID:
      return { ...state, selectedRoadId: action.payload };

    case actions.CLEAR_ROAD_ID:
      return { ...state, selectedRoadId: null };

    // Road ways
    case actions.SET_ROAD_WAY_ID:
      return { ...state, selectedRoadWayId: action.payload };

    case actions.CLEAR_ROAD_WAY_ID:
      return { ...state, selectedRoadWayId: null };

    // Road sides
    case actions.SET_ROAD_SIDE_ID:
      return { ...state, selectedRoadSideId: action.payload };

    case actions.CLEAR_ROAD_SIDE_ID:
      return { ...state, selectedRoadSideId: null };

    // Line sides
    case actions.SET_LINE_SIDE_ID:
      return { ...state, selectedLineSideId: action.payload };

    case actions.CLEAR_LINE_SIDE_ID:
      return { ...state, selectedLineSideId: null };

    // Abscissas
    case actions.SET_INITIAL_ABSCISSA:
      return { ...state, initialAbscissa: action.payload };

    case actions.SET_FINAL_ABSCISSA:
      return { ...state, finalAbscissa: action.payload };

    case actions.CLEAR_ABSCISSAS:
      return { ...state, initialAbscissa: null, finalAbscissa: null };

    case actions.SET_DATE:
      return { ...state, selectedDate: action.payload };

    // Images
    case actions.ADD_IMAGE:
      return { ...state, images: [action.payload, ...state.images] };

    case actions.REMOVE_IMAGE:
      const images = state.images.filter(image => image.id !== action.payload);

      return { ...state, images };

    case actions.UPDATE_IMAGE:
      // Find image and replace it
      const imageIndex = state.images.findIndex(
        image => image.id === action.payload.id,
      );

      state.images[imageIndex] = action.payload;
      return { ...state, images: [...state.images] };

    case actions.CLEAR_IMAGES:
      return { ...state, images: [] };

    // Comment
    case actions.CHANGE_COMMENT:
      return { ...state, comment: action.payload };

    // Tags
    case actions.ADD_TAG_TO_CHECKED_LIST:
      // Add tag to list if doesn't exists
      return {
        ...state,
        checkedTags: [action.payload, ...state.checkedTags],
      };

    case actions.REMOVE_TAG_FROM_CHECKED_LIST:
      const checkedTags = [...state.checkedTags].filter(
        oldTag => oldTag !== action.payload,
      );

      return { ...state, checkedTags };

    case actions.SET_TAGS:
      return { ...state, checkedTags: action.payload };

    case actions.CLEAR_TAGS:
      return { ...state, checkedTags: [] };

    case actions.SET_GRASS_MOW_GROUPS:
      return { ...state, selectedGrassMowGroups: action.payload };

    // External record
    case actions.SET_EXTERNAL_RECORD_SWITCH:
      return { ...state, isExternalRecord: action.payload };

    // Other
    case actions.CLEAR_ALL_FILTERS:
      return { ...initialState };

    case actions.SET_FILTERS:
      return { ...state, ...action.payload };

    default:
      return state;
  }
};

export default filters;
