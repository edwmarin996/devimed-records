import { produce } from 'immer';
import { actions } from '../actions/verticalSignals';

/**
 * Get an approximate zoom level from the deltas.
 * @see https://github.com/react-native-maps/react-native-maps/issues/705#issuecomment-343781344
 */
const getZoomFromRegion = region => {
  return Math.round(Math.log(360 / region.longitudeDelta) / Math.LN2);
};

const initialState = {
  inventory: {},
  flavors: {},
  inspection: {},
  paperTypes: {},
  measurementCompanies: {},

  localTilesPath: '',

  region: {},
  zoom: 20,

  selectedSignalId: null,
  selectedFlavorId: null,
  selectedPaperTypeId: null,
  selectedCompanyId: null,

  showFullScreenMap: false,
  showBottomCard: false,
  showForm: false,
  showMeasurementModal: false,
  showPhotoModal: false,
  showBarcodeScanner: false,
  showScope: false,

  movingSignal: false,

  measurements: {},

  verticalSignalsSyncList: [],
  deletedVerticalSignalsSyncList: [],

  measurementsSyncList: [],

  reflectometryMeasurement: null,
  reflectometryComment: null,

  iconsSize: 35,
  scopeSize: 84,
};

const verticalSignals = produce((draft, action) => {
  switch (action.type) {
    /**
     * App data
     */
    case actions.SET_VERTICAL_SIGNALS_INVENTORY:
      draft.inventory = action.payload;
      draft.verticalSignalsSyncList = [];
      draft.deletedVerticalSignalsSyncList = [];
      break;

    case actions.SET_INSPECTION:
      draft.inspection = action.payload;
      break;

    case actions.SET_VERTICAL_SIGNALS_APP_DATA:
      draft.flavors = action.payload.flavors;
      draft.paperTypes = action.payload.paperTypes;
      draft.measurementCompanies = action.payload.measurementCompanies;
      break;

    /**
     * App behaviour
     */
    case actions.SET_CONFIG:
      Object.entries(action.payload).forEach(([config, value]) => {
        draft[config] = value;
      });
      break;

    case actions.SET_LOCAL_TILES:
      draft.localTilesPath = action.payload;
      break;

    case actions.CHANGE_REGION:
      draft.zoom = getZoomFromRegion(action.payload);
      draft.region = action.payload;
      break;

    case actions.TOGGLE_FULL_SCREEN:
      draft.showFullScreenMap = !draft.showFullScreenMap;
      break;

    case actions.SET_SELECTED_SIGNAL_ID:
      draft.selectedSignalId = action.payload;
      break;

    case actions.SET_SELECTED_FLAVOR_ID:
      draft.selectedFlavorId = action.payload;
      break;

    case actions.SET_SELECTED_COMPANY_ID:
      draft.selectedCompanyId = action.payload;
      break;

    case actions.SET_SELECTED_PAPER_ID:
      draft.selectedPaperTypeId = action.payload;
      break;

    case actions.CLEAR_VERTICAL_SIGNALS_FILTERS:
      draft.selectedFlavorId = null;
      draft.selectedPaperTypeId = null;
      break;

    case actions.SHOW_MEASUREMENT_MODAL:
      draft.showMeasurementModal = true;
      break;

    case actions.HIDE_MEASUREMENT_MODAL:
      draft.showMeasurementModal = false;
      break;

    case actions.SHOW_PHOTO_MODAL:
      draft.showPhotoModal = true;
      break;

    case actions.HIDE_PHOTO_MODAL:
      draft.showPhotoModal = false;
      break;

    case actions.SHOW_BARCODE_SCANNER:
      draft.showBarcodeScanner = true;
      break;

    case actions.HIDE_BARCODE_SCANNER:
      draft.showBarcodeScanner = false;
      break;

    /**
     * VERTICAL SIGNALS
     */
    case actions.ADD_OR_UPDATE_VERTICAL_SIGNAL:
      draft.inventory[action.payload.id] = action.payload;

      if (!draft.verticalSignalsSyncList.includes(action.payload.id)) {
        draft.verticalSignalsSyncList.push(action.payload.id);
      }
      break;

    case actions.REMOVE_ID_FROM_VERTICAL_SIGNALS_SYNC_LIST: {
      const indexOfSyncList = draft.verticalSignalsSyncList.indexOf(
        action.payload,
      );

      if (indexOfSyncList > -1) {
        draft.verticalSignalsSyncList.splice(indexOfSyncList, 1);
      }
      break;
    }

    case actions.DELETE_VERTICAL_SIGNAL: {
      delete draft.inventory[action.payload];

      // For remove element of "verticalSignalsSyncList" if exists
      const indexOfSyncList = draft.verticalSignalsSyncList.indexOf(
        action.payload,
      );

      if (indexOfSyncList > -1) {
        draft.verticalSignalsSyncList.splice(indexOfSyncList, 1);
      }

      if (!draft.deletedVerticalSignalsSyncList.includes(action.payload)) {
        draft.deletedVerticalSignalsSyncList.push(action.payload);
      }
      break;
    }

    case actions.REMOVE_ID_FROM_DELETED_VERTICAL_SIGNALS_SYNC_LIST: {
      const indexOfSyncList = draft.deletedVerticalSignalsSyncList.indexOf(
        action.payload,
      );

      if (indexOfSyncList > -1) {
        draft.deletedVerticalSignalsSyncList.splice(indexOfSyncList, 1);
      }
      break;
    }

    case actions.CLEAR_VERTICAL_SIGNALS_SYNC_LIST:
      draft.verticalSignalsSyncList = [];
      break;

    case actions.CLEAR_DELETED_VERTICAL_SIGNALS_SYNC_LIST:
      draft.deletedVerticalSignalsSyncList = [];
      break;

    /**
     * Measurements
     */
    case actions.SET_MEASUREMENTS:
      draft.measurements = action.payload;
      break;

    case actions.SEARCH_AND_SET_MEASUREMENT:
      const measurement = draft.measurements[action.payload];

      draft.reflectometryMeasurement = measurement?.medicion || null;
      draft.reflectometryComment = measurement?.observacion || null;
      break;

    case actions.ADD_MEASUREMENT:
      draft.measurements[action.payload.senal_inventario_id] = action.payload;

      if (
        !draft.measurementsSyncList.includes(action.payload.senal_inventario_id)
      ) {
        draft.measurementsSyncList.push(action.payload.senal_inventario_id);
      }
      break;

    case actions.CHANGE_REFLECTOMETRY_MEASUREMENT:
      draft.reflectometryMeasurement = action.payload;
      break;

    case actions.CHANGE_REFLECTOMETRY_COMMENT:
      draft.reflectometryComment = action.payload;
      break;

    case actions.CLEAR_MEASUREMENTS_LIST:
      draft.measurements = {};
      break;

    case actions.CLEAR_MEASUREMENTS_SYNC_LIST:
      draft.measurementsSyncList = [];
      break;

    case actions.REMOVE_ID_FROM_MEASUREMENTS_SYNC_LIST:
      const indexOfSyncList = draft.measurementsSyncList.indexOf(
        action.payload,
      );

      if (indexOfSyncList > -1) {
        draft.measurementsSyncList.splice(indexOfSyncList, 1);
      }
      break;

    /**
     * Utils
     */
    case actions.CHANGE_ICONS_SIZE:
      draft.iconsSize = action.payload;
      break;

    case actions.CHANGE_SCOPE_SIZE:
      draft.scopeSize = action.payload;
      break;
  }
}, initialState);

export default verticalSignals;
