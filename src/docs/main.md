# Manual de usuario

### Tabla de contenido

1. [Introducción](#introducción)
2. [Autenticación y Autorización](#autenticación-y-autorización)  
   2.1 [Contraseña olvidada](#contraseña-olvidada)  
    2.2 [Permisos insuficientes](#Permisos-insuficientes)
3. [Navegación por la aplicación](#navegación-por-la-aplicación)
4. [Registros](#registros)  
   4.1 [Crear](#crear-un-registro)  
   4.2 [Editar](#editar-un-registro)  
   4.3 [Eliminar](#eliminar-un-registro)
5. [Rocería](#rocería)
6. [Señales Verticales](#señales-verticales)
7. [Fotografías](#fotografías)
8. [Señales verticales](#señales-verticales)  
   8.1 [Señales](#señales)  
    &nbsp;&nbsp;&nbsp;&nbsp; 8.1.1 [Crear una señal](#crear-una-señal)  
    &nbsp;&nbsp;&nbsp;&nbsp; 8.1.2 [Seleccionar una señal](#seleccionar-una-señal)  
    &nbsp;&nbsp;&nbsp;&nbsp; 8.1.3 [Editar una señal](#editar-una-señal)  
    &nbsp;&nbsp;&nbsp;&nbsp; 8.1.4 [Mover una señal](#mover-una-señal)  
    &nbsp;&nbsp;&nbsp;&nbsp; 8.1.5 [Eliminar una señal](#eliminar-una-señal)  
   8.2 [Mediciones](#mediciones)  
   &nbsp;&nbsp;&nbsp;&nbsp; 8.2.1 [Agregar una medicion](#agregar-una-medicion)  
   8.3 [Sincronización](#sincronización)
9. [Acerca de la aplicación](#acerca-de-la-aplicación)

### Introducción

La aplicación **Registros Devimed** permite la recolección en campo de información de mantenimiento y operación de las vías. Debido a su naturaleza, usted puede operar la aplicación aún cuando no cuente con una conexión a internet, sin embargo, usted debe seguir las siguientes recomendaciónes con el fin de garantizar que la aplicación funcione de manera optima.

- Debe conectarse a internet y abrir la aplicación, al menos cada semana, con el fin de que se sincronicen todos los registros de manera oportuna.

- Debe mantener su aplicación actualizada, de modo que, pueda aprovechar las ultimas características y ajustes de la aplicación.

- Si experimenta algún tipo de error o problema con la aplicación, debe comunicarse inmediatamente con el encargado del desarrollo.

### Autenticación y Autorización

La aplicación **Registros Devimed** se conecta directamente con el sistema de autenticación y autorización de Devimed S.A. por tal motivo, usted debe ingresar con su usuario y contraseña a la aplicación.

La aplicación le solicitará que se identifique en los siguientes casos.

- La primera vez que la instale.
- Cada vez que la aplicación permanezca mas de 15 días sin abrir.
- Cuando el administrador del sistema lo haya solicitado de manera explicita.

#### Contraseña Olvidada

Si usted olvida su contraseña, puede recuperarla, presionando en el enlace _Ha olvidado su contraseña._ y siguiendo las instrucciones de [este video](https://youtu.be/Wy_rJ9DgPwY).

<div align="center">
    <img src="./images/password_recovery.png" alt="Recuperar contraseña" width="200" />
</div>

Si usted no recuerda su correo electrónico, puede solicitar ayuda al administrador del sistema.

#### Permisos insuficientes

Si usted ingresa a la aplicación móvil, y se encuentra con el siguiente mensaje:

<div align="center">
    <img src="./images/unauthorized.png" alt="Permisos insuficientes" width="200" />
</div>

Debe comunicarse con el administrador del sistema para que le asigne los permisos necesarios para acceder a cada una de las funcionalidades de la aplicación.

### Navegación por la aplicación.

Una vez ingrese a la aplicación, podrá visualizar cada una de las secciones de la misma presionando sobre el botón menú, el cual siempre estará visible en la esquina superior izquierda de la aplicación.

<div align="center">
    <img     src="./images/menu.png" alt="Menu" width="200" />
</div>

Una vez presionado el botón menú, se desplegará desde la parte izquierda de la aplicación, el menú de navegación, desde allí usted podrá acceder a cada una de las secciones de la aplicación, presionando sobre ellas.

<div align="center">
    <img src="./images/navigation.png" alt="Menu" width="200" />
</div>

### Registros

Algunas de las secciónes de la aplicación, comparten una metodología similar a la hora de crear, editar o eliminar registros.

#### Crear un registro

Generalmente, la creación de un registro empieza con una pestaña similar a la que se muestra:

<div align="center">
    <img src="./images/create_record.png" alt="Crear registro registro" width="200" />
</div>

Allí usted debe completar todos los campos del formulario, y adjuntar imágenes si así lo solicita, tan pronto como lo haga, usted puede guardar el registro.

Una vez se cree el registro, este se mostrará en la pestaña _Ver registros_, desde allí es posible editarlo o eliminarlo.

#### Editar un registro

Usted puede editar un registro, presionando sobre el icono _Editar_, ubicado en la parte superior derecha de cada uno de los registros.

<div align="center">
    <img src="./images/edit.png" alt="Editar registro" width="200" />
</div>

Esto, lo llevará de nuevo al formulario, donde usted puede modificar los valores del registro y eliminar o adjuntar nuevas imágenes.

#### Eliminar un registro

Usted puede eliminar un registro, presionando sobre el icono _Eliminar_, ubicado en la parte superior derecha de cada uno de los registros.

<div align="center">
    <img src="./images/delete.png" alt="Eliminar registro" width="200" />
</div>

**Nota**  
Usted puede editar y/o eliminar los registros hasta 36 horas después de creados; después de este tiempo, las opciones editar/eliminar desaparecerán.

### Rocería

La sección de rocería permite registrar el avance de una cuadrílla de rocería, en un tramo de la vía.

Usted puede gestionar los registros de rocería de acuerdo a [esta sección](#registros).

### Señalización Horizontal

La sección de señalización horizontal permite registrar el avance de un grupo de pintura horizontal, en un tramo de la vía.

Usted puede gestionar los registros de señalización horizontal, de acuerdo a [esta sección](#registros).

### Fotografías

La sección de fotografías, le permite registrar eventos genéricos y especificar a que tipo de evento pertenece cada foto. En este tipo de registros, las fotografías son lo mas importante, por eso debe etiquetar cada una de las fotos que capture.

Usted puede crear un registro fotográfico de la siguiente forma:

- Adjunte una o varias imágenes al registro, presionando los botones _Capturar imágen_ o _Subir desde la galería_.

<div align="center">
    <img src="./images/capture.png" alt="Capturar imágenes" width="200" />
</div>

- Etiquete cada una de las imágenes que adjuntó, presionando sobre el icono _Etiquetar_ y seleccionando una o **varias** categorías en las cuales se puede clasificar la imagen.

<div align="center">
    <img src="./images/tag.png" alt="Etiquetar imagen" width="200" />
    <img src="./images/tag_2.png" alt="Etiquetar imagen" width="200" />
</div>

- Una vez etiquetadas todas la imágenes, **continúe** a la siguiente pestaña y complete los datos solicitados.

- En caso de que las fotografías pertenezcan a una vía, rellene los campos del formulario; de otro modo, si las fotografías pertenecen a algún lugar fuera de la concesión, establezca el registro como _Registro Externo_ presionando el switch en la parte superior del formulario.

<div align="center">
    <img src="./images/external.png" alt="Registro externo" width="200" />
</div>

Una vez creado, el registro fotográfico se mostrara en la pestaña _Ver registros_, desde allí usted puede [Editarlo](#editar-un-registro) o [Eliminarlo](#eliminar-un-registro).

### Señales Verticales

La sección de señales verticales le permite gestionar el **Inventario** de señales verticales dentro de la concesión, asi como **registrar las mediciones** de reflectometría vertical de cada una de estas señales.

Esta sección, cuenta con dos pestañas:

- La pestaña de mapa:

  desde allí es posible ver las señales verticales, así como moverlas y ver el detalle de cada una.

- La pestaña de configuración:

  Muestra información acerca de las señales y mediciones, además de permitir la sincronización de los datos, la gestión de las inspecciones, y la configuración de parámetros de la aplicación.

<div align="center">
    <img src="./images/vertical_signals_t1.png" alt="Señales verticales pestaña 1" width="200" />
    <img src="./images/vertical_signals_t2.png" alt="Señales verticales pestaña 2" width="200" />
</div>

**Nota**  
En la sección de señales verticales, es **responsabilidad del usuario** sincronizar los datos, así como abrir y cerrar las inspecciones. Esto se hace con el fin de evitar inconsistencias entre diversos dispositivos.

#### Señales

Para gestionar el inventario de señales verticales, es necesario que lo descargue, para eso puede seguir los siguientes pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Inventario_.
- Presione en el botón _Importar_.

**Nota**
Opcionalmente, usted puede importar las imágenes de las señales verticales para su uso sin conexión, para eso presione sobre el elemento _Importar Imágenes_, ubicado dentro del menú mencionado anteriormente.

##### Crear una señal

Para crear una señal vertical, usted debe seguir estos pasos:

- Diríjase a la pestaña mapa.
- Presione y **Mantenga presionado** cualquier lugar dentro del mapa, hasta que el objetivo aparezca en pantalla.

<div align="center">
    <img src="./images/create_signal_s1.png" alt="Crear señal" width="200" />
</div>

- Una vez aparezca el objetivo, muévalo hasta el punto donde quiere agregar la señal.

<div align="center">
    <img src="./images/create_signal_s2.png" alt="Crear señal" width="200" />
</div>

- Cuando haya definido el lugar en donde creará la señal vertical, presione en el botón + ubicado en la esquina inferior derecha de la pantalla. Esto deberá abrir el formulario para que complete alguna información de la señal.

<div align="center">
    <img src="./images/create_signal_s3.png" alt="Crear señal" width="200" />
</div>

- Complete los datos solicitados en el formulario y presione en el botón _guardar_.

<div align="center">
    <img src="./images/create_signal_s4.png" alt="Crear señal" width="200" />
</div>

##### Seleccionar una señal

- Para selecciónar una señal, usted deberá presionar sobre ella.

<div align="center">
    <img src="./images/select_signal_s1.png" alt="Seleccionar señal" width="200" />
</div>

- Una vez lo haga, aparecerá una tarjeta con toda la información relacionada a ella.

<div align="center">
    <img src="./images/select_signal_s2.png" alt="Seleccionar señal" width="200" />
</div>

**Nota**  
Una vez seleccionada, usted puede presionar sobre la imagen de la señal vertical para verla en tamaño completo.

##### Editar una señal

Para editar/editar una señal, usted debe seguir estos pasos:

- Seleccione la señal, de acuerdo a lo descrito [anteriormente](#seleccionar-una-señal).

- Presione sobre el icono de editar señal, después de esto, se abrirá un formulario para que edite la información de la señal.

<div align="center">
    <img src="./images/edit_signal_s1.png" alt="Editar señal" width="200" />
</div>

- Edite los parámetros de la señal en el formulario, y haga presione sobre el botón _guardar_.

<div align="center">
    <img src="./images/edit_signal_s2.png" alt="Editar señal" width="200" />
</div>
 
##### Mover una señal

Para mover una señal, usted debe seguir los siguientes pasos:

- Seleccione la señal, de acuerdo a lo descrito [anteriormente](#seleccionar-una-señal).

- Presione sobre el ícono _mover señal_, en ese momento deberá aparecer el objetivo en pantalla.

<div align="center">
    <img src="./images/move_signal.png" alt="Mover señal" width="200" />
</div>

- Mueva el objetivo a la lugar en donde quiere reubicar la señal.

- Presione nuevamente sobre el botón _mover señal_.

##### Eliminar una señal

Para eliminar una señal, usted debe seguir estos pasos:

- Seleccione la señal, de acuerdo a lo descrito [anteriormente](#seleccionar-una-señal).

- Presione el botón _eliminar señal_.

<div align="center">
    <img src="./images/delete_signal.png" alt="Eliminar señal" width="200" />
</div>

#### Mediciones

Para gestionar las mediciones de reflectividad, es necesario que tenga una **inspección abierta**, para eso puede seguir estos pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Inspeccion_.
- Presione en el botón _Obtener_.

Una vez obtenida la inspección, usted puede empezar a crear mediciones de reflectividad, o importar las ya existentes. Para importar las mediciones, siga estos pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Mediciones_.
- Presione en el botón _Importar_.

##### Agregar una medicion

Para agregar una medicion de reflectividad, siga estos pasos:

- Seleccione una señal, de acuerdo a [esta sección](#seleccionar-una-señal).

- Presione sobre el ícono _agregar medición_.

<div align="center">
    <img src="./images/add_measurement_s1.png" alt="Agregar medicion" width="200" />
</div>

- A continuación se desplegara una ventana en donde usted puede agregar el valor de la medicion, e información adicional, si así lo desea.

<div align="center">
    <img src="./images/add_measurement_s2.png" alt="Agregar medicion" width="200" />
</div>

- Agregue la información solicitada, y presione sobre el botón guardar.

#### Sincronización

Usted debe encargarse de sincronizar los cambios locales que haya hecho al inventario de señales verticales, o las mediciones que haya agregado. Esto debe hacerse de manera frecuente, con el fín de garantizar el acceso oportuno a la información.

Para sincronizar el inventario de señales verticales siga estos pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Inventario_.
- Presione en el botón _Exportar_.

Para sincronizar las mediciones de reflectividad siga estos pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Mediciones_.
- Presione en el botón _Exportar_.

Una vez que haya terminado de agregar los registros de reflectividad, usted debe cerrar la inspección. Para esto, siga estos pasos:

- Diríjase a la pestaña de configuración.
- Dentro de los _Ajustes de sincronización_ presione el menú _Inspección_.
- Presione en el botón _Cerrar_.

**Nota**  
Tenga cuidado de exportar las mediciones de reflectividad antes de cerrar la inspección, esto es debido a que **las mediciones locales se eliminan** una vez usted cierre la inspección.

### Acerca de la aplicación

La pestaña _Acerca de la aplicación_ brinda información acerca de el desarrollo de la aplicación, así como algunas opciones adicionales como restablecer los datos de la aplicación, u obtener la información necesaria para su funcionamiento.

En general los usuarios no necesitan manipular la sección de configuración, a menos que, así se les indique con el fin de obtener información de errores o problemas que pueda presentar el aplicativo.
