import jwtDecode from 'jwt-decode';
import { store } from '../redux/store/store';
import { userLogout, updateAuthState } from '../redux/actions/auth';
import { keyCloakConfig } from '../config/config';

// Define keyCloak url
const authUrl =
  keyCloakConfig.AUTH_SERVER_URL +
  'realms/' +
  keyCloakConfig.realm +
  '/protocol/openid-connect/token';

export const authServerErrors = {
  'Invalid user credentials':
    'Las credenciales de usuario son incorrectas, verifique e intente nuevamente.',

  'Offline tokens not allowed for the user or client':
    'Al parecer no tiene acceso (offline) a esta aplicación, contacte al administrador del sistema para resolver el problema.',

  'Account is not fully set up':
    'El sistema requiere que verifique y/o actualice su cuenta.\n\n¿Desea navegar hacia el sistema de autenticación para completar el proceso?',

  'Account disabled':
    'Su cuenta se encuentra deshabilitada, contacte al administrador del sistema para resolver el problema.',
};

async function sendAuthRequest(URL, requestOptions) {
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');
  requestOptions.headers = myHeaders;

  try {
    return await fetch(URL, requestOptions);
  } catch (error) {
    return null;
  }
}

// Get a pair of access token, and refresh token from user credentials
export async function getAccessToken(username, password) {
  const body = {
    client_id: keyCloakConfig.resource,
    grant_type: 'password',
    scope: 'offline_access email profile roles',
    username: username.trim(),
    password,
  };

  const requestOptions = {
    method: 'POST',
    body: new URLSearchParams(body).toString(),
  };

  let response = await sendAuthRequest(authUrl, requestOptions);
  const responseIsOk = response.ok;

  if (!response) {
    return {
      ok: false,
      error:
        'Al parecer no está conectado a internet, verifique su conexión e intente nuevamente.',
    };
  }

  try {
    response = await response.json();
  } catch (error) {
    return {
      ok: false,
      error:
        'Existe un error con el servidor de autenticación, contacte al administrador del sistema para resolver el problema.',
    };
  }

  if (!responseIsOk) {
    const error = response.error_description;

    return {
      ok: false,
      error:
        authServerErrors[error] ||
        'Se presentó un error no identificado, informe al administrador del sistema inmediatamente.',
    };
  }

  let decodedJWT = jwtDecode(response.access_token);
  const payload = {
    accessToken: response.access_token,
    refreshToken: response.refresh_token,
    userRoles:
      decodedJWT.resource_access?.[keyCloakConfig.resource]?.roles || [],

    userInfo: {
      fullName: decodedJWT.name,
      name: decodedJWT.given_name,
      lastName: decodedJWT.family_name,
      email: decodedJWT.email,
    },
  };

  store.dispatch(updateAuthState(payload));
  return { ok: true };
}

// Get a pair of access token, and refresh token from old refresh token
export async function refreshAccessToken() {
  console.log('Trying to refresh token');
  const {
    auth: { refreshToken },
  } = store.getState();

  // To cancel token refresh if RefreshToken does not exists
  if (!refreshToken) {
    return;
  }

  const body = {
    client_id: keyCloakConfig.resource,
    grant_type: 'refresh_token',
    scope: 'offline_access email profile roles',
    refresh_token: refreshToken,
  };

  const requestOptions = {
    method: 'POST',
    body: new URLSearchParams(body).toString(),
  };

  let response = await sendAuthRequest(authUrl, requestOptions);

  if (response && (response.status === 401 || response.status === 400)) {
    // Dispatch unauthorized (The user will be  logged out)
    store.dispatch(userLogout());
    console.log('Refresh token fail : 400 || 401');
    return;
  } else if (response && response.ok) {
    response = await response.json();
    let decodedJWT = jwtDecode(response.access_token);

    const payload = {
      accessToken: response.access_token,
      refreshToken: response.refresh_token,

      userRoles:
        decodedJWT.resource_access?.[keyCloakConfig.resource]?.roles || [],

      userInfo: {
        fullName: decodedJWT.name,
        name: decodedJWT.given_name,
        lastName: decodedJWT.family_name,
        email: decodedJWT.email,
      },
    };

    store.dispatch(updateAuthState(payload));
    console.log('Refresh token success');
    return;
  }
  console.log('Refresh token fail : Network error');
}
