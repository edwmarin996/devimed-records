import { store } from '../redux/store/store';
import syncStates from './syncStates';
import config from './../config/config';

/*---------------------------------------------------------------
 *
 *           Auxiliar functions to handle records sync
 *
 *---------------------------------------------------------------*/

export async function sendHttpRequest(ENDPOINT, requestOptions) {
  // Append credentials to request headers
  const state = store.getState();
  const token = state.auth.accessToken;

  requestOptions.headers.append('Authorization', token);

  try {
    return await fetch(config.SERVER_URL + ENDPOINT, requestOptions);
  } catch (error) {
    console.log('Fail Fetch - Sync Module: ' + ENDPOINT);
    console.log(error);
    return null;
  }
}

/**
 * @description Function to handle upload records data
 * @param {Array} records - The array of records, that will be synchronized
 * @param {Function} storeAction - Function that will be called once the resource is synchronized
 * @param {Object} fetchConfig - Request options
 * @param {String} [fetchConfig.endPoint] - The endpoint of resource that will be created
 * @param {String} [fetchConfig.method] - HTTP method that will be used (Usually 'POST' or 'PUT')
 */
export async function syncRecords(records, storeAction, fetchConfig) {
  const headers = new Headers({
    'Content-Type': 'application/json',
  });

  await Promise.all(
    records.map(async record => {
      // To update sync state before begin synchronization
      const lastSyncState = record.meta.syncState;

      // To update only sync states before begin synchronization
      const recordToUpdate = {
        id: record.id,
        meta: {
          syncState: syncStates.isSynchronizing,
        },
      };

      store.dispatch(storeAction(recordToUpdate));

      const requestOptions = {
        method: fetchConfig.method,
        body: JSON.stringify(record),
        headers,
      };

      const recordId = fetchConfig.method === 'PUT' ? `/${record.id}` : '';

      let response = await sendHttpRequest(
        fetchConfig.endPoint + recordId,
        requestOptions,
      );

      //If the record was created successfully update sync state
      if (response && response.ok) {
        // If the record doesn't have images
        if (!record.images || record.images.length === 0) {
          recordToUpdate.meta.syncState = syncStates.isFullySynchronized;
        } else {
          recordToUpdate.meta.syncState = syncStates.isPartiallySynchronized;
        }
      } else {
        console.log('Fail uploading record: ');
        console.log(await response.text());

        //Return record to original state
        recordToUpdate.meta.syncState = lastSyncState;
      }

      store.dispatch(storeAction(recordToUpdate));
    }),
  );
}

/**
 * @description Function to handle images uploading
 * @param {Array} records - The array of records, whose images will be synchronized
 * @param {Function} storeAction - Function that will be called once the resource is synchronized
 * @param {Object} fetchConfig - Request options
 * @param {String} [fetchConfig.endPoint] - The endpoint of resource that will be created
 * @param {String} [fetchConfig.method] - HTTP method that will be used (Usually 'POST')
 * @param {String} [fetchConfig.idRecordName] - name of id of relational example: roceria_cuneta_id
 */
export function syncImages(records, storeAction, fetchConfig) {
  records.forEach(async record => {
    // To update only sync states before begin synchronization
    const recordToUpdate = {
      id: record.id,
      meta: {
        syncState: syncStates.isSynchronizing,
      },
    };

    store.dispatch(storeAction(recordToUpdate));

    // Filter only non synchronized images
    const images = record.images.filter(
      image => image.meta.syncState === syncStates.isNotSynchronized,
    );

    const newImagesState = await Promise.all(
      images.map(async image => {
        const data = new FormData();

        data.append('foto', {
          name: `${image.id}.${image.type.split('/')[1]}`, //To add image extension to the name
          type: image.type,
          uri: image.uri,
        });

        data.append(fetchConfig.idRecordName, record.id);
        data.append('id', image.id);

        // Append tags if tags array exists
        if (image?.tags?.length > 0) {
          image.tags.forEach(tag => {
            data.append('etiquetas', tag);
          });
        }

        const requestOptions = {
          method: fetchConfig.method,
          body: data,
          headers: new Headers(),
        };

        let response = await sendHttpRequest(
          fetchConfig.endPoint,
          requestOptions,
        );

        //If the image was uploaded successfully update image state
        if (response && response.ok) {
          image.meta.syncState = syncStates.isFullySynchronized;
        } else {
          console.log('Fail uploading image: ');
          console.log(await response.text());
        }

        return image;
      }),
    );

    // Verify if all images was synchronized
    const allImagesWasSynchronized = newImagesState.every(
      image => image.meta.syncState === syncStates.isFullySynchronized,
    );

    if (allImagesWasSynchronized) {
      recordToUpdate.meta.syncState = syncStates.isFullySynchronized;
    } else {
      recordToUpdate.meta.syncState = syncStates.isPartiallySynchronized;
    }

    store.dispatch(storeAction(recordToUpdate));
  });
}

/**
 * @description Function to handle deleted resources
 * @param {Array} Ids - The UUIDs of the resources that will be deleted
 * @param {Function} storeAction - Function that will be called once the resource is removed
 * @param {Object} fetchConfig - Request options
 * @param {String} [fetchConfig.endPoint] - The endpoint of resource that will be deleted
 * @param {String} [fetchConfig.method] - HTTP method that will be used (Usually 'DELETE')
 */
export function syncDeleted(Ids, storeAction, fetchConfig) {
  const requestOptions = {
    method: fetchConfig.method,
    headers: new Headers(),
  };

  Ids.forEach(async id => {
    let response = await sendHttpRequest(
      fetchConfig.endPoint + `/${id}`,
      requestOptions,
    );

    if (response && response.ok) {
      // delete record from deleted list
      store.dispatch(storeAction(id));
    }
  });
}
