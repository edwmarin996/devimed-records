import config from '../config/config';
import { sendRequest } from './appData';
import { unzip } from 'react-native-zip-archive';
import {
  unlink,
  ExternalDirectoryPath,
  downloadFile,
  mkdir,
} from 'react-native-fs';

/*-----------------------------------------------------------------*
 *                           Signals
 *-----------------------------------------------------------------*/

export function getVerticalSignalsPaperTypes() {
  return sendRequest(config.VERTICAL_SIGNALS_PAPER_TYPES);
}

export function getVerticalSignalsFlavors() {
  return sendRequest(config.VERTICAL_SIGNALS_FLAVORS);
}

export async function getVerticalSignalsInventory() {
  const inventory = await sendRequest(config.VERTICAL_SIGNALS_INVENTORY);

  const flattenedInventory = inventory.reduce(
    (accumulator, verticalSignal) => {
      verticalSignal.coordinates = {
        longitude: verticalSignal.geom.coordinates[0],
        latitude: verticalSignal.geom.coordinates[1],
      };

      // Split "foto" in url and filename
      try {
        const splittedPath = verticalSignal.foto.split('/');
        const photoName = splittedPath.pop();
        const photoUrl = splittedPath.join('/');

        verticalSignal.photoName = photoName;
        verticalSignal.photoUrl = photoUrl;

        delete verticalSignal.foto;
      } catch (error) {}

      accumulator[verticalSignal.id] = verticalSignal;
      return accumulator;
    },

    {},
  );

  return flattenedInventory;
}

export async function getVerticalSignalsImages(progressCb) {
  const imagesFolder = `${ExternalDirectoryPath}/images`;
  const zipFile = `${imagesFolder}/compress.zip`;
  const fromUrl = `${config.SERVER_URL}${config.VERTICAL_SIGNALS_IMAGES}/${
    config.LOCAL_IMAGE_RESOLUTION
  }.zip`;

  await mkdir(imagesFolder);
  await downloadFile({
    fromUrl,
    toFile: zipFile,
    progressInterval: 3000,
    progress: ({ bytesWritten, contentLength }) => {
      const progress = bytesWritten / contentLength;
      progressCb(progress);
    },
  }).promise;

  await unzip(zipFile, imagesFolder);
  await unlink(zipFile);
}

export function uploadVerticalSignal(signal) {
  const body = new FormData();

  // Pick fields from signal
  const {
    coordinates,
    foto,
    photoNameAlt,
    photoName,
    photoUrl,
    foto_lista_escalas,
    ...pickedFields
  } = signal;

  pickedFields.geom = JSON.stringify(pickedFields.geom);

  if (signal.photoNameAlt) {
    body.append('foto', {
      name: `${signal.photoNameAlt.id}.${
        signal.photoNameAlt.type.split('/')[1]
      }`,
      type: signal.photoNameAlt.type,
      uri: signal.photoNameAlt.uri,
    });
  }

  Object.entries(pickedFields).forEach(([key, value]) => {
    body.append(key, value);
  });

  const requestOptions = {
    method: 'POST',
    body,
  };

  return sendRequest(config.VERTICAL_SIGNALS_INVENTORY, requestOptions);
}

export function deleteVerticalSignal(id) {
  const requestOptions = {
    method: 'DELETE',
  };

  return sendRequest(
    `${config.VERTICAL_SIGNALS_INVENTORY}/${id}`,
    requestOptions,
  );
}

/*-----------------------------------------------------------------*
 *                            Measurements
 *-----------------------------------------------------------------*/

export function getMeasurementCompanies() {
  return sendRequest(config.MEASUREMENT_COMPANIES);
}

export async function getVerticalSignalsInspection() {
  const inspections = await sendRequest(
    `${config.INSPECTIONS_ENDPOINT}/${
      config.VERTICAL_SIGNALS_INSP_CODE
    }?visible=${true}&finalizada=${false}`,
  );

  return inspections[0];
}

export async function createVerticalSignalsInspection(companyId) {
  const date = new Date().toISOString().split('T')[0];
  const headers = new Headers({
    'Content-Type': 'application/json',
  });

  const inspection = {
    fecha_inicial: date,
    fecha_final: date,
    empresa: companyId,
    finalizada: false,
    visible: false,
  };

  const requestOptions = {
    headers,
    method: 'POST',
    body: JSON.stringify(inspection),
  };

  return sendRequest(
    `${config.INSPECTIONS_ENDPOINT}/${config.VERTICAL_SIGNALS_INSP_CODE}`,
    requestOptions,
  );
}

export async function closeVerticalSignalInspection(inspectionId) {
  const date = new Date().toISOString().split('T')[0];
  const headers = new Headers({
    'Content-Type': 'application/json',
  });

  const inspection = {
    fecha_final: date,
    finalizada: true,
    visible: true,
  };

  const requestOptions = {
    headers,
    method: 'PATCH',
    body: JSON.stringify(inspection),
  };

  return sendRequest(
    `${config.INSPECTIONS_ENDPOINT}/${inspectionId}`,
    requestOptions,
  );
}

export async function getMeasurements(inspectionId) {
  let measurements = await sendRequest(
    `${config.VERTICAL_SIGNALS_MEASUREMENTS}?inspeccion_id=${inspectionId}`,
  );

  return measurements.reduce((accumulator, measurement) => {
    accumulator[measurement.senal_inventario_id] = measurement;
    return accumulator;
  }, {});
}

export function uploadMeasurement(measurement) {
  const headers = new Headers({
    'Content-Type': 'application/json',
  });

  const requestOptions = {
    headers,
    method: 'POST',
    body: JSON.stringify(measurement),
  };

  return sendRequest(config.VERTICAL_SIGNALS_MEASUREMENTS, requestOptions);
}
