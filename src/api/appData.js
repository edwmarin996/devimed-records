import config from '../config/config';
import { store } from '../redux/store/store';

export async function sendRequest(ENDPOINT, requestOptions = {}) {
  // Append credentials to request headers
  const state = store.getState();
  const token = state.auth.accessToken;

  if (!requestOptions.headers) {
    requestOptions.headers = new Headers();
  }

  requestOptions.headers.append('Authorization', token);

  let response;
  try {
    response = await fetch(config.SERVER_URL + ENDPOINT, requestOptions);
  } catch (error) {
    console.log('Fail Fetch: ' + ENDPOINT);
    console.log(error);
    throw error;
  }

  if (!response || !response.ok) {
    console.log('Response Not Ok: ' + ENDPOINT);

    // These lines should not be removed, because these guarantees
    // that "Promise.all" will fail if the expected values
    // are not returned.
    const error = await response.text();
    throw new Error(error);
  }

  try {
    const json = await response.json();
    console.log('Success Request: ' + ENDPOINT);
    return json;
  } catch (error) {
    console.log('Failed parsing response to JSON');
    return {};
  }
}

/*-------------------------------------------------------------------------
 *                        GET DATA FROM SERVER
 *-----------------------------------------------------------------------*/
export async function getSectors() {
  let sectors = await sendRequest(config.SECTORS_ENDPOINT);

  return sectors.reduce((accumulator, sector) => {
    accumulator[sector.id] = sector;
    return accumulator;
  }, {});
}

export async function getRoads() {
  let roads = await sendRequest(config.ROADS_ENDPOINT);

  return roads.reduce((accumulator, road) => {
    accumulator[road.id] = road;
    return accumulator;
  }, {});
}

export async function getRoadWays() {
  return sendRequest(config.ROADWAYS_ENDPOINT);
}

export async function getRoadSides() {
  return sendRequest(config.ROAD_SIDES_ENDPOINT);
}

export async function getOldRoadSides() {
  let roadSideTypes = await sendRequest(config.ROAD_SIDE_TYPES_ENDPOINT);
  let oldRoadSides = await sendRequest(config.OLD_ROAD_SIDES_ENDPOINT);

  oldRoadSides = oldRoadSides.map(roadSide => {
    let sideName = roadSideTypes.find(
      type => type.id === roadSide.tipo_costado_id,
    ).nombre;

    roadSide.nombre = sideName;
    return roadSide;
  });

  oldRoadSides = oldRoadSides.reduce((accumulator, roadSide) => {
    accumulator[roadSide.id] = roadSide;
    return accumulator;
  }, {});

  return oldRoadSides;
}

export async function getLineSides() {
  let lineSides = await sendRequest(config.LINE_SIDES_ENDPOINT);

  Object.keys(lineSides).forEach(id => {
    lineSides[id].id = id;
  });

  return lineSides;
}

// For grassMow records
export function getGrassMowGroups() {
  return sendRequest(config.GRASS_MOW_GROUPS_ENDPOINT);
}

// For photos records
export async function getTags() {
  let tags = await sendRequest(config.PHOTO_RECORDS_TAGS);

  tags = tags.sort((tagOne, tagTwo) => {
    // To capitalize tags
    tagOne.nombre =
      tagOne.nombre.charAt(0).toUpperCase() + tagOne.nombre.slice(1);

    tagTwo.nombre =
      tagTwo.nombre.charAt(0).toUpperCase() + tagTwo.nombre.slice(1);

    return tagOne.nombre > tagTwo.nombre ? 1 : -1;
  });

  return tags;
}
