import NetInfo from '@react-native-community/netinfo';
import { store } from '../redux/store/store';
import config from './../config/config';
import { refreshAccessToken } from './authModule';
import syncStates from './syncStates';
import * as helpers from './helpers';

import {
  updateGrassMowRecord,
  removeGrassMowRecordFromDeletedList,
  removeGrassMowImageFromDeletedList,
} from '../redux/actions/grassMow';

import {
  updateHorizontalPaintRecord,
  removeHorizontalPaintRecordFromDeletedList,
  removeHorizontalPaintImageFromDeletedList,
} from '../redux/actions/horizontalPaint';

import {
  updatePhotoRecord,
  removePhotoRecordFromDeletedList,
  removePhotoImageFromDeletedList,
} from '../redux/actions/photos';

/*---------------------------------------------------------------
 *
 *              Module to handle records sync
 *
 *-------------------------------------------------------------*/

export default async function synchronize() {
  // Check Internet connection
  if (!(await NetInfo.fetch()).isInternetReachable) {
    return;
  }

  await refreshAccessToken();

  console.log('Synchronization begins\n');

  return await Promise.all([
    syncGrassMow(),
    syncHorizontalPaint(),
    syncPhotosRecords(),
  ]);
}

/*--------------------------------------------------------------------
 *
 *           Functions to handle individual synchronization
 *
 *--------------------------------------------------------------------*/

/*--------------------------------------------------------------------
 *                            GrassMow
 *--------------------------------------------------------------------*/
async function syncGrassMow() {
  const state = store.getState();
  const records = Object.values(state.grassMow.records);
  const deletedRecordsIds = state.grassMow.deletedRecords;
  const deletedImagesIds = state.grassMow.deletedImages;
  const notSynchronizedRecords = records.filter(
    record => record.meta.syncState === syncStates.isNotSynchronized,
  );

  // To create or update records
  await helpers.syncRecords(notSynchronizedRecords, updateGrassMowRecord, {
    endPoint: config.GRASS_MOW_RECORD_ENDPOINT,
    method: 'POST',
  });

  // To synchronize deleted records
  helpers.syncDeleted(deletedRecordsIds, removeGrassMowRecordFromDeletedList, {
    endPoint: config.GRASS_MOW_RECORD_ENDPOINT,
    method: 'DELETE',
  });

  // To synchronize deleted images
  helpers.syncDeleted(deletedImagesIds, removeGrassMowImageFromDeletedList, {
    endPoint: config.GRASS_MOW_IMAGE_ENDPOINT,
    method: 'DELETE',
  });

  /*               To synchronize images
  ? Note that it is necessary to read the store again, because the
  ? "syncRecords" methods could modify the state of the records,
  ? and set it in "isPartiallySynchronized"
  */
  const latestState = store.getState();
  const latestRecords = Object.values(latestState.grassMow.records);

  const partiallySynchronizedRecords = latestRecords.filter(
    record => record.meta.syncState === syncStates.isPartiallySynchronized,
  );

  helpers.syncImages(partiallySynchronizedRecords, updateGrassMowRecord, {
    endPoint: config.GRASS_MOW_IMAGE_ENDPOINT,
    method: 'POST',
    idRecordName: 'roceria_cuneta_id',
  });

  // To return records stuck in state x, to a state that allows them to be
  // synchronized later
  latestRecords.forEach(record => {
    if (record.meta.syncState === syncStates.isSynchronizing) {
      record.meta.syncState = syncStates.isNotSynchronized;
      updateGrassMowRecord(record);
    }
  });
}

/*--------------------------------------------------------------------
 *                         HorizontalPaint
 *--------------------------------------------------------------------*/
async function syncHorizontalPaint() {
  const state = store.getState();
  const records = Object.values(state.horizontalPaint.records);
  const deletedRecordsIds = state.horizontalPaint.deletedRecords;
  const deletedImagesIds = state.horizontalPaint.deletedImages;
  const notSynchronizedRecords = records.filter(
    record => record.meta.syncState === syncStates.isNotSynchronized,
  );

  // To create or update records
  await helpers.syncRecords(
    notSynchronizedRecords,
    updateHorizontalPaintRecord,
    {
      endPoint: config.HORIZONTAL_PAINT_RECORD_ENDPOINT,
      method: 'POST',
    },
  );

  // To synchronize deleted records
  helpers.syncDeleted(
    deletedRecordsIds,
    removeHorizontalPaintRecordFromDeletedList,
    {
      endPoint: config.HORIZONTAL_PAINT_RECORD_ENDPOINT,
      method: 'DELETE',
    },
  );

  // To synchronize deleted images
  helpers.syncDeleted(
    deletedImagesIds,
    removeHorizontalPaintImageFromDeletedList,
    {
      endPoint: config.HORIZONTAL_PAINT_IMAGE_ENDPOINT,
      method: 'DELETE',
    },
  );

  /*               To synchronize images
  ? Note that it is necessary to read the store again, because the
  ? "syncRecords" methods could modify the state of the records,
  ? and set it in "isPartiallySynchronized"
  */
  const latestState = store.getState();
  const latestRecords = Object.values(latestState.horizontalPaint.records);

  const partiallySynchronizedRecords = latestRecords.filter(
    record => record.meta.syncState === syncStates.isPartiallySynchronized,
  );

  helpers.syncImages(
    partiallySynchronizedRecords,
    updateHorizontalPaintRecord,
    {
      endPoint: config.HORIZONTAL_PAINT_IMAGE_ENDPOINT,
      method: 'POST',
      idRecordName: 'pintura_horizontal_id',
    },
  );

  // To return records stuck in state x, to a state that allows them to be
  // synchronized later
  latestRecords.forEach(record => {
    if (record.meta.syncState === syncStates.isSynchronizing) {
      record.meta.syncState = syncStates.isNotSynchronized;
      updateHorizontalPaintRecord(record);
    }
  });
}

/*--------------------------------------------------------------------
 *                            Photos
 *--------------------------------------------------------------------*/
async function syncPhotosRecords() {
  const state = store.getState();
  const records = Object.values(state.photos.records);
  const deletedRecordsIds = state.photos.deletedRecords;
  const deletedImagesIds = state.photos.deletedImages;

  const notSynchronizedRecords = records.filter(
    record => record.meta.syncState === syncStates.isNotSynchronized,
  );

  // To create or update records
  await helpers.syncRecords(notSynchronizedRecords, updatePhotoRecord, {
    endPoint: config.PHOTOS_RECORDS_ENDPOINT,
    method: 'POST',
  });

  // To synchronize deleted records
  helpers.syncDeleted(deletedRecordsIds, removePhotoRecordFromDeletedList, {
    endPoint: config.PHOTOS_RECORDS_ENDPOINT,
    method: 'DELETE',
  });

  // To synchronize deleted images
  helpers.syncDeleted(deletedImagesIds, removePhotoImageFromDeletedList, {
    endPoint: config.PHOTOS_RECORDS_IMAGE_ENDPOINT,
    method: 'DELETE',
  });

  /*               To synchronize images
  ? Note that it is necessary to read the store again, because the
  ? "syncRecords" methods could modify the state of the records,
  ? and set it in "isPartiallySynchronized"
  */
  const latestState = store.getState();
  const latestRecords = Object.values(latestState.photos.records);

  const partiallySynchronizedRecords = latestRecords.filter(
    record => record.meta.syncState === syncStates.isPartiallySynchronized,
  );

  helpers.syncImages(partiallySynchronizedRecords, updatePhotoRecord, {
    endPoint: config.PHOTOS_RECORDS_IMAGE_ENDPOINT,
    method: 'POST',
    idRecordName: 'registro_fotografico_id',
  });

  // To return records stuck in state x, to a state that allows them to be
  // synchronized later.
  // Note that this does not affect the records that went into sync mode due
  // to the above functions.
  latestRecords.forEach(record => {
    if (record.meta.syncState === syncStates.isSynchronizing) {
      record.meta.syncState = syncStates.isNotSynchronized;
      updatePhotoRecord(record);
    }
  });
}
