export default {
  // Basic states
  isNotSynchronized: 0,
  isSynchronizing: 1,
  isPartiallySynchronized: 2,
  isFullySynchronized: 3,

  // Other states
  isNotReadyToSynchronize: 4,
};
