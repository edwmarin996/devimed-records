export const theme = {
  palette: {
    common: {
      white: '#FFFFFF',
      black: '#000000',
    },

    // Brand
    primary: {
      main: '#06F',
    },

    secondary: {
      main: '#353535',
    },

    // Neutral
    grey: {
      main: '#D9D9D7',
    },

    // Semantic
    error: {
      main: '#E4144B',
    },

    warning: {
      main: '#FFAA00',
    },

    info: {
      main: '#0095FF',
    },

    success: {
      main: '#02C39A',
    },
  },
};
