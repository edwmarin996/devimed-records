# Registros Devimed

Registros Devimed, es una aplicación móvil, para el registro de eventos de mantenimiento y operación en campo, dentro de las vías del concesionario vial Devimed SA.

# Información para usuarios

### Documentación

Puede consultar la documentación para el uso de la aplicación [aquí](./src/docs/main.md)

# Información para desarrolladores

### Como contribuir

- Clone este repositorio

  `git clone https://gitlab.com/jdiego06/devimed-records-manager`

  `cd devimed-records-manager`

* Prepare el entorno de trabajo de acuerdo a [este](https://reactnative.dev/docs/environment-setup) tutorial (React Native CLI Quickstart)
* Instale los paquetes necesarios

  `npm i`

* Inicie el servidor de desarrollo

  `npx react-native start`

* Ejecute la aplicación en modo debug

  `npx react-native run-android`

### Como desinstalar aplicación del celular

`adb uninstall "com.devimed_records_manager"`

### Como construir para google play

`cd android && ./gradlew bundleRelease`
