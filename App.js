import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';
import { NavigationContainer } from '@react-navigation/native';
import Navigation from './src/pages/Navigation/Navigation';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from './src/redux/store/store';
import { Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <ApplicationProvider {...eva} theme={eva.light}>
            <PaperProvider>
              <StatusBar hidden />
              <Navigation />
            </PaperProvider>
          </ApplicationProvider>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};

export default App;
